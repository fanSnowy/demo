<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::group(['prefix' => '/demo', 'namespace' => '\App\Http\Controllers'], function () {
    Route::get('/', 'SiteController@renderHomePage');
    Route::get('/services', 'SiteController@renderServicesPage');
    Route::get('/articles/{cgy}', 'SiteController@renderCgyArticlesPage');
    Route::get('/article/{article}', 'SiteController@renderArticlePage');
    Route::get('/about', 'SiteController@renderAboutPage');
    Route::get('/contact', 'SiteController@renderContactPage');
    Route::get('/thank', 'SiteController@renderThankPage');
    Route::get('/policy', 'SiteController@renderPolicyPage');
    Route::get('/portfolio', 'SiteController@renderPortfolioPage');
    Route::get('/portfolio/{media}', 'SiteController@renderPortfolioDetailPage');
    Route::post('/save', 'SiteController@save');
});

Route::get('/', '\App\Http\Controllers\SiteController@renderWelcomePage');

//自定義後台路由規則
Route::group(['prefix' => 'admin', 'namespace' => '\App\Http\Controllers', 'middleware' => ['web', 'javck.roleCheck', 'javck.verifyEnabled']], function () {
});

Route::get('/demo/re', function () {
    return redirect(url('/demo'));
});
Route::get('/demo/go', function () {
    return redirect('https://www.google.com');
});

Route::get('/test/{id}/{name}', function ($id, $name) {
    return 'HELLO' . $id . 'name is' . $name;
});
Route::get('/test/{id?}', function ($id = 'BB') {
    return 'HELLO' . $id;
});
//群組路由
Route::group(['prefix' => 'test'], function () {
    Route::get('/hello', function () {
        return 'Hello';
    });
    Route::get('/world', function () {
        return 'World';
    });
});
Route::get('/index', 'App\Http\Controllers\HomeController@index')->name('home.index');

Route::get('buy', function () {
    return view('buy');
});
Route::get('gamer2', 'App\Http\Controllers\HomeController@game');
Route::get('foreach', 'App\Http\Controllers\HomeController@foreach');
Route::get('test1', 'App\Http\Controllers\HomeController@test1');
Route::get('addinfo', 'App\Http\Controllers\HomeController@addinfo');

Route::get('find', 'App\Http\Controllers\HomeController@find');
Route::get('product/{product}', 'App\Http\Controllers\HomeController@findOne');
Route::get('product/{product}/cgy', 'App\Http\Controllers\HomeController@findcgy');
Route::get('/cgy/product/{cgy}', 'App\Http\Controllers\HomeController@findproduct');

Route::get('/order/addproduct/{product}', 'App\Http\Controllers\HomeController@addproduct');
Route::get('/order/product/{order}', 'App\Http\Controllers\HomeController@order');
Route::get('/order/remote/{product}', 'App\Http\Controllers\HomeController@remote');
Route::get('/order/sync/{order}', 'App\Http\Controllers\HomeController@sync');

Route::get('order/withProduct', 'App\Http\Controllers\HomeController@withproduct');
Route::get('product/fun/hasorder', 'App\Http\Controllers\HomeController@hasorder');
