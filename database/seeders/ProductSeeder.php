<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        product::truncate();
        product::factory()->times(500)->create();

        // for ($i = 1; $i <= 500; $i++) {
        //     product::create([
        //         // 'title' => '商品' . ($i),
        //         // 'price' => rand(300, 5000),
        //         // 'desc' => '很好的商品' . ($i),
        //         // 'enabled' => rand(0, 1),
        //         // 'sell_at' => Carbon::now(),
        //         // 'cgy_id' => rand(1, 10),

        //         'title' => $faker->realText(10),
        //         'price' => $faker->numberBetween(300, 10000),
        //         'desc' => $faker->realText(100),
        //         'enabled' => rand(0, 1),
        //         'sell_at' => Carbon::now()->addDays($faker->numberBetween(-50, 50)),
        //         'cgy_id' => $faker->randomDigit,
        //     ]);
        // }

    }
}
