<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdouctsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('title', 50);
            $table->integer('price')->default(0)->unsigned();
            $table->text('desc')->nullable();
            $table->boolean('enabled')->default(true);
            $table->timestamp('sell_at');
            $table->bigInteger('cgy_id');
            $table->foreign('cgy_id')->reference('id')->on('cgys')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign(['cgy_id']);
        });

        Schema::dropIfExists('products');
    }
}
