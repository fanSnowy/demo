@extends('gamer')

@section('li1')
    @parent
 {{-- ABC --}}
@endsection

@section('list')

<ul class="BA-serve">
    <li><a href="//gnn.gamer.com.tw/" title="ACG相關即時新聞" class="gtm-indexservice"><img src="https://i2.bahamut.com.tw/nav-gnn.svg" class="IMG-08" />GNN新聞</a></li>
    <li><a href="//forum.gamer.com.tw/" title="ACG主題討論區" class="gtm-indexservice"><img src="https://i2.bahamut.com.tw/nav-forum.svg" class="IMG-02" />哈啦區</a></li>
    <li><a href="//haha.gamer.com.tw/" title="即時通訊聊天室" class="gtm-indexservice"><img src="https://i2.bahamut.com.tw/nav-ey.svg" class="IMG-28" />哈哈姆特不EY</a></li>
    <li><a href="//wall.gamer.com.tw/" title="幫你追蹤感興趣的最新動態" class="gtm-indexservice"><img src="https://i2.bahamut.com.tw/nav-wall.svg" class="IMG-29">動態牆<span class="BA-serve_new">Beta</span></a></li>
    <li><a href="//gnn.gamer.com.tw/gamecrazy.php" title="巴哈姆特自製節目，每週五播出" class="gtm-indexservice"><img src="https://i2.bahamut.com.tw/nav-gamecrazy.svg" class="IMG-07" />電玩瘋</a></li>
    <li><a href="//ani.gamer.com.tw/" title="動畫瘋" class="gtm-indexservice"><img src="https://i2.bahamut.com.tw/nav-ani.svg" class="IMG-27"><span style="color: #fff;background: #58BEC9;margin-right: 1px;padding-left: 2px;vertical-align: middle;">動</span><span style="color: #fff;background: #EB9D3D;margin-right: 1px;padding-left: 1px;">畫</span><span style="color: #fff;background: #E57B94;padding-left: 2px;vertical-align: middle;">瘋</span></a></li>
    <li><a href="//home.gamer.com.tw/" title="各式創作集中地" class="gtm-indexservice"><img src="https://i2.bahamut.com.tw/nav-creat.svg" class="IMG-04" />創作大廳</a></li>
    <li><a href="//home.gamer.com.tw/liveindex.php" title="實況大廳" class="gtm-indexservice"><img src="https://i2.bahamut.com.tw/nav-live.svg" class="IMG-26" />實況大廳</a></li>
    <li><a href="//guild.gamer.com.tw/" title="公會申請、公會一覽" class="gtm-indexservice"><img src="https://i2.bahamut.com.tw/nav-guild.svg" class="IMG-05" />公會大廳</a></li>
    <li><a href="//avatar1.gamer.com.tw" title="個人紙娃娃系統" class="gtm-indexservice"><img src="https://i2.bahamut.com.tw/nav-avatar.svg" class="IMG-11" />勇者造型</a></li>
    <li><a href="//buy.gamer.com.tw/" title="線上購物" class="gtm-indexservice"><img src="https://i2.bahamut.com.tw/nav-buy.svg" class="IMG-12" />巴哈商城</a></li>
    <li><a href="//fuli.gamer.com.tw/" title="勇者福利社" class="gtm-indexservice"><img src="https://i2.bahamut.com.tw/nav-fuli.svg" class="IMG-22" />勇者福利社</a></li>
    <li><a href="//prj.gamer.com.tw/app2u/bahaapp.html" title="ＡＰＰ" class="gtm-indexservice"><img src="https://i2.bahamut.com.tw/nav-bh.svg" class="IMG-25" />ＡＰＰ</a></li>
    <li><a href="//prj.gamer.com.tw/acgaward/" title="ACG創作大賽" style="white-space: nowrap;" class="gtm-indexservice"><img src="https://i2.bahamut.com.tw/nav-prj.svg" class="IMG-21" />ACG創作大賽</a></li>
    <li><a href="//user.gamer.com.tw/" title="各式會員服務" class="gtm-indexservice"><img src="https://i2.bahamut.com.tw/nav-user.svg" class="IMG-13" />會員中心</a></li>
    </ul>
@endsection
