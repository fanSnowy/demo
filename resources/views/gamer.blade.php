
<!DOCTYPE html>
<html lang="zh-Hant-TW">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="keywords" content="遊戲,動漫,動畫,漫畫,輕小說,線上遊戲,手機遊戲,手遊,Android,iOS,APP,PS4,PS5,討論區,論壇,">
<meta name="description" content="華人最大動漫及遊戲社群網站，提供 ACG 每日最新新聞、熱門排行榜，以及豐富的討論交流空間，還有精采的個人影音實況、部落格文章。">
<title>巴哈姆特電玩資訊站</title>
<meta property="og:url" content="https://www.gamer.com.tw" />
<meta property="og:title" content="巴哈姆特電玩資訊站" />
<meta property="og:description" content="華人最大動漫及遊戲社群網站，提供 ACG 每日最新新聞、熱門排行榜，以及豐富的討論交流空間，還有精采的個人影音實況、部落格文章。">
<meta property="fb:app_id" content="668497826514848">
<meta property="og:image" content="https://i2.bahamut.com.tw/bahaLOGO_1200x630.jpg" />
<meta name="thumbnail" content="https://i2.bahamut.com.tw/bahaLOGO_1200x630.jpg" />
<link rel="canonical" href="https://www.gamer.com.tw/">
<link rel="alternate" media="only screen and (max-width: 640px)" href="https://m.gamer.com.tw/">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Teko:300,400">
<meta name="application-name" content="巴哈姆特電玩資訊站" />
<link rel="apple-touch-icon" href="https://i2.bahamut.com.tw/apple-touch-icon.png" />
<link rel="apple-touch-icon" sizes="72x72" href="https://i2.bahamut.com.tw/apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon" sizes="144x144" href="https://i2.bahamut.com.tw/apple-touch-icon-144x144.png" />
<link href="https://i2.bahamut.com.tw/css/basic.css?v=1604455783" rel="stylesheet">
<link href="https://i2.bahamut.com.tw/css/search.css?v=1600912258" rel="stylesheet">
<link href="https://i2.bahamut.com.tw/css/index_2k14.css?v=1599725135" rel="stylesheet">
<link href="https://i2.bahamut.com.tw/css/index_top.css?v=1573528117" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://i2.bahamut.com.tw/css/font-awesome.css" rel="stylesheet">
<link href="https://i2.bahamut.com.tw/css/index_festival.css?v=1604908814" rel="stylesheet"><script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://www.googletagservices.com/tag/js/gpt.js"></script>
<script src="https://i2.bahamut.com.tw/js/movetomobile.js?v=1563334688"></script>
<script src="https://i2.bahamut.com.tw/js/plugins/js.cookie-2.1.4.js?v=1502676176"></script>
<script src="https://i2.bahamut.com.tw/js/plugins/lazysizes-3.0.0.min.js?v=1492420851" async></script>
<script src="https://i2.bahamut.com.tw/js/plugins/lightbox.js?v=1593400512"></script>
<script src="https://i2.bahamut.com.tw/js/plugins/dialogify.min.js?v=1591687434"></script>
<script src="https://i2.bahamut.com.tw/js/user_login.js?v=1566194792"></script>
<script src="https://i2.bahamut.com.tw/js/BH_topBar_noegg.js?v=1604907011" defer></script>
<script src="https://i2.bahamut.com.tw/js/csrf.js?v=1597387242"></script>
<script src="https://i2.bahamut.com.tw/js/util.js?v=1590639293"></script>
<script src="https://i2.bahamut.com.tw/js/signin_prj24y.js?v=1604983681"></script>
<link href="https://i2.bahamut.com.tw/css/prj-24y-index_singin.css?v=1604024846" rel="stylesheet">
<script src="https://i2.bahamut.com.tw/js/signin_ad.js?v=1605059865"></script>
<script src="https://i2.bahamut.com.tw/js/jquery/jquery.qrcode.min.js?v=1507887901"></script>
<script src="https://i2.bahamut.com.tw/js/ad.js?v=1602819564"></script>
<script src="https://i2.bahamut.com.tw/js/dfp.js?v=1598425998"></script>
<script>
jQuery(document).ready(function($){
if( typeof BH_AlkasooD === 'function' ) {
BH_AlkasooD($);
}
});

if ('1' != Cookies.get('ckNOMOBILE')) {
headertomobile(navigator.userAgent||navigator.vendor||window.opera,'https://m.gamer.com.tw');
}

if (location.href.match(/^https?:\/\/gamer\.com\.tw/)) {
location.replace("https://www.gamer.com.tw/");
}
</script>

<script>

if (window.Cookies) {
BAHAID = Cookies.get('BAHAID') || "";
} else if (window.getCookie) {
BAHAID = getCookie( 'BAHAID' );
} else if (window.jQuery && jQuery.cookie) {
BAHAID = jQuery.cookie('BAHAID') || "";
} else {
BAHAID = "";
}

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-108920-1', 'auto');


if (BAHAID) {
ga('set', 'userId', BAHAID.toLowerCase());
}

ga('send', 'pageview');
</script>

<script>
//anti adblock
var AntiAd = AntiAd || {};
(function(AntiAd) {
'use strict';

AntiAd.flag = false;
AntiAd.whiteList = [];

AntiAd.verifyLink = function (link) {}
AntiAd.block = function() {}
AntiAd.check = function(element) {}

})(AntiAd);
</script>
<script type="application/ld+json">[{"@context":"http:\/\/schema.org","@type":"WebSite","name":"\u5df4\u54c8\u59c6\u7279\u96fb\u73a9\u8cc7\u8a0a\u7ad9","url":"https:\/\/www.gamer.com.tw\/","alternateName":"\u5df4\u54c8\u59c6\u7279"},{"@context":"http:\/\/schema.org","@type":"Organization","url":"https:\/\/www.gamer.com.tw\/","logo":"https:\/\/i2.bahamut.com.tw\/baha_logo5.png"}]</script>
<script src="https://www.gstatic.com/firebasejs/5.9.0/firebase.js"></script>
<script src="https://i2.bahamut.com.tw/js/im_layout_lib.js?v=1586846585"></script>
<script src="https://i2.bahamut.com.tw/js/im_top_bar.js?v=1603175569"></script>
</head>
<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KDKMGT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KDKMGT');</script>
<!-- End Google Tag Manager --><div class="BA-topbg">
<div class="BA-wrapper BA-top"><!--天開始-->
<a href="https://www.gamer.com.tw"><img width="156" height="107" class="BA-logo" src="https://i2.bahamut.com.tw/baha_logo5.svg" border="0" /></a>
<div class="TOP-my"></div>
<div id="topBarMsg_more" style="display:none" class="TOP-msg"></div>
<script>
if( window.top != window ) window.top.location = "https://www.gamer.com.tw";

jQuery(function(){
TopBar.render();
TopBar.getNotifyNumber();
});
</script>

<div style="margin-left:200px;">
<div id="index_search"><gcse:searchbox-only></gcse:searchbox-only></div>
<script>
(function() {
var cx = 'partner-pub-9012069346306566:kd3hd85io9c';
var gcse = document.createElement('script');
gcse.type = 'text/javascript';
gcse.async = true;
gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(gcse, s);
})();
</script>
<!-- GOOGLE CSE 搜尋結果結束 -->
</div>

<ul class="BA-menu">
    <li>{{$phone}}</li>
    <li class="h2">{{ $age }}</li>
    {{-- <li class="h2">{{ $height }}</li> --}}

@section('li1')
    <li><a href="//acg.gamer.com.tw/news.php?p=android">AAAndroid</a></li>
@show

<li><a href="//acg.gamer.com.tw/news.php?p=ios">iOS</a></li>
<li><a href="//acg.gamer.com.tw/news.php?p=olg">PC線上</a></li>
<li><a href="//acg.gamer.com.tw/news.php?p=pc">PC單機</a></li>
<li><a href="//acg.gamer.com.tw/news.php?p=web">WEB</a></li>
<li><a href="//acg.gamer.com.tw/news.php?p=ps5">PS5</a></li>
<li><a href="//acg.gamer.com.tw/news.php?p=ps4">PS4</a></li>
<li><a href="//acg.gamer.com.tw/news.php?p=xbsx">XBSX</a></li>
<li><a href="//acg.gamer.com.tw/news.php?p=xbone">XBOne</a></li>
<li><a href="//acg.gamer.com.tw/news.php?p=ns">NS</a></li>
<li><a href="//acg.gamer.com.tw/news.php?p=anime">動畫</a></li>
<li><a href="//acg.gamer.com.tw/news.php?p=comic">漫畫</a></li>
<li><a href="//acg.gamer.com.tw/news.php?p=novel">輕小說</a></li>
<li class="more"><a href="//acg.gamer.com.tw/more.php" title="更多平台"></a></li>
</ul>
</div><!--天結束-->
</div>

<div id="BH-background">

<div class="BA-wrapper BA-main"><!--中間主內容外包裝-->
<div class="BA-left"><!--左側內容-->
<script>
(function(window, $, undefined) {
'use strict';
var _ad = [["<a href=\"https:\/\/prj.gamer.com.tw\/24y\/index.php\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"210098\"><img border=\"0\" src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/67\/950e84444f0dfd51c86fdbb1211ahw75.PNG\"><\/a>"]];
if (_ad.length) {
document.write('<div style="margin-bottom:10px">' + AdBaha.output(_ad, 5) + '</div>');
}

_ad = {"unfixed":{"a1":["<a href=\"https:\/\/prj.gamer.com.tw\/acgaward\/2021\/\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209610\"><img border=\"0\" src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/07\/2b01e21ed4ff02e93928c735d61acof5.JPG\"><\/a>"]}};
if ($(_ad.fixed).length) {
$.each(_ad.fixed, function(k, val) {
if (val.length == 1) {
document.write('<div style="margin-bottom:10px">' + val + '</div>');
} else {
document.write('<div style="margin-bottom:10px">' + val[AdBaha.ck('S', k, val.length)] + '</div>');
}
});
}

if ($(_ad.unfixed).length) {
_ad.unfixed = AdBaha.Shuffle(_ad.unfixed);
$.each(_ad.unfixed, function(k, val) {
if (val.length == 1) {
document.write('<div style="margin-bottom:10px">' + val + '</div>');
} else {
document.write('<div style="margin-bottom:10px">' + val[AdBaha.ck('S', k, val.length)] + '</div>');
}
});
}
})(window, jQuery);
</script>

@yield('list')

<!--每日簽到-->
<div style="margin-bottom: 10px;">
<a id="signin-btn" class="btn-popup btn-block popup-ctrl" role="button" onClick="Signin.start(this);"></a>
</div>
<script>Signin.load();</script>
<h1 class="BA-ltitle">動畫瘋新番</h1>
<div class="BA-lbox BA-lbox3">
<a href="//ani.gamer.com.tw/animeVideo.php?sn=19193" class="newanime_img"><img src="https://p2.bahamut.com.tw/S/2KU/11/11a47ee68c8cca5fc052a628641airz5.JPG" /></a><a href="//ani.gamer.com.tw/animeVideo.php?sn=19193" class="newanime_text"><span>強襲魔女 通往柏林之路</span><br>第 6 集 <font color="#F04F75">今日更新</font></a><a href="//ani.gamer.com.tw/animeVideo.php?sn=19191" class="newanime_img"><img src="https://p2.bahamut.com.tw/S/2KU/76/e6e47d1ab382ea57414d5d40f21air05.JPG" /></a><a href="//ani.gamer.com.tw/animeVideo.php?sn=19191" class="newanime_text"><span>大貴族</span><br>第 6 集 <font color="#F04F75">昨日更新</font></a><a href="//ani.gamer.com.tw/animeVideo.php?sn=19189" class="newanime_img"><img src="https://p2.bahamut.com.tw/S/2KU/17/7d16189f101f654c0856dd18c41ais55.JPG" /></a><a href="//ani.gamer.com.tw/animeVideo.php?sn=19189" class="newanime_text"><span>土下座跪求給看</span><br>第 5 集 <font color="#F04F75">昨日更新</font></a><a href="//ani.gamer.com.tw/animeVideo.php?sn=19187" class="newanime_img"><img src="https://p2.bahamut.com.tw/S/2KU/49/623be20ef67cfaae736a3e70631aivt5.JPG" /></a><a href="//ani.gamer.com.tw/animeVideo.php?sn=19187" class="newanime_text"><span>熊熊勇闖異世界</span><br>第 6 集 <font color="#F04F75">昨日更新</font></a><a href="//ani.gamer.com.tw/animeVideo.php?sn=19185" class="newanime_img"><img src="https://p2.bahamut.com.tw/S/2KU/80/6ae496362345165f0d2a048fbc1aitw5.JPG" /></a><a href="//ani.gamer.com.tw/animeVideo.php?sn=19185" class="newanime_text"><span>月歌。THE ANIMATION 2</span><br>第 6 集 <font color="#F04F75">昨日更新</font></a></div>
<div class="BA-lbox BA-newanime">
<ul>
<li><a href="//ani.gamer.com.tw/animeVideo.php?sn=18778"><span class="newanime_name">小妖怪</span><span class="newanime_vol">[20]</span></a><span class="newanime_dateweek">週三</span></li><li><a href="//ani.gamer.com.tw/animeVideo.php?sn=19183"><span class="newanime_name">光之戰記 -ZUERST-</span><span class="newanime_vol">[5]</span></a><span class="newanime_dateweek">週二</span></li><li><a href="//ani.gamer.com.tw/animeVideo.php?sn=18780"><span class="newanime_name">闇影詩章</span><span class="newanime_vol">[30]</span></a><span class="newanime_dateweek">週二</span></li><li><a href="//ani.gamer.com.tw/animeVideo.php?sn=19033"><span class="newanime_name">在魔王城說晚安</span><span class="newanime_vol">[6]</span></a><span class="newanime_dateweek">週二</span></li><li><a href="//ani.gamer.com.tw/animeVideo.php?sn=19031"><span class="newanime_name">阿松 第三季</span><span class="newanime_vol">[5]</span></a><span class="newanime_dateweek">週二</span></li><li><a href="//ani.gamer.com.tw/animeVideo.php?sn=19029"><span class="newanime_name">A3!</span><span class="newanime_vol">[17]</span></a><span class="newanime_dateweek">週二</span></li><li><a href="//ani.gamer.com.tw/animeVideo.php?sn=18770"><span class="newanime_name">黃金神威 第三季</span><span class="newanime_vol">[30]</span></a><span class="newanime_dateweek">週一</span></li></ul>
</div>
<script>
var acgtimer;
(function(window, $, undefined) {
var acglist = {"PC":[{"sn":"109166","title":"眾神殞落","time":"","machine":"單機","anime":0},{"sn":"113642","title":"御劍巔峰 H5","time":"","machine":"WEB","anime":0},{"sn":"114549","title":"風魔米娜：不死身的秘密","time":"","machine":"單機","anime":0},{"sn":"113181","title":"決勝時刻：黑色行動冷戰","time":"11.13","machine":"單機","anime":0},{"sn":"111207","title":"人中之龍 7 光與闇的去向","time":"11.13","machine":"單機","anime":0},{"sn":"113092","title":"仙劍蒼穹錄","time":"11.14","machine":"WEB","anime":0},{"sn":"113641","title":"仙劍情緣傳","time":"11.15","machine":"WEB","anime":0},{"sn":"106099","title":"水晶傳奇","time":"11.17","machine":"單機","anime":0},{"sn":"114484","title":"模擬消防小隊","time":"11.17","machine":"單機","anime":0},{"sn":"113942","title":"真三國龍戰","time":"11.18","machine":"WEB","anime":0}],"TV":[{"sn":"89504","title":"天穗之咲稻姬","time":"","machine":"PS4","anime":0},{"sn":"105913","title":"天穗之咲稻姬","time":"","machine":"NS","anime":0},{"sn":"111842","title":"惡魔靈魂 重製版","time":"","machine":"PS5","anime":0},{"sn":"111217","title":"普利尼 1・2","time":"","machine":"NS","anime":0},{"sn":"113297","title":"舞力全開 2021","time":"","machine":"NS","anime":0},{"sn":"112015","title":"徽章戰士 經典合集 PLUS 獨角仙版","time":"","machine":"NS","anime":0},{"sn":"112016","title":"徽章戰士 經典合集 PLUS 鍬形蟲版","time":"","machine":"NS","anime":0},{"sn":"113296","title":"舞力全開 2021","time":"","machine":"PS4","anime":0},{"sn":"112801","title":"未轉變者","time":"","machine":"XBOne","anime":0},{"sn":"114533","title":"蘭島物語 少女的約定 Remake","time":"","machine":"NS","anime":0}],"CN":[{"sn":"23128","title":"王者天下(53)","time":"","machine":"漫畫","anime":0},{"sn":"19488","title":"足球騎士(55)","time":"","machine":"漫畫","anime":0},{"sn":"96780","title":"最強魔法師的隱遁計畫(11) 首刷限定版","time":"","machine":"輕小說","anime":0},{"sn":"22782","title":"女僕咖啡廳(16)END","time":"","machine":"漫畫","anime":0},{"sn":"107017","title":"你喜歡被可愛女生攻陷嗎？(02) 首刷限定版","time":"","machine":"輕小說","anime":0},{"sn":"113460","title":"全班都被召喚到異世界去，只有我被留下來(02)","time":"","machine":"輕小說","anime":0},{"sn":"114199","title":"等級0的魔王大人，在異世界展開冒險者生活(01)","time":"","machine":"輕小說","anime":0}],"QA":[{"sn":"109451","title":"暮蟬悲鳴時 業","time":"週四","machine":"","anime":0},{"sn":"108139","title":"Assault Lily Bouquet","time":"週四","machine":"","anime":0},{"sn":"113013","title":"小碧藍幻想！","time":"週四","machine":"","anime":0},{"sn":"105394","title":"安達與島村","time":"週四","machine":"","anime":0},{"sn":"113596","title":"全員惡玉","time":"週四","machine":"","anime":0},{"sn":"105855","title":"侏羅紀世界：白堊冒險營","time":"週五","machine":"","anime":0},{"sn":"113525","title":"霹靂兵烽決","time":"週五","machine":"","anime":0},{"sn":"107955","title":"在地下城尋求邂逅是否搞錯了什麼 第三季","time":"週五","machine":"","anime":0},{"sn":"108269","title":"魔女之旅","time":"週五","machine":"","anime":0},{"sn":"108886","title":"咒術迴戰","time":"週五","machine":"","anime":0}]};
var aclassAry = {"PC":'class="launch-short"',"TV":'class="launch-short-TV"',"CN":'class="launch-short-TV"',"QA":'class="launch-longer-anime"'};

var acg = {
'acg_change': function(who) {
acgtimer = setTimeout("ACGFRESH.acg_changea('"+who+"')",200);
},

'acg_changea': function(who) {
var showlist = '';

if( acglist[who] ) {
showlist += '<ul>';

for(i=0; i<acglist[who].length; i++) {
showlist += '<li>';
showlist +=  (acglist[who][i].machine) ? '<span>' + acglist[who][i].machine + '</span>' : '';
showlist += '<a href="//acg.gamer.com.tw/acgDetail.php?s=' + acglist[who][i].sn  + '"';

if( 'QA' === who ) {
showlist += (!acglist[who][i].time && !acglist[who][i].anime ) ? ' '+aclassAry[who] : '';
} else {
showlist += (acglist[who][i].time) ? ' '+aclassAry[who] : '';
}

showlist += '>' + acglist[who][i].title + '</a>';

if( acglist[who][i].anime ) {
showlist += '<a href="//ani.gamer.com.tw/animeRef.php?sn=' + acglist[who][i].anime + '" target="_blank" class="freewatch">免費看</a>';
} else {
showlist += ((acglist[who][i].time) ? '<i>'+acglist[who][i].time+'</i>': '') + '</li>';
}

showlist += '</li>';
}

showlist += '</ul>';
} else {
showlist += '<div style="text-align:center;padding:0 3px 8px 3px;">統計中</div>';
}

switch(who) {
case 'TV': case 'CN':
var boxclass = 'launch-TV';
break;
case 'QA':
var boxclass = 'launch-anime';
break;
default:   var boxclass = '';
}

$('#acgBlock').removeClass('launch-TV launch-anime').addClass(boxclass).html(showlist);

$('.launch-now').filter('[id^="AcgTab_"]').removeClass('launch-now');
$('#AcgTab_'+who).addClass('launch-now');
},

'acgShowIni': function() {
var acgBlockAry = ["PC","TV","QA","CN"];
var acgBlockIniKey = Math.floor( Math.random() * acgBlockAry.length );
acgBlockIniKey = (3 <= acgBlockIniKey || 0 >= acgBlockIniKey ) ? 0 : acgBlockIniKey;

ACGFRESH.acg_change(acgBlockAry[acgBlockIniKey]);
}
};

window.ACGFRESH = acg;
})(window, jQuery);
</script>
<h1 class="BA-ltitle">上市發行表</h1>
<div class="launch-nav">
<ul>
<li id="AcgTab_PC"><a href="javascript:;" onClick="ACGFRESH.acg_changea('PC')" onmouseover="ACGFRESH.acg_change('PC')" onmouseout="clearTimeout(acgtimer)">PC</a></li>
<li id="AcgTab_TV"><a href="javascript:;" onClick="ACGFRESH.acg_changea('TV')" onmouseover="ACGFRESH.acg_change('TV')" onmouseout="clearTimeout(acgtimer)">TV</a></li>
<li id="AcgTab_QA"><a href="javascript:;" onClick="ACGFRESH.acg_changea('QA')" onmouseover="ACGFRESH.acg_change('QA')" onmouseout="clearTimeout(acgtimer)">動畫</a></li>
<li id="AcgTab_CN"><a href="javascript:;" onClick="ACGFRESH.acg_changea('CN')" onmouseover="ACGFRESH.acg_change('CN')" onmouseout="clearTimeout(acgtimer)">其他</a></li>
</ul>
</div>
<div class="BA-lbox launch-box" id="acgBlock"></div>
<script>ACGFRESH.acgShowIni()</script>
<!--左側上市表 結束-->

<h1 class="BA-ltitle">精選商品</h1>
<div class="BA-lbox BA-lbox3">
<a href="https://buy.gamer.com.tw/search.php?kw=%E5%A4%A9%E7%A9%97%E4%B9%8B%E5%92%B2%E7%A8%BB%E5%A7%AC"><img src="https://p2.bahamut.com.tw/B/2KU/29/88e7911b2f70013c80ccea35da19ez55.JPG" /></a><a href="https://buy.gamer.com.tw/search.php?kw=%E5%A4%A9%E7%A9%97%E4%B9%8B%E5%92%B2%E7%A8%BB%E5%A7%AC"><font color=#F04F75>《天穗之咲稻姬》<br />
米就是力量！</font></a><a href="https://buy.gamer.com.tw/search.php?kw=%E5%B7%B4%E5%93%88%E5%A7%86%E7%89%B9+24+%E9%80%B1%E5%B9%B4+%E5%8B%87%E8%80%85%E5%A4%A7%E5%85%A8%E5%A5%97%E7%89%B9%E6%83%A0%E7%B5%84"><img src="https://p2.bahamut.com.tw/B/2KU/50/7e1be284de601f301e93bb9ce51aikq5.JPG" /></a><a href="https://buy.gamer.com.tw/search.php?kw=%E5%B7%B4%E5%93%88%E5%A7%86%E7%89%B9+24+%E9%80%B1%E5%B9%B4+%E5%8B%87%E8%80%85%E5%A4%A7%E5%85%A8%E5%A5%97%E7%89%B9%E6%83%A0%E7%B5%84"><font color=#4288E3>巴哈姆特 24 週年<br />
超值勇者周邊套組！</font></a></div>
<h1 class="BA-ltitle">學術研究</h1>
<div class="BA-lbox BA-lbox1">
<ul><li><a href="https://www.surveycake.com/s/VpzXo" target="_blank"><font color="#00A002">體感式遊戲消費者購買體感式遊戲再購意願因素之研究－以健身環大冒險為例</font></a></li><li><a href="https://docs.google.com/forms/d/e/1FAIpQLSdrbcVAYLE2h0JGai5TBL1zIQe8NRVlgmzqhFxP55jF5fsjBQ/viewform" target="_blank"><font color="#00A002">台灣泡麵喜好調查</font></a></li><li><a href="https://www.surveycake.com/s/Vqn1M" target="_blank"><font color="#009EA0">手遊『FGO』玩家相關之研究 --以玩家對歷史之關心程度為中心--</font></a></li><li><a href="https://docs.google.com/forms/d/e/1FAIpQLSfXead1RWx1NcWHWleJxGh7efmogZPZXq6-iyrkN2lMWs4lqA/viewform" target="_blank"><font color="#00A002">用戶生成內容來源對廣告效果之影響</font></a></li><li><a href="https://docs.google.com/forms/d/e/1FAIpQLSeNF3hsxm0IPNRVaRSV96RIwEX-q5CudQpxn8NUxkGVSkf6Jw/viewform" target="_blank"><font color="#009EA0">使用者對於遊戲手把的習慣與看法</font></a></li><li><a href="https://user.gamer.com.tw/help/tellus.php?c1=4" target="_blank">刊登學術問卷請來信巴哈姆特</a></li></ul></div>
<!-- 沒有投票 --><div class="BA-lbox-copyright">
<ul>
<li>旺普網路資訊股份有限公司<p>©All Rights Reserved.</p></li>
<li><a href="//user.gamer.com.tw/help/tellus.php" target="_blank">問題反應</a>｜<a href="//user.gamer.com.tw/help/tellus.php?c1=3" target="_blank">合作提案</a>｜<a href="apply.php" target="_blank">招兵買馬</a><br><a href="//user.gamer.com.tw/help/detail.php?sn=144&top=1" target="_blank">隱私權聲明</a>｜<a href="//user.gamer.com.tw/help/detail.php?sn=280&top=1" target="_blank">著作權聲明</a></li>
<li><a href="//user.gamer.com.tw/help/detail.php?sn=269" target="_blank">本站已依網站內容分級規定處理</a><br>法律顧問：益思科技法律事務所</li>
</ul>
</div>

</div><!--左側內容結束-->

<div class="BA-center" id="BA-center-id"><!--中間內容-->

<!--GNN新聞-->
<script>
var gnnflag, gnnshowing = 0;

(function(window, $, undefined) {
var gnnwhoflag, gnnwho = 'all', gnnwhoc = 0, ghc = 8;

var gnn = {
gnn_change: function(who) {
gnnflag = setTimeout("GNN_HEAD.gnn_changea('"+who+"', 0)",200);
},

gnn_changea: function(who, action) {
clearTimeout(gnnwhoflag);
if (gnnwho != who) {
gnnwhoc = 0;
}

var more = {"all":"","pc":"1","tv":"3","pm":"4","ac":"5","show":"11","prj":"9","esports":"13"};
var morecontx = {"all":"新聞","pc":"PC新聞","tv":"TV新聞","pm":"手機新聞","ac":"動漫新聞","show":"活動新聞","prj":"主題報導","esports":"電競新聞"};
var list = {"all":{"head":[],"headAdd":[{"sn":"206235","machine":"手機","title":"《Fate\/Grand Order》日版推出期間限定活動「虛數大海戰」 全新從者梵谷、尼莫登場","pic":"39\/91f374e361944904b9e9cad5951aj3v5.JPG"},{"sn":"206230","machine":"多平台","title":"微軟商店疑似曝光《極地戰嚎 6》新發售日 將延期至 2021 年 5 月上市？","pic":"18\/c0399ab8e9542ec5252229eacb1aj3a5.JPG"},{"sn":"206218","machine":"多平台","title":"CDP 強調《電馭叛客 2077》在台發售版本內容與海外絕無差異 重申 {{ $time }} 上市承諾","pic":"92\/56289719060289e25713673f591aj2k5.JPG"},{"sn":"206212","machine":"PS5","title":"紀念 PS5 發售！日本 SIE 於秋葉原神田明神神社驚喜舉辦燈光秀活動","pic":"54\/e8f3b22307fa2f971b4047563b1aj1i5.JPG"},{"sn":"206206","machine":"多平台","title":"Apple 公開搭載自家晶片 M1 的新一代 MacBook Air、MacBook Pro、Mac mini 等","pic":"30\/4d5db4fd6dc037b4ae07ab4ecc1aj0u5.JPG"},{"sn":"206182","machine":"多平台","title":"培育優良稻米提升能力！和風動作 RPG《天穗之咲稻姬》全球版搶先上市","pic":"66\/ff0f071a0070220d66cca56f231aiwa5.JPG"},{"sn":"206181","machine":"手機","title":"韓國漫畫改編遊戲《靈魂方舟》雙平台上線 與妲己一起成長並見證仙界大戰的一切故事","pic":"93\/cc36466346442a24cb0c023d4e1aix15.JPG"},{"sn":"206177","machine":"手機","title":"Niantic 將於台南等地區推出「Pokemon GO 城市焦點」計畫 結伴探索鄉土 振興在地觀光","pic":"27\/c1b6f8ed02cf287749f40f80391aiv75.JPG"}],"normal":[{"sn":"206238","machine":"手機","title":"《最後的克勞迪亞 LAST CLOUDIA》繁中版代理權確定 即日起開放事前登錄","icon":{"video":"1","player":"0","hot":0}},{"sn":"206236","machine":"ETC","title":"Youtube 傳出全球大當機災情 官方表示正陸續修復問題","icon":{"video":"0","player":"0","hot":0}},{"sn":"206237","machine":"其他","title":"「米滷蛋家族」聯手角色經紀公司卡洛特將展開新企劃 預定 2021 年底推出動畫影集","icon":{"video":"0","player":"0","hot":0}},{"sn":"206173","machine":"手機","title":"Sensor Tower 公布 2020 年 10 月全球手機遊戲營收排行 《原神》以 2.39 億美元空降第一名","icon":{"video":"0","player":"0","hot":1}},{"sn":"206159","machine":"OLG","title":"《魔獸世界：暗影之境》版本前夕事件即將登場 響應號召面對希瓦娜斯造成的後果","icon":{"video":"1","player":"0","hot":1}},{"sn":"206233","machine":"多平台","title":"《小小大冒險》線上多人模式年內登場 豪華版《死亡擱淺》《底特律》等服裝搶先看","icon":{"video":"1","player":"0","hot":0}},{"sn":"206232","machine":"ETC","title":"【TpGS 21】首款桌遊《我的世界：建造者與生物群落》確定推出中文版 近期開放預購","icon":{"video":"0","player":"0","hot":0}},{"sn":"206228","machine":"PC","title":"扮演摩西分紅海！冒險新作《摩西：從埃及到應許之地》曝光","icon":{"video":"1","player":"0","hot":0}},{"sn":"206211","machine":"手機","title":"節奏遊戲《HoneyWorks Premium Live》將於 11 月 18 日推出 開放日本雙平台預約","icon":{"video":"1","player":"0","hot":0}},{"sn":"206210","machine":"ETC","title":"Take-Two 宣布以新台幣 288 億元收購英國老牌遊戲開發商 Codemasters","icon":{"video":"0","player":"0","hot":0}},{"sn":"206202","machine":"動漫畫","title":"《怪病醫拉姆尼》公開主視覺圖以及追加聲優名單 諏訪部順一將參與本作演出","icon":{"video":"1","player":"0","hot":0}}]},"tv":{"head":[],"headAdd":[{"sn":"206230","machine":"多平台","title":"微軟商店疑似曝光《極地戰嚎 6》新發售日 將延期至 2021 年 5 月上市？","pic":"18\/c0399ab8e9542ec5252229eacb1aj3a5.JPG"},{"sn":"206212","machine":"PS5","title":"紀念 PS5 發售！日本 SIE 於秋葉原神田明神神社驚喜舉辦燈光秀活動","pic":"54\/e8f3b22307fa2f971b4047563b1aj1i5.JPG"},{"sn":"206233","machine":"多平台","title":"《小小大冒險》線上多人模式年內登場 豪華版《死亡擱淺》《底特律》等服裝搶先看","pic":"24\/faa587491e7d994fbd3b1449921aj3g5.JPG"},{"sn":"206210","machine":"ETC","title":"Take-Two 宣布以新台幣 288 億元收購英國老牌遊戲開發商 Codemasters","pic":"58\/eb208684dd710441a257c3f6991aj1m5.PNG"},{"sn":"206192","machine":"多平台","title":"《眾神殞落 Godfall》發售前夕釋出自訂武裝配置以及頭目戰介紹","pic":"65\/4ff3e72ccee6439085c58b1dcf1aiz15.JPG"},{"sn":"206186","machine":"ETC","title":"美國新興職業摔角團體 AEW 宣布成立遊戲品牌 將與 Yuke&#039;s 合作開發家用主機遊戲","pic":"37\/43390b8e6367a5af6dcdfbe9451aiy95.JPG"},{"sn":"206183","machine":"多平台","title":"adidas《電馭叛客 2077》X9000 聯名跑鞋系列上陣 結合遊戲元素與未來科幻配色","pic":"16\/2cbc15d9163adca96c166766321aixo5.JPG"},{"sn":"206180","machine":"多平台","title":"當個潮派超級英雄！ 《漫威蜘蛛人：邁爾斯摩拉斯》愛迪達合作款球鞋現身遊戲中","pic":"78\/4163094a896ae1daef308ef7f71aiwm5.JPG"}],"normal":[{"sn":"206175","machine":"XBSX","title":"國外非官方 Xbox Series X 拆機影片確認內建 SSD 為可更換模組設計","icon":{"video":"0","player":"0","hot":0}},{"sn":"206167","machine":"多平台","title":"《極限競速：地平線 4》釋出 Xbox Series X | S 強化宣傳影片 體驗 4K 60FPS 極致快感","icon":{"video":"1","player":"0","hot":0}},{"sn":"206164","machine":"NS","title":"揮舞專用控制器組件來炒飯！料理動作戰鬥遊戲《食王者》決定 11 月 28 日發售","icon":{"video":"1","player":"0","hot":0}},{"sn":"206161","machine":"多平台","title":"《決勝時刻：現代戰域》公布未來計畫 支援《現代戰爭》與《黑色行動冷戰》","icon":{"video":"0","player":"0","hot":0}},{"sn":"206234","machine":"多平台","title":"《王國之心》系列首部節奏動作遊戲《王國之心：記憶旋律》今日上市","icon":{"video":"1","player":"0","hot":0}},{"sn":"206185","machine":"多平台","title":"《決勝時刻：黑色行動冷戰》解析「殭屍進擊」模式及其他 PlayStation 獨家內容","icon":{"video":"1","player":"0","hot":0}},{"sn":"206176","machine":"ETC","title":"以《超級瑪利歐》登場角色與道具為靈感打造的禮品與角色夜燈即將在日本推出","icon":{"video":"0","player":"0","hot":0}},{"sn":"206226","machine":"PS5","title":"【直播】PS5 版《漫威蜘蛛人：邁爾斯摩拉斯》化身新生代蜘蛛人的全新冒險","icon":{"video":"1","player":"0","hot":0}},{"sn":"206152","machine":"PS5","title":"PS5 公布官方終極問答 確認 M.2 插槽將於後續開放 外接儲存裝置遊戲存放功能研究中","icon":{"video":"0","player":"0","hot":1}},{"sn":"206143","machine":"多平台","title":"探索英格蘭黑暗時期！《刺客教條：維京紀元》今日上市 在英靈殿諸神間爭取一席之地","icon":{"video":"1","player":"0","hot":1}},{"sn":"206117","machine":"多平台","title":"恐怖驚悚系列《黑相集》第三彈作品《黑相集：灰冥界》中文版預定 2021 年發售","icon":{"video":"1","player":"0","hot":1}}]},"pc":{"head":[],"headAdd":[{"sn":"206236","machine":"ETC","title":"Youtube 傳出全球大當機災情 官方表示正陸續修復問題","pic":"48\/85a8112d9b80f7aa84627efa2d1aj445.JPG"},{"sn":"206218","machine":"多平台","title":"CDP 強調《電馭叛客 2077》在台發售版本內容與海外絕無差異 重申 12 月上市承諾","pic":"92\/56289719060289e25713673f591aj2k5.JPG"},{"sn":"206206","machine":"多平台","title":"Apple 公開搭載自家晶片 M1 的新一代 MacBook Air、MacBook Pro、Mac mini 等","pic":"30\/4d5db4fd6dc037b4ae07ab4ecc1aj0u5.JPG"},{"sn":"206182","machine":"多平台","title":"培育優良稻米提升能力！和風動作 RPG《天穗之咲稻姬》全球版搶先上市","pic":"66\/ff0f071a0070220d66cca56f231aiwa5.JPG"},{"sn":"206159","machine":"OLG","title":"《魔獸世界：暗影之境》版本前夕事件即將登場 響應號召面對希瓦娜斯造成的後果","pic":"17\/cc1586ee6a12d6219cc0629d9e1afb55.PNG"},{"sn":"206232","machine":"ETC","title":"【TpGS 21】首款桌遊《我的世界：建造者與生物群落》確定推出中文版 近期開放預購","pic":"21\/d4754e3755efd6227131be75831aj3d5.JPG"},{"sn":"206228","machine":"PC","title":"扮演摩西分紅海！冒險新作《摩西：從埃及到應許之地》曝光","pic":"17\/3bb8412fa467c18151d2676f991aj395.JPG"},{"sn":"206184","machine":"多平台","title":"加入多人連線模式《俄羅斯方塊效應：連接》登上 Xbox、Win10 平台 釋出上市宣傳影片","pic":"19\/d9cccd2175c66c5f4d678db5521aixr5.JPG"}],"normal":[{"sn":"206168","machine":"多平台","title":"空戰角色扮點遊戲《空戰獵鷹》上市 操控空中戰士展開技巧性遠距攻擊","icon":{"video":"1","player":"0","hot":0}},{"sn":"206165","machine":"OLG","title":"《英雄聯盟》公開季前 2021 介紹影片 全明星賽票選正式展開","icon":{"video":"1","player":"0","hot":0}},{"sn":"206205","machine":"多平台","title":"2020 創新科技教育暨電競大賽連續五天三創登場 推出多項課程、體驗《黑色行動冷戰》等","icon":{"video":"0","player":"0","hot":0}},{"sn":"206229","machine":"多平台","title":"《絕地求生》越野摩托車 9.2 版參上 上演花式車技並以全新騎射技術摧毀敵人","icon":{"video":"1","player":"0","hot":0}},{"sn":"206227","machine":"多平台","title":"2020 遊戲橘子週年慶「橘星花火祭」　公開玩家遊玩數據、「讀星術」剖析玩家遊戲風格等","icon":{"video":"0","player":"0","hot":0}},{"sn":"206225","machine":"OLG","title":"使用特殊公事包戰鬥！《永恆輪迴：黑色倖存者》第 17 位新角色「彰一」今日登場","icon":{"video":"0","player":"0","hot":0}},{"sn":"206221","machine":"PC","title":"Porsche Design 與 AOC 推出專業電競顯示器「PD27」 搭配多項專為長時間遊戲設計護眼功能","icon":{"video":"0","player":"0","hot":0}},{"sn":"206219","machine":"PC","title":"《拳皇 2002 無限對決》今日更新開放「回滾型網路代碼」 將改善連線對戰延遲問題","icon":{"video":"0","player":"0","hot":0}},{"sn":"206191","machine":"PC","title":"探究不死之身忍者的秘密 《風魔米娜：不死身的秘密》健全版明日推出","icon":{"video":"1","player":"0","hot":0}},{"sn":"206171","machine":"PC","title":"國產獨立恐怖 AVG《案件００：食人小男孩》對應 macOS 平台","icon":{"video":"1","player":"jadegamer","hot":0}},{"sn":"206170","machine":"PC","title":"【開箱】CHERRY MX BOARD 8.0 機械式鍵盤開箱","icon":{"video":"0","player":"norick04","hot":0}}]},"ac":{"head":[],"headAdd":[{"sn":"206202","machine":"動漫畫","title":"《怪病醫拉姆尼》公開主視覺圖以及追加聲優名單 諏訪部順一將參與本作演出","pic":"17\/3c882fe101b0a36c99676edf8b1aj0h5.JPG"},{"sn":"206187","machine":"動漫畫","title":"《愛★Chu》動畫公開主視覺圖、首支宣傳影片 預計 2021 年 1 月開播","pic":"45\/c59689e86ec0edb9b36f047d4e1aiyh5.JPG"},{"sn":"206105","machine":"動漫畫","title":"確認將提告！木棉花針對《鬼滅之刃劇場版 無限列車篇》發出反盜版嚴正聲明","pic":"82\/2f9d9c890edd66ef0b8386bb9e1aida5.JPG"},{"sn":"206099","machine":"其他","title":"「亞洲動漫創作展 Petit Fancy 33」活動現場 Cosplay 照片大集合","pic":"24\/8fbb13fc2b641be82fed0555a51aibo5.JPG"},{"sn":"206096","machine":"其他","title":"初音未來「MAGICAL MIRAI 2020」大阪場演唱會線上轉播月底登場","pic":"09\/12929b9f5a6b1c8f3f931d697b1aib95.JPG"},{"sn":"206033","machine":"動漫畫","title":"《鬼滅之刃劇場版》日本票房突破 204 億 擠進影史票房前五 台灣票房已達 2.5 億","pic":"78\/5445a8b41b48b1882d94fdd9891ahqy5.JPG"},{"sn":"206032","machine":"動漫畫","title":"《刀劍神域 Progressive 無星之夜的詠嘆調》釋出視覺圖與特報影片 2021 年上映","pic":"76\/a1bdf6ee43d1edde6ac3f850261ahqw5.JPG"},{"sn":"206030","machine":"動漫畫","title":"《角落小夥伴》宣布將推出第二彈動畫電影 預定 2021 年日本上映","pic":"95\/4200054e276ac9e26fcc7bc29b1ahon5.JPG"}],"normal":[{"sn":"206237","machine":"其他","title":"「米滷蛋家族」聯手角色經紀公司卡洛特將展開新企劃 預定 2021 年底推出動畫影集","icon":{"video":"0","player":"0","hot":0}},{"sn":"206188","machine":"其他","title":"聲優歌手 水瀨祈 第 9 張單曲「Starlight Museum」釋出短版 MUSIC CLIP","icon":{"video":"1","player":"heart7153","hot":0}},{"sn":"206178","machine":"其他","title":"「電視動畫鬼滅之刃×京都南座 歌舞伎之舘」在日本登場 展覽現場影片公開","icon":{"video":"1","player":"0","hot":0}},{"sn":"206174","machine":"動漫畫","title":"【書訊】台灣東販 11 月新書《明天將會是晴天》等作","icon":{"video":"0","player":"0","hot":0}},{"sn":"206172","machine":"其他","title":"FX Creations 與《福音戰士》系列宣布展開合作 推出一系列聯名包款","icon":{"video":"0","player":"0","hot":0}},{"sn":"206166","machine":"動漫畫","title":"《七大罪 憤怒的審判》動畫公開主視覺圖與首支宣傳影片 預計 1 月 6 日開播","icon":{"video":"1","player":"moon6533","hot":0}},{"sn":"206190","machine":"其他","title":"原創企劃《電音部》釋出麻布區域「帝音國際女學院」視覺圖與新曲試聽影像","icon":{"video":"1","player":"nk940155","hot":0}},{"sn":"206160","machine":"其他","title":"《LoveLive! Sunshine!!》雙人&amp;三人歌曲 CD WINTER VACATION 釋出收錄曲與試聽影像","icon":{"video":"1","player":"nk940155","hot":0}},{"sn":"206189","machine":"其他","title":"【模型】SKYTUBE「ピロ水 插畫作品」胡川香乃 紅艷 Ver. 預定 4 月發售","icon":{"video":"0","player":"akd00248","hot":0}},{"sn":"206162","machine":"其他","title":"【模型】ANIPLEX+限定《鬼滅之刃》我妻善逸 開箱介紹","icon":{"video":"0","player":"a1950660","hot":0}},{"sn":"206131","machine":"動漫畫","title":" 《海邊的異邦人》即將於 13 日上映 片商宣布將推出戲院特典","icon":{"video":"0","player":"0","hot":0}}]},"pm":{"head":[],"headAdd":[{"sn":"206238","machine":"手機","title":"《最後的克勞迪亞 LAST CLOUDIA》繁中版代理權確定 即日起開放事前登錄","pic":"80\/71ed0787b06f994ea7565871c21aj505.JPG"},{"sn":"206235","machine":"手機","title":"《Fate\/Grand Order》日版推出期間限定活動「虛數大海戰」 全新從者梵谷、尼莫登場","pic":"39\/91f374e361944904b9e9cad5951aj3v5.JPG"},{"sn":"206181","machine":"手機","title":"韓國漫畫改編遊戲《靈魂方舟》雙平台上線 與妲己一起成長並見證仙界大戰的一切故事","pic":"93\/cc36466346442a24cb0c023d4e1aix15.JPG"},{"sn":"206177","machine":"手機","title":"Niantic 將於台南等地區推出「Pokemon GO 城市焦點」計畫 結伴探索鄉土 振興在地觀光","pic":"27\/c1b6f8ed02cf287749f40f80391aiv75.JPG"},{"sn":"206173","machine":"手機","title":"Sensor Tower 公布 2020 年 10 月全球手機遊戲營收排行 《原神》以 2.39 億美元空降第一名","pic":"35\/207fdd53afef275487ca244fcf1a1cv5.JPG"},{"sn":"206211","machine":"手機","title":"節奏遊戲《HoneyWorks Premium Live》將於 11 月 18 日推出 開放日本雙平台預約","pic":"59\/89c902715a3a2168306a2ec6e51aj1n5.JPG"},{"sn":"206197","machine":"手機","title":"《月光雕刻師》首度改版 開啟新地圖「被遺忘的王國之地」與等級上限開放","pic":"00\/9b4fe390160aa35ba9edb84ced1aj005.JPG"},{"sn":"206196","machine":"手機","title":"《Pokémon HOME》即日起支援《Pokemon GO》初次傳送獎勵將贈送「美錄梅塔」","pic":"98\/f3e5786d6d99c35b4450f0a64a1aizy5.JPG"}],"normal":[{"sn":"206194","machine":"手機","title":"《怪物彈珠》x 動畫《七大罪 憤怒的審判》第二彈合作活動 11 月 14 日中午開跑","icon":{"video":"0","player":"0","hot":0}},{"sn":"206193","machine":"手機","title":"休閒新作《魔術洋品店》預計 11 月 17 日於韓國推出 隨心所欲布置自己的魔法商店","icon":{"video":"0","player":"0","hot":0}},{"sn":"206179","machine":"手機","title":"《WOTV FFBE》x《FF X》合作 14 日登場 同日將於日本播出志尊淳、松岡茉優拍攝 CM","icon":{"video":"1","player":"0","hot":0}},{"sn":"206169","machine":"手機","title":"電撃文庫節奏遊戲新作《拜託了，不要讓我回到現實！交響舞台》釋出遊戲玩法介紹","icon":{"video":"1","player":"0","hot":0}},{"sn":"206163","machine":"手機","title":"願原力與你同在《星際大戰：戰機任務》將於 11 月 19 日推出 化身戰機駕駛員遨遊宇宙","icon":{"video":"1","player":"0","hot":0}},{"sn":"206200","machine":"手機","title":"《灌籃高手 SLAM DUNK》版本更新 推出「小隊系統」及「榮耀戰跡」","icon":{"video":"0","player":"0","hot":0}},{"sn":"206224","machine":"手機","title":"MMO《天涯幻夢》事前預約開放 釋出主視覺與首波宣傳影片","icon":{"video":"1","player":"0","hot":0}},{"sn":"206222","machine":"手機","title":"《蒼之騎士團 R》釋出「試煉」玩法 刪檔測試進行中","icon":{"video":"0","player":"0","hot":0}},{"sn":"206220","machine":"手機","title":"《十二之天 M》開放「3 勢力團隊戰」 放下恩怨情仇齊心抵禦外敵","icon":{"video":"0","player":"0","hot":0}},{"sn":"206217","machine":"手機","title":"《新誅仙》射手「烈山」降臨 雙型態新職業來襲","icon":{"video":"1","player":"0","hot":0}},{"sn":"206216","machine":"手機","title":"《King’s Raid – 王之逆襲》 新增 PVP 玩法「試煉的戰場」感恩節任務","icon":{"video":"0","player":"0","hot":0}}]},"show":{"head":[],"headAdd":[{"sn":"206212","machine":"PS5","title":"紀念 PS5 發售！日本 SIE 於秋葉原神田明神神社驚喜舉辦燈光秀活動","pic":"54\/e8f3b22307fa2f971b4047563b1aj1i5.JPG"},{"sn":"206177","machine":"手機","title":"Niantic 將於台南等地區推出「Pokemon GO 城市焦點」計畫 結伴探索鄉土 振興在地觀光","pic":"27\/c1b6f8ed02cf287749f40f80391aiv75.JPG"},{"sn":"206232","machine":"ETC","title":"【TpGS 21】首款桌遊《我的世界：建造者與生物群落》確定推出中文版 近期開放預購","pic":"21\/d4754e3755efd6227131be75831aj3d5.JPG"},{"sn":"206151","machine":"ETC","title":"巴哈姆特歡慶 24 歲生日 要讓玩家「巴幣加倍送，主機週週抽！」","pic":"13\/a652d3e4bebbe968fa7848db911ais15.JPG"},{"sn":"206099","machine":"其他","title":"「亞洲動漫創作展 Petit Fancy 33」活動現場 Cosplay 照片大集合","pic":"24\/8fbb13fc2b641be82fed0555a51aibo5.JPG"},{"sn":"206078","machine":"多平台","title":"「愛麗絲的衣櫃 Alice Closet 展～種村有菜的世界～」11 月 28 日起將於日本登場","pic":"47\/0fddbdb2347b346754ac462e6d1ai3z5.PNG"},{"sn":"206044","machine":"手機","title":"《RO 仙境傳說：新世代的誕生》 於菁桐國小舉辦首屆「波利天燈節」 波利燈海冉冉升空","pic":"45\/64b4b895bd8375f6d6d1e4d1521ahst5.JPG"},{"sn":"205963","machine":"其他","title":"TAMASHII Features 2020 萬代收藏玩具展 正式開展 MB 龍神丸、炎柱日輪刀現場展示中","pic":"63\/5642021fb495fde7e1d26530c31ah735.JPG"}],"normal":[{"sn":"206178","machine":"其他","title":"「電視動畫鬼滅之刃×京都南座 歌舞伎之舘」在日本登場 展覽現場影片公開","icon":{"video":"1","player":"0","hot":0}},{"sn":"206154","machine":"ETC","title":"跨 14 校、共 21 組獨立遊戲團隊與會「異校遊戲大亂鬥」 《細胞迷途》等團隊現場分享經歷","icon":{"video":"0","player":"0","hot":0}},{"sn":"206063","machine":"PC","title":"以拍照為核心玩法新作《攝追赤紅末世代》釋出描述主線兩週前的世界 DLC「Macro」","icon":{"video":"1","player":"0","hot":0}},{"sn":"206084","machine":"手機","title":" 《夢幻海島》線下活動圓滿落幕 現場邀請吳箐箐、周荀 DenKa 等網紅與玩家同樂","icon":{"video":"0","player":"0","hot":0}},{"sn":"205957","machine":"其他","title":"巴哈姆特 24 週年慶簽到加倍送活動預告 更新巴哈姆特 APP 領雙倍巴幣抽新主機","icon":{"video":"0","player":"0","hot":0}},{"sn":"205993","machine":"手機","title":"《陰陽師 Onmyoji》四週年紀念主題特展將於 d\/art 展出","icon":{"video":"0","player":"0","hot":0}},{"sn":"205976","machine":"手機","title":"《sin 七大罪～魔王崇拜～》宣布參展 PF33 邀請 Coser 扮演大罪魔王與玩家同樂","icon":{"video":"0","player":"0","hot":0}},{"sn":"205932","machine":"PS5","title":"SIE 宣布 PlayStation 5 在台上市當天僅提供網路販售 將不舉辦實體店面活動與現貨販售","icon":{"video":"0","player":"0","hot":1}},{"sn":"205866","machine":"XBSX","title":"Xbox Series X | S 台灣首賣會 11\/9 北中南三地同步登場 全球獨家紀念品限量送","icon":{"video":"0","player":"0","hot":0}},{"sn":"205872","machine":"PS5","title":"日本 SIE 發布重要公告表示 PS5 發售日當天將不舉辦店面活動也不提供現貨","icon":{"video":"0","player":"0","hot":1}},{"sn":"205806","machine":"PS5","title":"PS5 亞洲線上特別直播節目雙 11 登場 將邀請「JJ 林俊傑」等台港名人共襄盛舉","icon":{"video":"1","player":"0","hot":0}}]},"prj":{"head":[],"headAdd":[{"sn":"206027","machine":"多平台","title":"【專欄】PS5 VS Xbox Series X | S 運作噪音、溫度大車拼 比比看誰最安靜、冷靜！","pic":"26\/6cf60fa564583ca431eb408a221ahmq5.JPG"},{"sn":"205926","machine":"XBSX","title":"【專欄】Xbox Series X | S 常見問答大彙整！ 解答你想知道的新主機相關疑問","pic":"75\/85c409676d27c9e66d725ce2931a5v35.JPG"},{"sn":"205265","machine":"PS5","title":"【專欄】PlayStation 5 常見問答大彙整！ 解答你想知道的新主機相關疑問","pic":"12\/5ffcff86a73d2bc01f5be7aba21ai5s5.JPG"},{"sn":"204658","machine":"PC","title":"【巴哈ACG20】遊戲組銅賞《艾倫的自動機工坊》團隊專訪 挑戰程式邏輯燒腦思考！","pic":"15\/c95aafcbafae1c67e6c9a483201a77f5.JPG"},{"sn":"204604","machine":"PC","title":"【巴哈ACG20】遊戲組銅賞《地城謎蹤》團隊專訪 以「倉庫番」玩法結合 RPG 要素","pic":"10\/c4abdb29091fa4d796a28c2fd01a6cq5.PNG"},{"sn":"204537","machine":"PC","title":"【巴哈ACG20】遊戲組銀賞《疫原》開發者專訪 釋出爽快動作遊戲試玩版","pic":"90\/838e49fa07b42661cc1910385c1a66m5.PNG"},{"sn":"204530","machine":"動漫畫","title":"【巴哈ACG20】動畫組金賞《上吧！魚》團隊專訪  平民美食「虱目魚」也能出頭天","pic":"97\/ff4c42cf8af29ed2f8f193dab21a6415.JPG"},{"sn":"204519","machine":"動漫畫","title":"【巴哈ACG20】動畫組銅賞《我可以去公園玩嗎?》團隊專訪 將生活經驗融入創作","pic":"72\/c8ba6f9b9a7bc83360cded8c341a5v05.JPG"}],"normal":[{"sn":"204414","machine":"多平台","title":"回顧 2020 年首屆「純線上」東京電玩展 疫情衝擊下的遺憾與現象","icon":{"video":"1","player":"0","hot":1}},{"sn":"204360","machine":"動漫畫","title":"【巴哈ACG20】動畫組銀賞《愚行者 00 THE FOOL》團隊專訪 集結夥伴激盪無限光芒","icon":{"video":"1","player":"0","hot":1}},{"sn":"204233","machine":"PC","title":"【巴哈ACG20】遊戲組金賞《文字遊戲》團隊訪問 文字構成「遠看是圖，近讀是文」","icon":{"video":"0","player":"0","hot":1}},{"sn":"204232","machine":"動漫畫","title":"【巴哈ACG20】漫畫組銀賞《我叫溫索。》作者專訪 以自身或周遭經歷帶入創作中","icon":{"video":"0","player":"0","hot":0}},{"sn":"204089","machine":"動漫畫","title":"【巴哈ACG20】漫畫組銅賞《小姐好正》作者專訪 可愛足以毀滅世界","icon":{"video":"0","player":"0","hot":1}},{"sn":"203613","machine":"動漫畫","title":"【巴哈ACG20】漫畫組銅賞《奇蹟什麼最不可信》作者專訪 創作心中意屬的搞笑漫畫","icon":{"video":"0","player":"0","hot":1}},{"sn":"203454","machine":"動漫畫","title":"【巴哈ACG20】漫畫組金賞《小黃司機的不平凡日常》作者專訪 主婦拾筆重燃繪畫熱情","icon":{"video":"0","player":"0","hot":1}},{"sn":"202795","machine":"ETC","title":"【GNN 大調查】玩到背脊發涼？！玩家心中最恐怖的恐怖遊戲排行結果出爐","icon":{"video":"1","player":"0","hot":1}},{"sn":"201729","machine":"多平台","title":"【專欄】電視遊樂器影音傳輸介面演進與趨勢介紹 數位世代展望篇","icon":{"video":"0","player":"0","hot":1}},{"sn":"201357","machine":"多平台","title":"【GNN 大調查】ACG 角色「最強父親」結果出爐！祝爸爸們父親節快樂！","icon":{"video":"0","player":"0","hot":1}},{"sn":"200637","machine":"多平台","title":"【巴哈大調查】Google Stadia 問卷調查結果出爐 逾 9 成玩家不排斥嘗試 Stadia 串流服務","icon":{"video":"0","player":"0","hot":1}}]},"esports":{"head":[],"headAdd":[{"sn":"206165","machine":"OLG","title":"《英雄聯盟》公開季前 2021 介紹影片 全明星賽票選正式展開","pic":"78\/3630b8a0da836b2ce249401eb91air25.JPG"},{"sn":"206156","machine":"OLG","title":"《英雄聯盟》LCK SB戰隊選手OnFleek因檢舉中國選手時使用種族歧視字眼遭禁賽與罰款","pic":"43\/3723607f868aa8ff85f49374581ainb5.JPG"},{"sn":"206075","machine":"多平台","title":"高雄電競嘉年華 12 月中連三天登場 規劃直播主培訓營、《英雄聯盟》《灌籃高手》等賽事","pic":"44\/8a8e4a56136798a177b52696c31ai3w5.JPG"},{"sn":"206039","machine":"PC","title":"2020 WCG 落幕 《魔獸爭霸 3：淬鍊重生》個人賽  Fly100% 擊敗 Moon 奪冠","pic":"41\/68d751e95f6a26c9dce3603ebc1ahsp5.JPG"},{"sn":"206026","machine":"手機","title":"《Garena 傳說對決》MAD Team 擊敗 FW 閃電狼勇奪 2020 GCS 職業聯賽夏季冠軍","pic":"19\/13319593e2238f987eb8b14d0e1ahmj5.JPG"},{"sn":"206015","machine":"OLG","title":"《英雄聯盟》戰隊 MCX 人事異動 教練 Mountain、選手 M1ssion、PK 等人將不再續約","pic":"36\/da334f86dce7364e549538d74c1ahk85.JPG"},{"sn":"205933","machine":"OLG","title":"《英雄聯盟》2020 全明星賽 12 月登場 分為黑馬與巨星賽事、PCS 將挑戰 LCK","pic":"84\/6330de2b05aefb3c7734407b901ago85.JPG"},{"sn":"205903","machine":"多平台","title":"《絕地求生》PCS3 洲際賽正式開戰 隊伍 GEX、K7 挑戰亞洲強權","pic":"92\/811f70743b670e3120e5789e911agiw5.JPG"}],"normal":[{"sn":"206205","machine":"多平台","title":"2020 創新科技教育暨電競大賽連續五天三創登場 推出多項課程、體驗《黑色行動冷戰》等","icon":{"video":"0","player":"0","hot":0}},{"sn":"206221","machine":"PC","title":"Porsche Design 與 AOC 推出專業電競顯示器「PD27」 搭配多項專為長時間遊戲設計護眼功能","icon":{"video":"0","player":"0","hot":0}},{"sn":"206110","machine":"手機","title":"以「咆嘯吧！最強校園！」為號召《Garena 傳說對決》ACS 冬季賽 11 月 10 日開賽","icon":{"video":"0","player":"0","hot":0}},{"sn":"206142","machine":"PC","title":"HTC 公布 2020 年第三季營運成果 與技嘉推出結合 VIVE Cosmos 的 VR 電競筆電","icon":{"video":"0","player":"0","hot":0}},{"sn":"205987","machine":"手機","title":"《魔靈召喚》SWC 2020 歐洲區決賽本周六壓軸開戰","icon":{"video":"0","player":"0","hot":0}},{"sn":"205854","machine":"OLG","title":"《英雄聯盟》世界亞軍 SN 公告處罰打野選手 SofM 打排位掛機行為 扣除一個月薪水","icon":{"video":"0","player":"0","hot":1}},{"sn":"205930","machine":"多平台","title":"《戰車世界：閃擊戰》亞洲區錦標賽 11 月 6 日線上正式開打","icon":{"video":"1","player":"0","hot":0}},{"sn":"205911","machine":"手機","title":"《第五人格》萬聖狂歡盃電競賽事冠軍出爐 「RD x GB」奪冠 釋出賽後專訪","icon":{"video":"0","player":"0","hot":0}},{"sn":"205909","machine":"PC","title":"以直升機為設計概念「AH T200 小型強化玻璃機殼」11 月下旬在台上市","icon":{"video":"0","player":"0","hot":0}},{"sn":"205901","machine":"多平台","title":"WCG 2020 世界大賽總決賽今起登場 競逐《魔獸爭霸 3：淬鍊重生》等四項目","icon":{"video":"0","player":"0","hot":0}},{"sn":"205899","machine":"多平台","title":"十銓科技推出 T-FORCE TREASURE Touch 外接式 RGB 固態硬碟 可支援多平台使用","icon":{"video":"0","player":"0","hot":0}}]}};

action = (1 == action) ? 1 : 0;
if (1 == action) { //onclick
if (gnnwho == who && 1 == gnnwhoc) {
if ('all' == who) {
location.href = '//gnn.gamer.com.tw/';
} else {
if (typeof(more[who]) !== 'undefined') {
location.href = '//gnn.gamer.com.tw/?k='+more[who];
}
}
return;
}
}

var useAry = list[who].head;
var hc = list[who].head.length, hac = list[who].headAdd.length;
var now = 0;

if (hc < ghc) {
if ((hc + hac) > ghc) {
var start = Math.floor(Math.random() * hac);

for (i = 0; i < (ghc - hc); i++) {
now = start + i;
if (now >= hac) {
now -= hac;
}

useAry.push(list[who].headAdd[now]);
}
} else {
useAry = useAry.concat(list[who].headAdd);
}
}

now = 0;
var head = '', style = '';
var gtmconfig = ('all' === who) ? ' data-gtmgamer="GNN頭圖"' : '';
var COLUMNS = 4;
var ROWS = Math.ceil(useAry.length / COLUMNS);
var dpr = Math.min(Math.round(window.devicePixelRatio), 2);

for (i = 0; i < ROWS; i++) {
head += '<div class="BA-cbox BA-cbox2">';

for (j = 0; j < COLUMNS; j++) {
if (j == COLUMNS - 1) {
style += ' style="border:none;"';
}

var resizeStr = '';
if (dpr != 2) {
resizeStr = '?w=300';
}

head += '<div'+style+'>\
<a href="//gnn.gamer.com.tw/detail.php?sn='+useAry[now].sn+'"'+gtmconfig+'>\
<span>'+useAry[now].machine+'</span>\
<img src="//p2.bahamut.com.tw/S/2KU/' + useAry[now].pic + resizeStr + '" /></a>\
<a href="//gnn.gamer.com.tw/detail.php?sn='+useAry[now].sn+'"'+gtmconfig+'>'+useAry[now].title+'</a>\
</div>';

now++;
}

head += '</div>';
}

var normal = '';
var gtmconfig2 = ('all' === who) ? ' data-gtmgamer="GNN頭文"' : '';
for (i=0; i<list[who].normal.length; i++) {
normal += '<p>\
<span>'+list[who].normal[i].machine+'</span>\
<a href="//gnn.gamer.com.tw/detail.php?sn='+list[who].normal[i].sn+'"'+gtmconfig2+'>'+list[who].normal[i].title+'</a>';

if ('0' != list[who].normal[i].icon.video) {
normal += '<img src="https://i2.bahamut.com.tw/gnn/movie.gif" />';
}

if ('0' != list[who].normal[i].icon.player) {
normal += '<img src="https://i2.bahamut.com.tw/gnn/editor2.gif" title="'+list[who].normal[i].icon.player+ '" />';
}

if ('0' != list[who].normal[i].icon.hot) {
normal += '<img src="https://i2.bahamut.com.tw/gnn/hot.gif" />';
}

normal += '</p>';
}

normal += '<div><a href="//home.gamer.com.tw/creationDetail.php?sn=434951" target="_blank">&gt; 投稿</a>';

if (typeof(more[who]) !== 'undefined') {
normal += '<a href="//gnn.gamer.com.tw/?k='+more[who]+'">&gt; 更多'+morecontx[who]+'</a></div>';
}

$('#gnn_head').html(head);
$('#gnn_normal').html(normal);

$('.BA-ctag1now').filter('[id^="gnn_"]').removeClass('BA-ctag1now');
$('#gnn_'+who).addClass('BA-ctag1now');

if( 'show' != who && 1 == gnnshowing ){
$('#gnn_show').addClass('show');
}

gnnwho = who;
gnnwhoflag = setTimeout(function(){ gnnwhoc=1; }, 200);
}
};

window.GNN_HEAD = gnn;
})(window, jQuery);
</script>

<ul class="BA-ctag1">
<li><a id="gnn_all" href="javascript:void(0)" class="BA-ctag1now" onclick="GNN_HEAD.gnn_changea('all', 1)" onmouseover="GNN_HEAD.gnn_change('all')" onmouseout="clearTimeout(gnnflag)">GNN 新聞</a></li>
<li><a id="gnn_pm" href="javascript:void(0)" onclick="GNN_HEAD.gnn_changea('pm', 1)" onmouseover="GNN_HEAD.gnn_change('pm')" onmouseout="clearTimeout(gnnflag)">手機遊戲</a></li>
<li><a id="gnn_pc" href="javascript:void(0)" onclick="GNN_HEAD.gnn_changea('pc', 1)" onmouseover="GNN_HEAD.gnn_change('pc')" onmouseout="clearTimeout(gnnflag)">PC</a></li>
<li><a id="gnn_tv" href="javascript:void(0)" onclick="GNN_HEAD.gnn_changea('tv', 1)" onmouseover="GNN_HEAD.gnn_change('tv')" onmouseout="clearTimeout(gnnflag)">TV 掌機</a></li>
<li><a id="gnn_ac" href="javascript:void(0)" onclick="GNN_HEAD.gnn_changea('ac', 1)" onmouseover="GNN_HEAD.gnn_change('ac')" onmouseout="clearTimeout(gnnflag)">動漫畫</a></li>
<li><a id="gnn_esports" href="javascript:void(0)" onclick="GNN_HEAD.gnn_changea('esports', 1)" onmouseover="GNN_HEAD.gnn_change('esports')" onmouseout="clearTimeout(gnnflag)">電競</a></li>
<li><a id="gnn_show" href="javascript:void(0)" onclick="GNN_HEAD.gnn_changea('show', 1)" onmouseover="GNN_HEAD.gnn_change('show')" onmouseout="clearTimeout(gnnflag)">活動展覽</a></li>
<li><a id="gnn_prj" href="javascript:void(0)" onclick="GNN_HEAD.gnn_changea('prj', 1)" onmouseover="GNN_HEAD.gnn_change('prj')" onmouseout="clearTimeout(gnnflag)">主題報導</a></li>
</ul>

<div id="gnn_head">
</div>

<div id="gnn_normal" class="BA-cbox BA-cbox3">
</div>
<script>
GNN_HEAD.gnn_changea('all', 0);
</script>
<script>
(function(window, $, undefined) {
'use strict';

var _ad = [];
if (_ad.length) {
document.write('<div class="BA-cboxAD">' + AdBaha.output(_ad, 14) + '</div>');
}
})(window, jQuery);
</script>
<ul id="CrazyTab" class="BA-ctag1">
</ul>
<div id="CrazyBlock"></div>

<script>
var gamecrazytimer;
(function(window, $, undefined) {
var gamecrazy = {"setting":[{"boxtitle":"\u96fb\u73a9\u760b","btime":"2020-01-01 00:00:00","etime":"2021-12-31 23:59:59"}],"video":[{"bno":"2,1","id":"DoBBSba-Tz0","number":"1","title":"\u300a\u84bc\u4e4b\u9a0e\u58eb\u5718 R\u300b\u672a\u4e0a\u5e02\u624b\u6a5f\u904a\u6232 \u7d93\u5178\u7b56\u7565 RPG \u65b0\u4f5c\u91cd\u73fe","ad_btime":"2020-11-11 15:54:20","ad_etime":"2030-12-31 23:59:59","cover":"https:\/\/i.ytimg.com\/vi\/DoBBSba-Tz0\/maxresdefault.jpg","area":"youtube","viewer":"420","islive":"0"},{"bno":"2,2","id":"McCYXDgEjg0","number":"2","title":"\u300a\u9f8d\u4e4b\u8c37\uff1a\u65b0\u4e16\u754c\u300b\u672a\u4e0a\u5e02\u624b\u6a5f\u904a\u6232 \u300a\u9f8d\u4e4b\u8c37\u300bIP \u7cfb\u5217\u65b0\u4f5c","ad_btime":"2020-11-11 15:54:20","ad_etime":"2030-12-31 23:59:59","cover":"https:\/\/i.ytimg.com\/vi\/McCYXDgEjg0\/maxresdefault.jpg","area":"youtube","viewer":"306","islive":"0"},{"bno":"2,3","id":"GQ58CyF4VnY","number":"3","title":"\u300a\u92fc\u5f48\u722d\u92d2\u5c0d\u6c7a\u300b\u672a\u4e0a\u5e02\u624b\u6a5f\u904a\u6232\u30003D \u92fc\u5f48\u52d5\u4f5c\u5c0d\u6230\u904a\u6232","ad_btime":"2020-11-11 15:54:20","ad_etime":"2030-12-31 23:59:59","cover":"https:\/\/i.ytimg.com\/vi\/GQ58CyF4VnY\/maxresdefault.jpg","area":"youtube","viewer":"438","islive":"0"},{"bno":"2,4","id":"NL4Uocf7ipY","number":"4","title":"\u300aTera\uff1aEndless War\u300b\u624b\u6a5f\u904a\u6232 \u300aTERA\u300b\u6539\u7de8\u7684 SLG \u8207\u591a\u540d\u82f1\u96c4\u5171\u540c\u4f5c\u6230","ad_btime":"2020-11-11 15:54:20","ad_etime":"2030-12-31 23:59:59","cover":"https:\/\/i.ytimg.com\/vi\/NL4Uocf7ipY\/maxresdefault.jpg","area":"youtube","viewer":"757","islive":"0"},{"bno":"2,5","id":"bGeDsEpP8I0","number":"5","title":"\u3010\u76f4\u64ad\u3011\u300a\u523a\u5ba2\u6559\u689d\uff1a\u7dad\u4eac\u7d00\u5143\u300b\u7b2c\u4e00\u6b21 Xbox Series X \u76f4\u64ad\u73fe\u8eab\uff01","ad_btime":"2020-11-11 15:54:20","ad_etime":"2030-12-31 23:59:59","cover":"https:\/\/i.ytimg.com\/vi\/bGeDsEpP8I0\/maxresdefault.jpg","area":"youtube","viewer":"38141","islive":"0"},{"bno":"2,6","id":"y7mh6XXTnBE","number":"6","title":"PS5 \u8207 Xbox Series X \u5230\u5e95\u8ab0\u8072\u97f3\u5927\u8ab0\u6bd4\u8f03\u71b1\uff01\u4e00\u6b21\u5be6\u6e2c\u7d66\u4f60\u770b\uff01","ad_btime":"2020-11-11 10:25:40","ad_etime":"2030-12-31 23:59:59","cover":"https:\/\/i.ytimg.com\/vi\/y7mh6XXTnBE\/maxresdefault.jpg","area":"youtube","viewer":"19954","islive":"0"},{"bno":"2,7","id":"lF-Cs3V9D7E","number":"7","title":"PS5 \u4e3b\u6a5f\u8207 DualSense \u63a7\u5236\u5668\u7d42\u65bc\u958b\u7bb1\u5566\uff01\u6700\u8a73\u7d30\u7684\u89e3\u8aaa\u8207\u6b77\u4ee3\u4e3b\u6a5f\u6392\u6392\u7ad9\uff01","ad_btime":"2020-11-07 00:10:07","ad_etime":"2030-12-31 23:59:59","cover":"https:\/\/i.ytimg.com\/vi\/lF-Cs3V9D7E\/maxresdefault.jpg","area":"youtube","viewer":"127083","islive":"0"},{"bno":"2,8","id":"yt3ousZiaDM","number":"8","title":"PS5 \u9996\u767c\u904a\u6232\u60c5\u5831\u6574\u7406\uff01\u4e00\u4e0a\u5e02\u5c31\u53ef\u4ee5\u73a9\u7206\u5566\uff01","ad_btime":"2020-10-30 19:21:03","ad_etime":"2030-12-31 23:59:59","cover":"https:\/\/i.ytimg.com\/vi\/yt3ousZiaDM\/maxresdefault.jpg","area":"youtube","viewer":"41040","islive":"0"},{"bno":"2,9","id":"EnZ6JS0csfQ","number":"9","title":"Xbox Series X|S \u9996\u767c\u904a\u6232\u60c5\u5831\u6574\u7406\uff01\u5225\u5fd8\u4e86\u9084\u6709\u73a9\u5230\u7206\u7684 Game Pass \u5594","ad_btime":"2020-11-05 10:29:16","ad_etime":"2030-12-31 23:59:59","cover":"https:\/\/i.ytimg.com\/vi\/EnZ6JS0csfQ\/maxresdefault.jpg","area":"youtube","viewer":"16142","islive":"0"},{"bno":"2,10","id":"XkUVVL6E61s","number":"10","title":"2020 \u5e74 11 \u6708\u671f\u5f85\u904a\u6232\u60c5\u5831\u6574\u7406\u300a\u85a9\u723e\u9054\u7121\u96d9 \u707d\u5384\u555f\u793a\u9304\u300b\u300a\u60e1\u9b54\u9748\u9b42\u300b\u91cd\u88fd\u7248\u300a\u523a\u5ba2\u6559\u689d\uff1a\u7dad\u4eac\u7d00\u5143\u300b","ad_btime":"2020-10-30 19:21:03","ad_etime":"2030-12-31 23:59:59","cover":"https:\/\/i.ytimg.com\/vi\/XkUVVL6E61s\/maxresdefault.jpg","area":"youtube","viewer":"6106","islive":"0"},{"bno":"2,11","id":"wCMc0Gl0aco","number":"11","title":"2020 \u4e0b\u534a\u5e74\u5341\u5927\u671f\u5f85\u624b\u6a5f\u904a\u6232","ad_btime":"2020-09-04 22:20:29","ad_etime":"2030-12-31 23:59:59","cover":"https:\/\/i.ytimg.com\/vi\/wCMc0Gl0aco\/maxresdefault.jpg","area":"youtube","viewer":"430639","islive":"0"},{"bno":"2,12","id":"IqguwxUZNok","number":"12","title":"\u5fae\u8edf\u7684\u6700\u5f37\u5e95\u724c Xbox Game Pass\uff01NT.30 \u66a2\u73a9\u300a\u60e1\u9748\u53e4\u5821 7\u300b\u300a\u4eba\u4e2d\u4e4b\u9f8d\u300b\u7cfb\u5217\u3001\u300a\u6700\u5f8c\u4e00\u6230\u300b\u7cfb\u5217","ad_btime":"2020-10-05 10:16:51","ad_etime":"2030-12-31 23:59:59","cover":"https:\/\/i.ytimg.com\/vi\/IqguwxUZNok\/maxresdefault.jpg","area":"youtube","viewer":"25775","islive":"0"},{"bno":"2,13","id":"ouDH33uwT1U","number":"13","title":"PS5 VS XboxSeriesX \u65b0\u4e16\u4ee3\u4e3b\u6a5f\u8a55\u6bd4 \u8ab0\u662f CP \u503c\u4e4b\u738b?!","ad_btime":"2020-10-05 10:16:51","ad_etime":"2030-12-31 23:59:59","cover":"https:\/\/i.ytimg.com\/vi\/ouDH33uwT1U\/maxresdefault.jpg","area":"youtube","viewer":"95866","islive":"0"},{"bno":"2,14","id":"zqjnQM8uCkU","number":"14","title":"2020 \u4e0b\u534a\u5e74\u5341\u5927\u671f\u5f85\u904a\u6232","ad_btime":"2020-08-19 12:07:51","ad_etime":"2030-12-31 23:59:59","cover":"https:\/\/i.ytimg.com\/vi\/zqjnQM8uCkU\/maxresdefault.jpg","area":"youtube","viewer":"283692","islive":"0"}],"videotop":[{"bno":"3,1","id":"n5nsrgmf8pk","number":"1","title":"\u30104K\u76f4\u64ad\u3011 PS5\u300a\u6f2b\u5a01\u8718\u86db\u4eba\uff1a\u9081\u723e\u65af\u6469\u62c9\u65af\u300b\u5c55\u73fe\u5f37\u5927\u6027\u80fd\u5149\u8ffd\u7279\u6548\u7684\u904a\u6232\u4f86\u5566\uff01","ad_btime":"2020-11-11 00:00:00","ad_etime":"2020-11-13 00:00:00","cover":"https:\/\/i.ytimg.com\/vi\/n5nsrgmf8pk\/maxresdefault.jpg","area":"youtube","viewer":"23127","islive":"0"}]};
var gamecrazyAd = gamecrazy.ad;
var gamecrazyVideoTop = gamecrazy.videotop || [];
var gamecrazyVideoGeneral = gamecrazy.video;
var setting = gamecrazy.setting;

var html1 = '', len = 0, pit = 3;

var tab = '<li><a class="BA-ctag1now" onclick="GAMECRAZY_LIVE.gamecrazyBlockT();" onmouseover="GAMECRAZY_LIVE.gamecrazyBlockT();" onmouseout="clearTimeout(gamecrazytimer)">'+setting[0].boxtitle+'</a></li>';
$('#CrazyTab').html(tab);

//general random
if (setting[0].btime <= '2020-11-12 10:49:56' && setting[0].etime >= '2020-11-12 10:49:56') {
for (var i = 0; i < gamecrazyVideoGeneral.length - 1; i++) {
var j = i+ Math.floor(Math.random() * (gamecrazyVideoGeneral.length-i));
var temp = gamecrazyVideoGeneral[j];
gamecrazyVideoGeneral[j] = gamecrazyVideoGeneral[i];
gamecrazyVideoGeneral[i] = temp;
}
}

//merge top & general
var gamecrazyVideo = jQuery.merge(jQuery.merge([], gamecrazyVideoTop), gamecrazyVideoGeneral);
for (var i = 0; i < gamecrazyVideo.length - 1; i++) {
if (gamecrazyVideo[i].ad_btime > '2020-11-12 10:49:56' || gamecrazyVideo[i].ad_etime < '2020-11-12 10:49:56') {
gamecrazyVideo.splice(i, 1);
i--;
}
}

len = gamecrazyVideo.length;
//ad
if (gamecrazyAd) {
for (var j = 0; j < gamecrazyAd.length; j++) {
var bno = gamecrazyAd[j].bno.split(",");
if (gamecrazyAd[j].area == 'twitch') {
var url = 'https://gnn.gamer.com.tw/gamecrazy.php?k=12&gt=T';
} else if (gamecrazyAd[j].area == 'youtube') {
var url = 'https://gnn.gamer.com.tw/gamecrazy.php?vid='+gamecrazyAd[j].id;
} else { //banner
var url = gamecrazyAd[j].id;
}

if (gamecrazyAd[j].ad_btime <= '2020-11-12 10:49:56' && gamecrazyAd[j].ad_etime >= '2020-11-12 10:49:56') {
html1 += '  <div class="liveAction">';
html1 += '    <span class="livead-title">'+gamecrazyAd[j].adtitle+'</span>';
html1 += '    <a class="a-mercy-d" data-ssn="'+bno[1]+'" data-gtm="電玩瘋" href="'+url+'">';
if (gamecrazyAd[j].islive == 1) {
html1 += '      <div class="spectator">';
html1 += '        <p class="is-live-text">直播中</p>';
html1 += '        <i class="material-icons">person</i>';
html1 += '        <p class="people">'+gamecrazyAd[j].viewer+'</p>';
html1 += '      </div>';
} else {
if (gamecrazyAd[j].viewer != 0) {
html1 += '      <div class="spectator">';
html1 += '        <i class="material-icons">visibility</i>';
html1 += '        <p class="people">'+gamecrazyAd[j].viewer+'</p>';
html1 += '      </div>';
}
}
html1 += '    <img class="is-livead-img" src="'+gamecrazyAd[j].cover+'">';
html1 += '    <div class="live-adlight"></div>';
html1 += '    </a>';
html1 += '    <a class="is-livead a-mercy-d" data-ssn="'+bno[1]+'" data-gtm="電玩瘋" href="'+url+'">'+gamecrazyAd[j].title+'</a>';
html1 += '  </div>';

len = len + 1;
pit = pit - 1;
}

if (!gamecrazyAd[j]) {
break;
}
}
}


live = {
gamecrazyBlockT: function(s) {
gamecrazytimer = setTimeout(function(){ GAMECRAZY_LIVE.gamecrazyBlock(s); }, 200);
},

gamecrazyBlock: function(s) {
var livedata = [], html = '';

html += '<div class="BA-cbox BA-cbox9A">';

if (4 > len) {
livedata = gamecrazyVideo;
if (pit > livedata.length) {
pit = livedata.length;
}
} else {
while (livedata.length < pit) {
var tmp = gamecrazyVideo.shift();
livedata.push(tmp);
gamecrazyVideo.push(tmp);
}
}
html += html1;

for (var j = 0; j < pit; j++) {
if (livedata[j].area == 'twitch') {
var url = 'https://gnn.gamer.com.tw/gamecrazy.php?k=12&gt=T';
} else if (livedata[j].area == 'youtube') {
var url = 'https://gnn.gamer.com.tw/gamecrazy.php?vid='+livedata[j].id;
}
html += '  <div class="liveAction">';
html += '    <a data-gtm="電玩瘋" href="'+url+'">';
if (livedata[j].islive == 1) {
html += '      <div class="spectator">';
html += '        <p class="is-live-text">直播中</p>';
html += '        <i class="material-icons">person</i>';
html += '        <p class="people">'+livedata[j].viewer+'</p>';
html += '      </div>';
} else {
if (livedata[j].viewer != 0) {
html += '      <div class="spectator">';
html += '        <i class="material-icons">visibility</i>';
html += '        <p class="people">'+livedata[j].viewer+'</p>';
html += '      </div>';
}
}
html += '    <img src="'+livedata[j].cover+'">';
html += '    </a>';
html += '    <a class="is-livead" data-gtm="電玩瘋" href="'+url+'">'+livedata[j].title+'</a>';
html += '  </div>';

if (!livedata[j]) {
break;
}
}

html += '</div>';

html += '<p class="BA-cbox9C"><a href="//gnn.gamer.com.tw/gamecrazy.php">&gt; 看更多</a></p>';

$("#CrazyBlock").html(html);
}
};

window.GAMECRAZY_LIVE = live;
})(window, jQuery);
</script>
<script>GAMECRAZY_LIVE.gamecrazyBlock('live');</script>
<script>
(function(window, $, undefined){
var chart = {
mobileGameBoard: function(tabs, _this) {
$(_this).closest("ul").find('.BA-ctag1now').removeClass('BA-ctag1now');
$(_this).addClass('BA-ctag1now');

if( tabs == 'growth' ) {
$("#gamechart-growth").show();
$("#gamechart-hot").hide();
} else {
$("#gamechart-growth").hide();
$("#gamechart-hot").show();
}
}
};

window.MobileChart = chart;
})(window, jQuery);
</script>
<ul class="BA-ctag1">
<li><a href="javascript:;" onmouseover="MobileChart.mobileGameBoard('growth', this)" class="BA-ctag1now">Android 成長榜</a></li>
<li><a href="javascript:;" onmouseover="MobileChart.mobileGameBoard('hot', this)">Android 安裝榜</a></li>
</ul>
<div id="gamechart-growth" class="BA-cbox BA-cbox5 BA-mobilegamechart">
<table style="FLOAT: left"><tr>
<td width="20" align="middle"><span>1</span></td>
<td>
<a href="//acg.gamer.com.tw/acgDetail.php?s=113076" class="growth" data-gtmgamer="手遊榜Android"><p class="game">吉他少女</p></a><p class="game-data"><i class="fa fa-long-arrow-up" aria-hidden="true"></i> 0.42%</p>
</td>
<td align=right><a href="//forum.gamer.com.tw/searchb.php?dc_c1=37497&dc_c2=1&dc_type=1&dc_machine=16777216,0" class="go-forum" data-gtmgamer="手遊榜哈啦區">哈啦區</a></td>
</tr><tr>
<td width="20" align="middle"><span>2</span></td>
<td>
<a href="//acg.gamer.com.tw/acgDetail.php?s=113872" class="growth" data-gtmgamer="手遊榜Android"><p class="game">如果重來</p></a><p class="game-data"><i class="fa fa-long-arrow-up" aria-hidden="true"></i> 0.32%</p>
</td>
<td align=right><a href="//forum.gamer.com.tw/searchb.php?dc_c1=37746&dc_c2=1&dc_type=1&dc_machine=16777216,0" class="go-forum" data-gtmgamer="手遊榜哈啦區">哈啦區</a></td>
</tr><tr>
<td width="20" align="middle"><span>3</span></td>
<td>
<a href="//acg.gamer.com.tw/acgDetail.php?s=59910" class="growth" data-gtmgamer="手遊榜Android"><p class="game">神魔之塔</p></a><p class="game-data"><i class="fa fa-long-arrow-up" aria-hidden="true"></i> 0.17%</p>
</td>
<td align=right><a href="//forum.gamer.com.tw/searchb.php?dc_c1=18743&dc_c2=1&dc_type=1&dc_machine=16777216,0" class="go-forum" data-gtmgamer="手遊榜哈啦區">哈啦區</a></td>
</tr><tr>
<td width="20" align="middle"><span>4</span></td>
<td>
<a href="//acg.gamer.com.tw/acgDetail.php?s=102435" class="growth" data-gtmgamer="手遊榜Android"><p class="game">A3: STILL ALIVE 倖存者</p></a><p class="game-data"><i class="fa fa-long-arrow-up" aria-hidden="true"></i> 0.16%</p>
</td>
<td align=right><a href="//forum.gamer.com.tw/searchb.php?dc_c1=33559&dc_c2=1&dc_type=1&dc_machine=16777216,0" class="go-forum" data-gtmgamer="手遊榜哈啦區">哈啦區</a></td>
</tr><tr>
<td width="20" align="middle"><span>5</span></td>
<td>
<a href="//acg.gamer.com.tw/acgDetail.php?s=113291" class="growth" data-gtmgamer="手遊榜Android"><p class="game">Tsum Tsum Stadium</p></a><p class="game-data"><i class="fa fa-long-arrow-up" aria-hidden="true"></i> 0.09%</p>
</td>
<td align=right><a href="//forum.gamer.com.tw/searchb.php?dc_c1=37570&dc_c2=1&dc_type=1&dc_machine=16777216,0" class="go-forum" data-gtmgamer="手遊榜哈啦區">哈啦區</a></td>
</tr></table><table style="FLOAT: right"><tr>
<td width="20" align="middle"><span>6</span></td>
<td>
<a href="//acg.gamer.com.tw/acgDetail.php?s=93874" class="growth" data-gtmgamer="手遊榜Android"><p class="game">JUMPUTI HEROES 英雄氣泡</p></a><p class="game-data"><i class="fa fa-long-arrow-up" aria-hidden="true"></i> 0.06%</p>
</td>
<td align=right><a href="//forum.gamer.com.tw/searchb.php?dc_c1=31230&dc_c2=1&dc_type=1&dc_machine=16777216,0" class="go-forum" data-gtmgamer="手遊榜哈啦區">哈啦區</a></td>
</tr><tr>
<td width="20" align="middle"><span>7</span></td>
<td>
<a href="//acg.gamer.com.tw/acgDetail.php?s=68282" class="growth" data-gtmgamer="手遊榜Android"><p class="game">LINE Rangers 銀河特攻隊</p></a><p class="game-data"><i class="fa fa-long-arrow-up" aria-hidden="true"></i> 0.04%</p>
</td>
<td align=right><a href="//forum.gamer.com.tw/searchb.php?dc_c1=21587&dc_c2=1&dc_type=1&dc_machine=16777216,0" class="go-forum" data-gtmgamer="手遊榜哈啦區">哈啦區</a></td>
</tr><tr>
<td width="20" align="middle"><span>8</span></td>
<td>
<a href="//acg.gamer.com.tw/acgDetail.php?s=107348" class="growth" data-gtmgamer="手遊榜Android"><p class="game">LINE：哆啦 A 夢樂園</p></a><p class="game-data"><i class="fa fa-long-arrow-up" aria-hidden="true"></i> 0.04%</p>
</td>
<td align=right><a href="//forum.gamer.com.tw/searchb.php?dc_c1=35401&dc_c2=1&dc_type=1&dc_machine=16777216,0" class="go-forum" data-gtmgamer="手遊榜哈啦區">哈啦區</a></td>
</tr><tr>
<td width="20" align="middle"><span>9</span></td>
<td>
<a href="//acg.gamer.com.tw/acgDetail.php?s=85288" class="growth" data-gtmgamer="手遊榜Android"><p class="game">跑跑薑餅人：烤箱大逃亡</p></a><p class="game-data"><i class="fa fa-long-arrow-up" aria-hidden="true"></i> 0.02%</p>
</td>
<td align=right><a href="//forum.gamer.com.tw/searchb.php?dc_c1=27951&dc_c2=1&dc_type=1&dc_machine=16777216,0" class="go-forum" data-gtmgamer="手遊榜哈啦區">哈啦區</a></td>
</tr><tr>
<td width="20" align="middle"><span>10</span></td>
<td>
<a href="//acg.gamer.com.tw/acgDetail.php?s=79477" class="growth" data-gtmgamer="手遊榜Android"><p class="game">Pokemon GO</p></a><p class="game-data"><i class="fa fa-long-arrow-up" aria-hidden="true"></i> 0.02%</p>
</td>
<td align=right><a href="//forum.gamer.com.tw/searchb.php?dc_c1=26295&dc_c2=1&dc_type=1&dc_machine=16777216,0" class="go-forum" data-gtmgamer="手遊榜哈啦區">哈啦區</a></td>
</tr></table><p class="BA-cbox8A" style="padding: 5px; min-height: 0;"></p>
</div>

<div id="gamechart-hot" class="BA-cbox BA-cbox5 BA-mobilegamechart" style="display:none;">
<table style="FLOAT: left"><tr>
<td width="20" align="middle"><span>1</span></td>
<td>
<a href="//acg.gamer.com.tw/acgDetail.php?s=83801" class="hotgame" data-gtmgamer="手遊榜Android"><p class="game">Garena 傳說對決</p></a><p class="install-data"><i class="fa fa-child" aria-hidden="true"></i> 21.44%</p>
</td>
<td align="right"><a href="//forum.gamer.com.tw/searchb.php?dc_c1=27371&dc_c2=1&dc_type=1&dc_machine=16777216,0" class="go-forum" data-gtmgamer="手遊榜哈啦區">哈啦區</a></td>
</tr><tr>
<td width="20" align="middle"><span>2</span></td>
<td>
<a href="//acg.gamer.com.tw/acgDetail.php?s=59910" class="hotgame" data-gtmgamer="手遊榜Android"><p class="game">神魔之塔</p></a><p class="install-data"><i class="fa fa-child" aria-hidden="true"></i> 15.8%</p>
</td>
<td align="right"><a href="//forum.gamer.com.tw/searchb.php?dc_c1=18743&dc_c2=1&dc_type=1&dc_machine=16777216,0" class="go-forum" data-gtmgamer="手遊榜哈啦區">哈啦區</a></td>
</tr><tr>
<td width="20" align="middle"><span>3</span></td>
<td>
<a href="//acg.gamer.com.tw/acgDetail.php?s=79477" class="hotgame" data-gtmgamer="手遊榜Android"><p class="game">Pokemon GO</p></a><p class="install-data"><i class="fa fa-child" aria-hidden="true"></i> 15.07%</p>
</td>
<td align="right"><a href="//forum.gamer.com.tw/searchb.php?dc_c1=26295&dc_c2=1&dc_type=1&dc_machine=16777216,0" class="go-forum" data-gtmgamer="手遊榜哈啦區">哈啦區</a></td>
</tr><tr>
<td width="20" align="middle"><span>4</span></td>
<td>
<a href="//acg.gamer.com.tw/acgDetail.php?s=113722" class="hotgame" data-gtmgamer="手遊榜Android"><p class="game">Among Us</p></a><p class="install-data"><i class="fa fa-child" aria-hidden="true"></i> 12.77%</p>
</td>
<td align="right"><a href="//forum.gamer.com.tw/searchb.php?dc_c1=37693&dc_c2=1&dc_type=1&dc_machine=16777216,0" class="go-forum" data-gtmgamer="手遊榜哈啦區">哈啦區</a></td>
</tr><tr>
<td width="20" align="middle"><span>5</span></td>
<td>
<a href="//acg.gamer.com.tw/acgDetail.php?s=106884" class="hotgame" data-gtmgamer="手遊榜Android"><p class="game">RO 仙境傳說：新世代的誕生</p></a><p class="install-data"><i class="fa fa-child" aria-hidden="true"></i> 11.34%</p>
</td>
<td align="right"><a href="//forum.gamer.com.tw/searchb.php?dc_c1=35227&dc_c2=1&dc_type=1&dc_machine=16777216,0" class="go-forum" data-gtmgamer="手遊榜哈啦區">哈啦區</a></td>
</tr></table><table style="FLOAT: right"><tr>
<td width="20" align="middle"><span>6</span></td>
<td>
<a href="//acg.gamer.com.tw/acgDetail.php?s=84777" class="hotgame" data-gtmgamer="手遊榜Android"><p class="game">超異域公主連結☆Re:Dive</p></a><p class="install-data"><i class="fa fa-child" aria-hidden="true"></i> 9%</p>
</td>
<td align="right"><a href="//forum.gamer.com.tw/searchb.php?dc_c1=27770&dc_c2=1&dc_type=1&dc_machine=16777216,0" class="go-forum" data-gtmgamer="手遊榜哈啦區">哈啦區</a></td>
</tr><tr>
<td width="20" align="middle"><span>7</span></td>
<td>
<a href="//acg.gamer.com.tw/acgDetail.php?s=92735" class="hotgame" data-gtmgamer="手遊榜Android"><p class="game">PUBG MOBILE：絕地求生 M</p></a><p class="install-data"><i class="fa fa-child" aria-hidden="true"></i> 8.71%</p>
</td>
<td align="right"><a href="//forum.gamer.com.tw/searchb.php?dc_c1=30801&dc_c2=1&dc_type=1&dc_machine=16777216,0" class="go-forum" data-gtmgamer="手遊榜哈啦區">哈啦區</a></td>
</tr><tr>
<td width="20" align="middle"><span>8</span></td>
<td>
<a href="//acg.gamer.com.tw/acgDetail.php?s=105799" class="hotgame" data-gtmgamer="手遊榜Android"><p class="game">弓箭傳說</p></a><p class="install-data"><i class="fa fa-child" aria-hidden="true"></i> 8.67%</p>
</td>
<td align="right"><a href="//forum.gamer.com.tw/searchb.php?dc_c1=34856&dc_c2=1&dc_type=1&dc_machine=16777216,0" class="go-forum" data-gtmgamer="手遊榜哈啦區">哈啦區</a></td>
</tr><tr>
<td width="20" align="middle"><span>9</span></td>
<td>
<a href="//acg.gamer.com.tw/acgDetail.php?s=71788" class="hotgame" data-gtmgamer="手遊榜Android"><p class="game">Fate/Grand Order</p></a><p class="install-data"><i class="fa fa-child" aria-hidden="true"></i> 8.06%</p>
</td>
<td align="right"><a href="//forum.gamer.com.tw/searchb.php?dc_c1=22734&dc_c2=1&dc_type=1&dc_machine=16777216,0" class="go-forum" data-gtmgamer="手遊榜哈啦區">哈啦區</a></td>
</tr><tr>
<td width="20" align="middle"><span>10</span></td>
<td>
<a href="//acg.gamer.com.tw/acgDetail.php?s=66219" class="hotgame" data-gtmgamer="手遊榜Android"><p class="game">怪物彈珠</p></a><p class="install-data"><i class="fa fa-child" aria-hidden="true"></i> 8.01%</p>
</td>
<td align="right"><a href="//forum.gamer.com.tw/searchb.php?dc_c1=20272&dc_c2=1&dc_type=1&dc_machine=16777216,0" class="go-forum" data-gtmgamer="手遊榜哈啦區">哈啦區</a></td>
</tr></table><p class="BA-cbox8A" style="padding: 5px; min-height: 0;"></p>
</div>
<script>
var hometimer;
(function(window, $, undefined){
var home_nowPage = {"hot":0,"daily":0,"novel":0,"art":0, "comic":0, "loft":0,"cos":0,"others":0};
var home_DNAC_rand = Math.round(Math.random());
var home_LOFT_rand = Math.round(Math.random());
var home_OTHERS_rand = Math.round(Math.random())+2;

//除了hot以外每個Tab的前兩頁會隨機出現,1 or 2
var home_groupPage = {"hot":1,"daily":home_DNAC_rand,"novel":home_DNAC_rand,"art":home_DNAC_rand, "comic":home_DNAC_rand, "loft":home_LOFT_rand,"cos":home_DNAC_rand,"others":home_OTHERS_rand};

var home_more = {"hot":{"str":"逛創作大廰","url":"//home.gamer.com.tw"},"daily":{"str":"看更多","url":"//home.gamer.com.tw/?k1=1"},"novel":{"str":"看更多","url":"//home.gamer.com.tw/?k1=2"},"art":{"str":"看更多","url":"//home.gamer.com.tw/?k1=3"},"comic":{"str":"看更多","url":"//home.gamer.com.tw/?k1=6"},"loft":{"str":"看更多","url":"//home.gamer.com.tw/?k1=0&k2=0&vt=5"},"others":{"str":"逛創作大廰","url":"//home.gamer.com.tw"},"cos":{"str":"看更多","url":"//home.gamer.com.tw/?k1=4"}};
var home_ref = {"hot":"0000163","daily":"0000120","novel":"0000122","art":"0000124", "comic":"", "loft":"0000126","cos":"0000181","others":"0000164"};

var home_list_o = {"hot":[{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977805","title":" 【日常】時效性的愛","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/05\/0004977805.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/05\/0004977805_B.JPG","ts":1605069589,"gp":"194","comment":49,"btitle":"漫畫達人","userid":"sorasky","csn":"4977805","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977765","title":" Pocky日 雷姆 練習","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/65\/0004977765.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/65\/0004977765_B.JPG","ts":1605065370,"gp":"178","comment":36,"btitle":"插畫達人","userid":"sheru0103","csn":"4977765","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978397","title":" 照燒貓的PF33 【3】","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/97\/0004978397.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/97\/0004978397_B.JPG","ts":1605106523,"gp":"131","comment":18,"btitle":"漫畫達人","userid":"niwacat","csn":"4978397","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978247","title":" Pocky Day!","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/47\/0004978247.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/47\/0004978247_B.JPG","ts":1605100578,"gp":"114","comment":18,"btitle":"插畫達人","userid":"torozai0702","csn":"4978247","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978036","title":" 女梅林☆","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/36\/0004978036.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/36\/0004978036_B.JPG","ts":1605090265,"gp":"105","comment":11,"btitle":"插畫達人","userid":"kyaroru","csn":"4978036","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977756","title":" 【吐崽一吐】回頭","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/56\/0004977756.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/56\/0004977756_B.JPG","ts":1605064289,"gp":"95","comment":23,"btitle":"漫畫達人","userid":"ro8230k","csn":"4977756","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978387","title":"FGO  巴御前  泳裝 cosplay (`・ω・´)","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/87\/0004978387.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/87\/0004978387_B.JPG","ts":1605105956,"gp":"87","comment":12,"btitle":"Cosplay達人","userid":"smp030","csn":"4978387","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978229","title":" Shark!","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/29\/0004978229.PNG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/29\/0004978229_B.PNG","ts":1605099819,"gp":"81","comment":11,"btitle":"插畫達人","userid":"faicha2010","csn":"4978229","flag_more":"2"}],"daily":[{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978861","title":" 積分是什麼？為什麼微分的相反是積分？","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/61\/0004978861.PNG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/61\/0004978861_B.PNG","ts":1605148036,"gp":"2","comment":2,"btitle":"日誌達人","userid":"johnny860726","csn":"4978861","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978774","title":" 《靈魂行者》新手必學! 65副本隱匿藏身處攻略教學","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/74\/0004978774.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/74\/0004978774_B.JPG","ts":1605134066,"gp":"2","comment":2,"btitle":"日誌達人","userid":"jiff852","csn":"4978774","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978689","title":" 【桌遊】《Sons of Faeriell》我為人人，人人危我？","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/89\/0004978689.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/89\/0004978689_B.JPG","ts":1605117302,"gp":"19","comment":2,"btitle":"日誌達人","userid":"natsuhotsuki","csn":"4978689","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978505","title":" 【生活雜記】自費健康檢查一日紀錄","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/94\/creation_shiki527.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/94\/creation_shiki527_B.JPG","ts":1605110196,"gp":"18","comment":9,"btitle":"日誌達人","userid":"shiki527","csn":"4978505","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978469","title":" 花《Flower》遊玩心得","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/69\/0004978469.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/69\/0004978469_B.JPG","ts":1605108999,"gp":"19","comment":4,"btitle":"日誌達人","userid":"rigoclean","csn":"4978469","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978405","title":" 【手工篆刻】竈門禰豆子 鬼滅之刃","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/05\/0004978405.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/05\/0004978405_B.JPG","ts":1605106761,"gp":"6","comment":0,"btitle":"日誌達人","userid":"singer0","csn":"4978405","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978319","title":" 【生活遊記】2020\/11\/11 新北市板橋區二餐式韓式年糕鍋飲食","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/19\/0004978319.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/19\/0004978319_B.JPG","ts":1605103193,"gp":"15","comment":3,"btitle":"日誌達人","userid":"ricksmith3","csn":"4978319","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978227","title":" 【台中】裊裊鍋物｜冬天來了～進補必點！麻油燒酒烏骨雞鍋","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/27\/0004978227.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/27\/0004978227_B.JPG","ts":1605099773,"gp":"13","comment":3,"btitle":"日誌達人","userid":"z24518261","csn":"4978227","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978072","title":" 「開箱」變形金剛 NETFLIX War for Cybertron Trilogy 撞針 Impactor","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/72\/0004978072.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/72\/0004978072_B.JPG","ts":1605091856,"gp":"10","comment":5,"btitle":"日誌達人","userid":"aiex888","csn":"4978072","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978017","title":" 《5X1》亞洲導演的五部VR嘗試","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/17\/0004978017.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/17\/0004978017_B.JPG","ts":1605089216,"gp":"6","comment":2,"btitle":"日誌達人","userid":"keivnmoleaf","csn":"4978017","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977999","title":" SHF 超藍貝吉特-SUPER","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/99\/0004977999.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/99\/0004977999_B.JPG","ts":1605088633,"gp":"20","comment":3,"btitle":"日誌達人","userid":"hjc12345p2","csn":"4977999","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977772","title":" 《嘿！聽說妳在鬼島當作家？》65 小說家的未來","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/72\/0004977772.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/72\/0004977772_B.JPG","ts":1605065823,"gp":"26","comment":12,"btitle":"日誌達人","userid":"utsu12","csn":"4977772","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977744","title":" 【生猛海鮮】介紹宜蘭(聚鮮平價快炒)擁有高級餐廳的菜色水準。","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/44\/0004977744.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/44\/0004977744_B.JPG","ts":1605062381,"gp":"17","comment":2,"btitle":"日誌達人","userid":"saliyaaa","csn":"4977744","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977514","title":" 萬代 可動烏龜02 環保扭蛋","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/14\/0004977514.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/14\/0004977514_B.JPG","ts":1605026547,"gp":"38","comment":6,"btitle":"日誌達人","userid":"wuda5555","csn":"4977514","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977191","title":" 【鋼琴】《聲之形》OST「lit」鋼琴演奏分享 ~Pan Piano~","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/91\/0004977191.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/91\/0004977191_B.JPG","ts":1605014811,"gp":"39","comment":1,"btitle":"日誌達人","userid":"panpiano","csn":"4977191","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977130","title":" 【開箱】《新楓之谷 x A3》JMS 日本官方周邊 - 怪獸壓克力站牌開箱！","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/30\/0004977130.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/30\/0004977130_B.JPG","ts":1605012722,"gp":"30","comment":4,"btitle":"日誌達人","userid":"a2017660zx","csn":"4977130","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976772","title":" 1\/100 MRC-F20 Mobile SUMO","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/72\/0004976772.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/72\/0004976772_B.JPG","ts":1604981824,"gp":"28","comment":6,"btitle":"日誌達人","userid":"felled","csn":"4976772","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976672","title":" 〔動畫〕談談近期鬼滅之刃的二三事：配音事件、劇場版（雷）以及為什麼爆紅","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/72\/0004976672.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/72\/0004976672_B.JPG","ts":1604967177,"gp":"22","comment":15,"btitle":"日誌達人","userid":"ghost321","csn":"4976672","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976374","title":" 【青豬】大學篇始動！——《迷惘女歌手》讀後","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/74\/0004976374.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/74\/0004976374_B.JPG","ts":1604936585,"gp":"23","comment":5,"btitle":"日誌達人","userid":"ga839429","csn":"4976374","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976299","title":" 【開箱】SHF 鋼鐵人 東尼史塔克Tony Stark","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/99\/0004976299.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/99\/0004976299_B.JPG","ts":1604933356,"gp":"10","comment":3,"btitle":"日誌達人","userid":"paul860116","csn":"4976299","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976195","title":" 【楷書臨摹】北魏司馬昞墓誌銘～#EP10","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/95\/0004976195.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/95\/0004976195_B.JPG","ts":1604927596,"gp":"38","comment":2,"btitle":"日誌達人","userid":"owy840205","csn":"4976195","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976023","title":" 十 分 早 蟹","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/23\/0004976023.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/23\/0004976023_B.JPG","ts":1604918086,"gp":"96","comment":7,"btitle":"日誌達人","userid":"cck5874tw","csn":"4976023","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4975963","title":" 今天是拿破崙奪權的日子，霧月18","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/63\/0004975963.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/63\/0004975963_B.JPG","ts":1604915103,"gp":"23","comment":6,"btitle":"日誌達人","userid":"dano80441","csn":"4975963","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4975917","title":" 寵豬舉灶寵子不孝，電視劇我的婆婆怎麼那麼可愛。","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/17\/0004975917.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/17\/0004975917_B.JPG","ts":1604911588,"gp":"49","comment":7,"btitle":"日誌達人","userid":"a5511867","csn":"4975917","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4975720","title":" 【霜の雜談】鬼滅之刃劇場版-鍾明軒配音事件大致統整與個人看法","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/20\/0004975720.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/20\/0004975720_B.JPG","ts":1604890722,"gp":"29","comment":6,"btitle":"日誌達人","userid":"mingyue52184","csn":"4975720","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4975192","title":" 舞法少女大戰七海妖!!!-桑塔與七賽蓮","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/92\/0004975192.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/92\/0004975192_B.JPG","ts":1604845035,"gp":"11","comment":3,"btitle":"日誌達人","userid":"arhero99","csn":"4975192","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4975035","title":" [遊戲閒聊]------閒聊最後生還者2中的設計矛盾","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/35\/0004975035.PNG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/35\/0004975035_B.PNG","ts":1604837991,"gp":"30","comment":13,"btitle":"日誌達人","userid":"lee88650","csn":"4975035","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4975010","title":" 略為可惜的後日談──《愛上火車-Last Run!!-》〈日日姬線〉　後日談遊玩心得","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/10\/0004975010.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/10\/0004975010_B.JPG","ts":1604836039,"gp":"11","comment":3,"btitle":"日誌達人","userid":"mondream1222","csn":"4975010","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4974779","title":" 【開箱】GSC 初音未來交響樂 2017Ver. \/\/\/ 微笑著，為各位帶來最棒的演奏","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/79\/0004974779.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/79\/0004974779_B.JPG","ts":1604823956,"gp":"18","comment":2,"btitle":"日誌達人","userid":"asopuun5961","csn":"4974779","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4974573","title":" 父礙：碎瓷-強力挑戰今年最雷續集的糞Game，氣死我了。","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/73\/0004974573.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/73\/0004974573_B.JPG","ts":1604801354,"gp":"28","comment":7,"btitle":"日誌達人","userid":"rougs417","csn":"4974573","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4974539","title":" 醫學生日記：大型醫院路障上線啦！第一站婦產科。","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/39\/0004974539.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/39\/0004974539_B.JPG","ts":1604797203,"gp":"94","comment":29,"btitle":"日誌達人","userid":"annlee0112","csn":"4974539","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4974198","title":" 【活動】太魯閣馬拉松～胖貓也是能跑起來的","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/98\/0004974198.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/98\/0004974198_B.JPG","ts":1604763044,"gp":"44","comment":10,"btitle":"日誌達人","userid":"flys8028","csn":"4974198","flag_more":"2"}],"novel":[{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978773","title":" 心情不好？進來坐坐吧！【楔子or第一章試閱】","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/73\/0004978773.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/73\/0004978773_B.JPG","ts":1605134057,"gp":"1","comment":0,"btitle":"小說達人","userid":"b0931632812","csn":"4978773","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978735","title":" Transcender Game(超越者遊戲)　第二卷：女巫詭島　第二十一章","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/35\/0004978735.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/35\/0004978735_B.JPG","ts":1605123401,"gp":"2","comment":3,"btitle":"小說達人","userid":"loveyou13255","csn":"4978735","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978584","title":" 斷罪少女宅急便 第四十章","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/84\/0004978584.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/84\/0004978584_B.JPG","ts":1605111562,"gp":"8","comment":2,"btitle":"小說達人","userid":"z925675056","csn":"4978584","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978463","title":" 《籠中鳥與紙飛機》七","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/63\/0004978463.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/63\/0004978463_B.JPG","ts":1605108798,"gp":"7","comment":3,"btitle":"小說達人","userid":"asd22552158","csn":"4978463","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978460","title":" 《二手索米與垃圾指揮官特別篇》「Pocky Day !」","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/60\/0004978460.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/60\/0004978460_B.JPG","ts":1605108739,"gp":"15","comment":9,"btitle":"小說達人","userid":"jtsai314","csn":"4978460","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978439","title":" 【中篇原創】不滅的王國（四）","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/39\/0004978439.PNG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/39\/0004978439_B.PNG","ts":1605107758,"gp":"10","comment":3,"btitle":"小說達人","userid":"zeftplant","csn":"4978439","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978428","title":" 台灣.異界戰爭-151","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/28\/0004978428.PNG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/28\/0004978428_B.PNG","ts":1605107416,"gp":"10","comment":5,"btitle":"小說達人","userid":"ss106025","csn":"4978428","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978310","title":" 生存界限　第十四章My dear⋯⋯②","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/10\/0004978310.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/10\/0004978310_B.JPG","ts":1605102892,"gp":"16","comment":2,"btitle":"小說達人","userid":"yuukii","csn":"4978310","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978305","title":" 【短篇】輪迴之間","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/05\/0004978305.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/05\/0004978305_B.JPG","ts":1605102634,"gp":"3","comment":0,"btitle":"小說達人","userid":"kiss52034","csn":"4978305","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978241","title":" 璞華－玖.衝突(2)","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/41\/0004978241.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/41\/0004978241_B.JPG","ts":1605100463,"gp":"4","comment":1,"btitle":"小說達人","userid":"zozo10727","csn":"4978241","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978239","title":" [奇幻小說]　海格德日誌　第二十六章　長腳了？","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/39\/0004978239.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/39\/0004978239_B.JPG","ts":1605100428,"gp":"4","comment":0,"btitle":"小說達人","userid":"ken201010","csn":"4978239","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978237","title":" [百異之端]第一百三十六話  透徹全場","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/37\/0004978237.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/37\/0004978237_B.JPG","ts":1605100363,"gp":"20","comment":1,"btitle":"小說達人","userid":"farmcreate","csn":"4978237","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978178","title":" 《矢破天境》 章回合輯　第三十一章～第三十五章","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/78\/0004978178.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/78\/0004978178_B.JPG","ts":1605097223,"gp":"5","comment":0,"btitle":"小說達人","userid":"gefhacd444","csn":"4978178","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978173","title":" [IG小說]《網路上的魚與貓》016","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/73\/0004978173.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/73\/0004978173_B.JPG","ts":1605097120,"gp":"19","comment":2,"btitle":"小說達人","userid":"risedance","csn":"4978173","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978158","title":" 神奇寶貝幻夢之旅 超級進化的對決","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/58\/0004978158.PNG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/58\/0004978158_B.PNG","ts":1605096260,"gp":"10","comment":7,"btitle":"小說達人","userid":"starmie","csn":"4978158","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978131","title":" 《暴言女教師小愛》CH.9-2「就算死了也會從地獄再爬回來。」","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/31\/0004978131.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/31\/0004978131_B.JPG","ts":1605095217,"gp":"4","comment":3,"btitle":"小說達人","userid":"tom85287","csn":"4978131","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978106","title":" 公爵家的獨生子 第五百七十八章 潑辣女士","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/06\/0004978106.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/06\/0004978106_B.JPG","ts":1605094092,"gp":"55","comment":6,"btitle":"小說達人","userid":"leo25127","csn":"4978106","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978103","title":" 屍體先生總是在裝死(5): 這次是真的鬼呀！","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/03\/0004978103.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/03\/0004978103_B.JPG","ts":1605094050,"gp":"17","comment":3,"btitle":"小說達人","userid":"a22654577","csn":"4978103","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978089","title":" 我在末世天氣晴　第三十九章　落井下石寇德拉","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/89\/0004978089.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/89\/0004978089_B.JPG","ts":1605092895,"gp":"15","comment":5,"btitle":"小說達人","userid":"chh10120","csn":"4978089","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978088","title":" 《曦光試煉：星銅戰途》第八章：現實跟少年漫畫是不同的‧1","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/88\/0004978088.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/88\/0004978088_B.JPG","ts":1605092852,"gp":"2","comment":0,"btitle":"小說達人","userid":"r9805chen","csn":"4978088","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978070","title":" 《畫槌錄》第一百八十七章     當康離去","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/70\/0004978070.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/70\/0004978070_B.JPG","ts":1605091804,"gp":"0","comment":0,"btitle":"小說達人","userid":"ricky112277","csn":"4978070","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978050","title":" 【短篇】替綠月的童話增添色彩（睡美人Ｘ后羿射日）","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/50\/creation_mondream1222.PNG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/50\/creation_mondream1222_B.PNG","ts":1605090976,"gp":"6","comment":1,"btitle":"小說達人","userid":"mondream1222","csn":"4978050","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977996","title":" 龍武傳‧起源之旅二部曲555篇　0-6-0-5【ENTER】","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/96\/0004977996.PNG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/96\/0004977996_B.PNG","ts":1605088444,"gp":"16","comment":0,"btitle":"小說達人","userid":"rw506fr001","csn":"4977996","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977977","title":" 女巫和來自未來的惡魔 第3-24節 對策","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/47\/creation_alexgod29.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/47\/creation_alexgod29_B.JPG","ts":1605087317,"gp":"3","comment":0,"btitle":"小說達人","userid":"alexgod29","csn":"4977977","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977839","title":" 【刀劍亂舞─刀女審】流月篇_#13 甦醒(2\/4)","pic":"https:\/\/avatar2.bahamut.com.tw\/avataruserpic\/c\/l\/claire91824\/claire91824.png","picB":"https:\/\/avatar2.bahamut.com.tw\/avataruserpic\/c\/l\/claire91824\/claire91824.png","ts":1605074191,"gp":"0","comment":0,"btitle":"小說達人","userid":"claire91824","csn":"4977839","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977568","title":" [奇幻愛情]《關於我被國家配發給一個男性人類這件事》第五十七回","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/68\/0004977568.PNG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/68\/0004977568_B.PNG","ts":1605030460,"gp":"9","comment":1,"btitle":"小說達人","userid":"ggglasses","csn":"4977568","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977567","title":" 江湖才子：風雲恆不變，破第歸仙途","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/67\/0004977567.PNG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/67\/0004977567_B.PNG","ts":1605030389,"gp":"5","comment":7,"btitle":"小說達人","userid":"taiwan1998","csn":"4977567","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977560","title":" 《莫名其妙穿越到PM世界》第二十五章、演唱會正式開幕！天嵐行動正式展開！","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/90\/creation_teko53520.PNG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/90\/creation_teko53520_B.PNG","ts":1605029775,"gp":"6","comment":3,"btitle":"小說達人","userid":"teko53520","csn":"4977560","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977558","title":" 歐洲人玩黑魂險喪命，友人：他運氣很好！(4-2)","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/58\/0004977558.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/58\/0004977558_B.JPG","ts":1605029555,"gp":"9","comment":2,"btitle":"小說達人","userid":"ifom","csn":"4977558","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977506","title":" 在雲層之上的女武神","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/91\/creation_tw444.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/91\/creation_tw444_B.JPG","ts":1605026195,"gp":"19","comment":6,"btitle":"小說達人","userid":"tw444","csn":"4977506","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977300","title":" 【隨興短文】手機","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/00\/0004977300.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/00\/0004977300_B.JPG","ts":1605020059,"gp":"30","comment":10,"btitle":"小說達人","userid":"fc105123","csn":"4977300","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977234","title":" 《少年符咒師》第20回：謎樣宇書","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/34\/0004977234.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/34\/0004977234_B.JPG","ts":1605016452,"gp":"3","comment":0,"btitle":"小說達人","userid":"peterwen152","csn":"4977234","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977226","title":" 新進少女指揮官與人形EP21:情報獲取","pic":"https:\/\/avatar2.bahamut.com.tw\/avataruserpic\/w\/v\/wv62801314\/wv62801314.png","picB":"https:\/\/avatar2.bahamut.com.tw\/avataruserpic\/w\/v\/wv62801314\/wv62801314.png","ts":1605016138,"gp":"6","comment":6,"btitle":"小說達人","userid":"wv62801314","csn":"4977226","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977196","title":" 轉生到異世界，然後下面沒有了 01-15：有什麼意見","pic":"https:\/\/avatar2.bahamut.com.tw\/avataruserpic\/m\/t\/mthouse\/mthouse.png","picB":"https:\/\/avatar2.bahamut.com.tw\/avataruserpic\/m\/t\/mthouse\/mthouse.png","ts":1605014873,"gp":"24","comment":0,"btitle":"小說達人","userid":"mthouse","csn":"4977196","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977095","title":" 【咒術迴戰│五夏五】結－１","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/95\/0004977095.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/95\/0004977095_B.JPG","ts":1605010813,"gp":"3","comment":1,"btitle":"小說達人","userid":"mm07110703","csn":"4977095","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977044","title":" 異世界冒險者傳 118.絕望&amp;崩塌","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/44\/0004977044.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/44\/0004977044_B.JPG","ts":1605006830,"gp":"10","comment":4,"btitle":"小說達人","userid":"KKTarta","csn":"4977044","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976901","title":" 寶可夢大挑戰　第八十六話　「毫無根據的推理」","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/01\/0004976901.PNG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/01\/0004976901_B.PNG","ts":1604995618,"gp":"4","comment":1,"btitle":"小說達人","userid":"aqsq1424","csn":"4976901","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976896","title":" 第二十二章－The Frenemy 昨天的敵人，今天的朋友","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/96\/0004976896.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/96\/0004976896_B.JPG","ts":1604995049,"gp":"4","comment":0,"btitle":"小說達人","userid":"bingh21","csn":"4976896","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976757","title":" 鬥人 第兩百九十九章 三國交流會","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/57\/0004976757.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/57\/0004976757_B.JPG","ts":1604979886,"gp":"16","comment":2,"btitle":"小說達人","userid":"foxy80196","csn":"4976757","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978773","title":" 心情不好？進來坐坐吧！【楔子or第一章試閱】","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/73\/0004978773.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/73\/0004978773_B.JPG","ts":1605134057,"gp":"1","comment":0,"btitle":"小說達人","userid":"b0931632812","csn":"4978773","flag_more":"0"}],"art":[{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978725","title":" 燃燒的本能寺","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/25\/0004978725.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/25\/0004978725_B.JPG","ts":1605121470,"gp":"26","comment":3,"btitle":"插畫達人","userid":"s0800880842","csn":"4978725","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978723","title":" 可可蘿媽媽♨♨♨♨♨♨","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/23\/0004978723.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/23\/0004978723_B.JPG","ts":1605120847,"gp":"73","comment":7,"btitle":"插畫達人","userid":"yhes70903","csn":"4978723","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978361","title":" 悠白出道~☁","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/61\/0004978361.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/61\/0004978361_B.JPG","ts":1605104645,"gp":"56","comment":5,"btitle":"插畫達人","userid":"bear2011357","csn":"4978361","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978247","title":" Pocky Day!","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/47\/0004978247.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/47\/0004978247_B.JPG","ts":1605100578,"gp":"114","comment":18,"btitle":"插畫達人","userid":"torozai0702","csn":"4978247","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978229","title":" Shark!","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/29\/0004978229.PNG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/29\/0004978229_B.PNG","ts":1605099819,"gp":"81","comment":11,"btitle":"插畫達人","userid":"faicha2010","csn":"4978229","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978224","title":" 泳裝團長","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/24\/0004978224.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/24\/0004978224_B.JPG","ts":1605099680,"gp":"30","comment":3,"btitle":"插畫達人","userid":"rei55123","csn":"4978224","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978192","title":" ❤再出發！1111光棍節就由瀕臨絕種團來陪伴人類❤","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/92\/0004978192.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/92\/0004978192_B.JPG","ts":1605097860,"gp":"78","comment":8,"btitle":"插畫達人","userid":"springfish01","csn":"4978192","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978174","title":" PAＬe &lt;EX&gt;","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/74\/0004978174.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/74\/0004978174_B.JPG","ts":1605097133,"gp":"18","comment":1,"btitle":"插畫達人","userid":"d3879012","csn":"4978174","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978159","title":" ハッピーシンセサイザ","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/59\/0004978159.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/59\/0004978159_B.JPG","ts":1605096288,"gp":"47","comment":7,"btitle":"插畫達人","userid":"z4031259","csn":"4978159","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978080","title":" Pocky日","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/80\/0004978080.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/80\/0004978080_B.JPG","ts":1605092241,"gp":"14","comment":3,"btitle":"插畫達人","userid":"sogreia","csn":"4978080","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978049","title":" Crossick-Pocky日!","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/49\/0004978049.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/49\/0004978049_B.JPG","ts":1605090953,"gp":"56","comment":7,"btitle":"插畫達人","userid":"amy30535","csn":"4978049","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978036","title":" 女梅林☆","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/36\/0004978036.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/36\/0004978036_B.JPG","ts":1605090265,"gp":"105","comment":11,"btitle":"插畫達人","userid":"kyaroru","csn":"4978036","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977995","title":" Pocky日－召喚","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/95\/0004977995.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/95\/0004977995_B.JPG","ts":1605088369,"gp":"55","comment":11,"btitle":"插畫達人","userid":"manto9898","csn":"4977995","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977983","title":" 上色練習","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/83\/0004977983.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/83\/0004977983_B.JPG","ts":1605087484,"gp":"37","comment":5,"btitle":"插畫達人","userid":"qwer713781","csn":"4977983","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977932","title":" AQUA","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/32\/0004977932.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/32\/0004977932_B.JPG","ts":1605084475,"gp":"57","comment":9,"btitle":"插畫達人","userid":"loxaii","csn":"4977932","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977927","title":" 埃爾德里奇 和 女兒生日動圖","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/27\/0004977927.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/27\/0004977927_B.JPG","ts":1605083869,"gp":"48","comment":10,"btitle":"插畫達人","userid":"juiceneko","csn":"4977927","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977891","title":" 冬天就是要畫夏天啦(?","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/91\/0004977891.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/91\/0004977891_B.JPG","ts":1605080814,"gp":"37","comment":5,"btitle":"插畫達人","userid":"luke3411","csn":"4977891","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977765","title":" Pocky日 雷姆 練習","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/65\/0004977765.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/65\/0004977765_B.JPG","ts":1605065370,"gp":"178","comment":36,"btitle":"插畫達人","userid":"sheru0103","csn":"4977765","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977664","title":" 【idolish7】百生日快樂+i7塗鴉","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/64\/0004977664.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/64\/0004977664_B.JPG","ts":1605052545,"gp":"23","comment":2,"btitle":"插畫達人","userid":"cindychiang","csn":"4977664","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977621","title":" 委託繪-哥吉拉女孩大戰","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/21\/0004977621.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/21\/0004977621_B.JPG","ts":1605039684,"gp":"43","comment":7,"btitle":"插畫達人","userid":"fai2392894","csn":"4977621","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977533","title":" 【SANPHY的亂七八糟法國日常】 －67－NIKY家的兩隻貓","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/33\/0004977533.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/33\/0004977533_B.JPG","ts":1605027748,"gp":"113","comment":33,"btitle":"插畫達人","userid":"black603","csn":"4977533","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977360","title":" ⟡訓練家⟡ :: Dress Code::","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/60\/0004977360.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/60\/0004977360_B.JPG","ts":1605022260,"gp":"85","comment":8,"btitle":"插畫達人","userid":"pink0930cat","csn":"4977360","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977339","title":" 【死神】赫麗貝爾崩玉融合","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/39\/0004977339.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/39\/0004977339_B.JPG","ts":1605021141,"gp":"58","comment":13,"btitle":"插畫達人","userid":"z4675520","csn":"4977339","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977282","title":" 桃鈴ねね","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/82\/0004977282.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/82\/0004977282_B.JPG","ts":1605019447,"gp":"50","comment":5,"btitle":"插畫達人","userid":"s931610","csn":"4977282","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977204","title":" [水彩] 娜娜奇吃沾麵","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/04\/0004977204.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/04\/0004977204_B.JPG","ts":1605015113,"gp":"57","comment":7,"btitle":"插畫達人","userid":"blackcatbox","csn":"4977204","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977176","title":" 精靈","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/76\/0004977176.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/76\/0004977176_B.JPG","ts":1605013834,"gp":"98","comment":15,"btitle":"插畫達人","userid":"lagijay123","csn":"4977176","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977152","title":" 【繪圖】偷懶女僕","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/52\/0004977152.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/52\/0004977152_B.JPG","ts":1605013117,"gp":"115","comment":41,"btitle":"插畫達人","userid":"qqq60710","csn":"4977152","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977032","title":" 菲謝爾","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/32\/0004977032.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/32\/0004977032_B.JPG","ts":1605005936,"gp":"181","comment":17,"btitle":"插畫達人","userid":"g391611","csn":"4977032","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976986","title":" 百鬼喵喵","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/86\/0004976986.PNG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/86\/0004976986_B.PNG","ts":1605002453,"gp":"129","comment":10,"btitle":"插畫達人","userid":"a86527413","csn":"4976986","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976960","title":" 機械盔甲","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/60\/0004976960.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/60\/0004976960_B.JPG","ts":1605001034,"gp":"17","comment":4,"btitle":"插畫達人","userid":"asd09192529","csn":"4976960","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976935","title":" 瑪琳船長","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/35\/0004976935.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/35\/0004976935_B.JPG","ts":1604998983,"gp":"304","comment":25,"btitle":"插畫達人","userid":"zaku2s1024","csn":"4976935","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976660","title":" Princess Towa","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/60\/0004976660.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/60\/0004976660_B.JPG","ts":1604965291,"gp":"333","comment":35,"btitle":"插畫達人","userid":"snake13579","csn":"4976660","flag_more":"0"}],"comic":[{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978857","title":" 酸菜是惡魔食物!!","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/57\/0004978857.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/57\/0004978857_B.JPG","ts":1605147648,"gp":"0","comment":0,"btitle":"漫畫達人","userid":"rock869842","csn":"4978857","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978835","title":" 錯字姬紅妻","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/35\/0004978835.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/35\/0004978835_B.JPG","ts":1605144053,"gp":"41","comment":33,"btitle":"漫畫達人","userid":"sundae0204","csn":"4978835","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978700","title":" 南部靜美的中學手記.其七十五 (教官的完美示範)","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/00\/0004978700.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/00\/0004978700_B.JPG","ts":1605118119,"gp":"5","comment":0,"btitle":"漫畫達人","userid":"nosuke678","csn":"4978700","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978661","title":" 【耍蠢實錄】AMAZING TOYAMA","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/61\/0004978661.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/61\/0004978661_B.JPG","ts":1605114739,"gp":"2","comment":1,"btitle":"漫畫達人","userid":"shu855025","csn":"4978661","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978479","title":" 【倪亞的戀丹工坊】毛毛先生?? ep.36","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/79\/0004978479.PNG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/79\/0004978479_B.PNG","ts":1605109301,"gp":"6","comment":2,"btitle":"漫畫達人","userid":"niamasalu1","csn":"4978479","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978397","title":" 照燒貓的PF33 【3】","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/97\/0004978397.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/97\/0004978397_B.JPG","ts":1605106523,"gp":"131","comment":18,"btitle":"漫畫達人","userid":"niwacat","csn":"4978397","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978293","title":" 糾結漫畫｜生日快樂、別人生日","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/93\/0004978293.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/93\/0004978293_B.JPG","ts":1605102133,"gp":"13","comment":2,"btitle":"漫畫達人","userid":"s2k128","csn":"4978293","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978225","title":" 嘴砲戰爭(下)","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/25\/0004978225.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/25\/0004978225_B.JPG","ts":1605099679,"gp":"9","comment":5,"btitle":"漫畫達人","userid":"chejia","csn":"4978225","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978086","title":" 棒球人生賽 前傳 16~20","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/86\/0004978086.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/86\/0004978086_B.JPG","ts":1605092676,"gp":"31","comment":1,"btitle":"漫畫達人","userid":"chia80740","csn":"4978086","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978028","title":" [原創]中元祭之夜","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/28\/0004978028.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/28\/0004978028_B.JPG","ts":1605089614,"gp":"29","comment":8,"btitle":"漫畫達人","userid":"bonnie801014","csn":"4978028","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977994","title":" [原創]魔法少年，襲來！2-1","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/94\/0004977994.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/94\/0004977994_B.JPG","ts":1605088361,"gp":"11","comment":1,"btitle":"漫畫達人","userid":"saya000","csn":"4977994","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977820","title":" 【動森】無人島的森活-EP075 深夜食堂2","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/20\/0004977820.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/20\/0004977820_B.JPG","ts":1605072056,"gp":"6","comment":3,"btitle":"漫畫達人","userid":"luffy2074","csn":"4977820","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977805","title":" 【日常】時效性的愛","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/05\/0004977805.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/05\/0004977805_B.JPG","ts":1605069589,"gp":"194","comment":49,"btitle":"漫畫達人","userid":"sorasky","csn":"4977805","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977777","title":" Xbox Series S 發售了！","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/77\/0004977777.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/77\/0004977777_B.JPG","ts":1605066971,"gp":"27","comment":2,"btitle":"漫畫達人","userid":"off60","csn":"4977777","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977756","title":" 【吐崽一吐】回頭","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/56\/0004977756.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/56\/0004977756_B.JPG","ts":1605064289,"gp":"95","comment":23,"btitle":"漫畫達人","userid":"ro8230k","csn":"4977756","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977522","title":" 第51篇：向城裡前進!!","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/22\/0004977522.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/22\/0004977522_B.JPG","ts":1605026886,"gp":"157","comment":33,"btitle":"漫畫達人","userid":"mikosuika","csn":"4977522","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977115","title":" HOLOLIVE  前輩的三叉戟捏!","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/15\/0004977115.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/15\/0004977115_B.JPG","ts":1605011892,"gp":"113","comment":15,"btitle":"漫畫達人","userid":"tmb3027","csn":"4977115","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977069","title":" 【日安同學漫畫】女朋友","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/69\/0004977069.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/69\/0004977069_B.JPG","ts":1605008674,"gp":"154","comment":46,"btitle":"漫畫達人","userid":"pg2675","csn":"4977069","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977021","title":" 奇片漫畫&lt;討論&gt;","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/21\/0004977021.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/21\/0004977021_B.JPG","ts":1605005106,"gp":"146","comment":33,"btitle":"漫畫達人","userid":"hguuy","csn":"4977021","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977008","title":" 【日常】曲線","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/08\/0004977008.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/08\/0004977008_B.JPG","ts":1605003913,"gp":"142","comment":22,"btitle":"漫畫達人","userid":"kinoko94","csn":"4977008","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976875","title":" 你的名字！","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/75\/0004976875.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/75\/0004976875_B.JPG","ts":1604993647,"gp":"40","comment":5,"btitle":"漫畫達人","userid":"a4112477","csn":"4976875","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976841","title":" [滾動劇場]殺手","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/41\/0004976841.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/41\/0004976841_B.JPG","ts":1604991953,"gp":"106","comment":29,"btitle":"漫畫達人","userid":"rglcs0606","csn":"4976841","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976760","title":" 【原創】S.H.E.R.O-1","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/60\/0004976760.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/60\/0004976760_B.JPG","ts":1604980004,"gp":"13","comment":2,"btitle":"漫畫達人","userid":"namecochen","csn":"4976760","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976733","title":" 新手們有點尷尬的達摩車站:未完成","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/33\/0004976733.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/33\/0004976733_B.JPG","ts":1604976285,"gp":"6","comment":1,"btitle":"漫畫達人","userid":"yanjie202","csn":"4976733","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976714","title":" 【日常】繪筆精靈","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/14\/0004976714.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/14\/0004976714_B.JPG","ts":1604973555,"gp":"148","comment":26,"btitle":"漫畫達人","userid":"korokoro","csn":"4976714","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976639","title":" ☢書書的崩潰倒數！5、4、3、2、1…..☢","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/39\/0004976639.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/39\/0004976639_B.JPG","ts":1604958800,"gp":"168","comment":12,"btitle":"漫畫達人","userid":"springfish01","csn":"4976639","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976485","title":" 【伈婷畫冷漫】：別偷開甚麼肉車!!","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/85\/0004976485.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/85\/0004976485_B.JPG","ts":1604939094,"gp":"79","comment":14,"btitle":"漫畫達人","userid":"qoo0958237","csn":"4976485","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976471","title":" 【萬能麥可】S2-EP.83‧眼鏡業障重（2）","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/71\/0004976471.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/71\/0004976471_B.JPG","ts":1604938651,"gp":"11","comment":7,"btitle":"漫畫達人","userid":"pupu8520","csn":"4976471","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976291","title":" 【動森森日Day0】 - 我的酷東西","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/91\/0004976291.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/91\/0004976291_B.JPG","ts":1604932929,"gp":"130","comment":22,"btitle":"漫畫達人","userid":"g84fu4asu4dj","csn":"4976291","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976266","title":" 時尚","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/66\/0004976266.PNG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/66\/0004976266_B.PNG","ts":1604931479,"gp":"120","comment":18,"btitle":"漫畫達人","userid":"ccm830112","csn":"4976266","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976075","title":" 那些年我們一起玩的楓之谷-31","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/75\/0004976075.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/75\/0004976075_B.JPG","ts":1604922472,"gp":"18","comment":5,"btitle":"漫畫達人","userid":"z0970300678","csn":"4976075","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976061","title":" 【菜圓日常】不務正業系列.心理治療","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/61\/0004976061.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/61\/0004976061_B.JPG","ts":1604921403,"gp":"112","comment":24,"btitle":"漫畫達人","userid":"w7881214","csn":"4976061","flag_more":"0"}],"cos":[{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978703","title":"催眠麥克風 入間銃兔","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/03\/0004978703.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/03\/0004978703_B.JPG","ts":1605118512,"gp":"5","comment":0,"btitle":"Cosplay達人","userid":"ryo0105","csn":"4978703","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978475","title":"非人哉 九月 試妝","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/75\/0004978475.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/75\/0004978475_B.JPG","ts":1605109188,"gp":"3","comment":0,"btitle":"Cosplay達人","userid":"karashi12","csn":"4978475","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978387","title":"FGO  巴御前  泳裝 cosplay (`・ω・´)","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/87\/0004978387.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/87\/0004978387_B.JPG","ts":1605105956,"gp":"87","comment":12,"btitle":"Cosplay達人","userid":"smp030","csn":"4978387","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977326","title":"PF2日","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/26\/0004977326.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/26\/0004977326_B.JPG","ts":1605020785,"gp":"15","comment":3,"btitle":"Cosplay達人","userid":"QAZW83836","csn":"4977326","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976077","title":"【原創】廢棄神社的妖怪 @v@","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/77\/0004976077.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/77\/0004976077_B.JPG","ts":1604922622,"gp":"18","comment":0,"btitle":"Cosplay達人","userid":"housejk20212","csn":"4976077","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4975038","title":"PF~久違的艦娘!!!","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/38\/0004975038.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/38\/0004975038_B.JPG","ts":1604838082,"gp":"19","comment":3,"btitle":"Cosplay達人","userid":"m450086","csn":"4975038","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4973406","title":"sin七大罪❧傲慢-路西法(CN咖啡)","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/06\/0004973406.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/06\/0004973406_B.JPG","ts":1604705222,"gp":"91","comment":11,"btitle":"Cosplay達人","userid":"nvf1414","csn":"4973406","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4972099","title":"FGO 伊斯塔【 同人女僕】","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/99\/0004972099.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/99\/0004972099_B.JPG","ts":1604589129,"gp":"60","comment":18,"btitle":"Cosplay達人","userid":"wl02147565","csn":"4972099","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4971782","title":"✟MOMO x 阿比泳裝✟【第二再臨】","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/82\/0004971782.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/82\/0004971782_B.JPG","ts":1604571240,"gp":"145","comment":14,"btitle":"Cosplay達人","userid":"fluffy950057","csn":"4971782","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4971660","title":"霞之丘詩羽 睡衣","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/60\/0004971660.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/60\/0004971660_B.JPG","ts":1604562702,"gp":"62","comment":11,"btitle":"Cosplay達人","userid":"sally810927","csn":"4971660","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4968935","title":"✟MOMO x 阿比泳裝✟【第三再臨】","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/35\/0004968935.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/35\/0004968935_B.JPG","ts":1604311141,"gp":"161","comment":12,"btitle":"Cosplay達人","userid":"fluffy950057","csn":"4968935","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4968750","title":"《 RWBY 》 Ruby Rose","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/50\/0004968750.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/50\/0004968750_B.JPG","ts":1604295372,"gp":"34","comment":10,"btitle":"Cosplay達人","userid":"kyoko891801","csn":"4968750","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4968494","title":"萬聖節扮成可愛的鯊鯊!","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/94\/0004968494.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/94\/0004968494_B.JPG","ts":1604252930,"gp":"86","comment":18,"btitle":"Cosplay達人","userid":"k753732","csn":"4968494","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4967541","title":"真三國無双－蜀三傑","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/41\/0004967541.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/41\/0004967541_B.JPG","ts":1604201774,"gp":"23","comment":3,"btitle":"Cosplay達人","userid":"dragonda1244","csn":"4967541","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4966807","title":"【落花凋零】刀劍神域萬聖節魔女詩乃","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/07\/0004966807.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/07\/0004966807_B.JPG","ts":1604154296,"gp":"29","comment":2,"btitle":"Cosplay達人","userid":"chichi58525","csn":"4966807","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4966628","title":"【原創速報】妖孽3 @v@","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/28\/0004966628.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/28\/0004966628_B.JPG","ts":1604148258,"gp":"28","comment":7,"btitle":"Cosplay達人","userid":"housejk20212","csn":"4966628","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4966528","title":"美少女万華鏡 - 篝ノ霧枝 Neko Ver.","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/28\/0004966528.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/28\/0004966528_B.JPG","ts":1604144287,"gp":"231","comment":31,"btitle":"Cosplay達人","userid":"bigbang84427","csn":"4966528","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4966515","title":"K Neko","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/15\/0004966515.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/15\/0004966515_B.JPG","ts":1604143991,"gp":"60","comment":17,"btitle":"Cosplay達人","userid":"sally810927","csn":"4966515","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4966448","title":"【Fate\/Grand Order】難得的萬聖節，奴家也來玩玩吧","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/48\/0004966448.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/48\/0004966448_B.JPG","ts":1604140208,"gp":"37","comment":2,"btitle":"Cosplay達人","userid":"s912046","csn":"4966448","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4966282","title":"自創 萬聖節【小倉鼠】","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/82\/0004966282.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/82\/0004966282_B.JPG","ts":1604131040,"gp":"107","comment":27,"btitle":"Cosplay達人","userid":"wl02147565","csn":"4966282","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4966125","title":"【Fate\/Grand Order】瑪修‧基利艾拉特. Shielder マシュ・キリエライト 常夏の水着Ver.02","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/25\/0004966125.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/25\/0004966125_B.JPG","ts":1604118838,"gp":"175","comment":14,"btitle":"Cosplay達人","userid":"darksteel","csn":"4966125","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4964995","title":"2020 萬聖節節慶圖","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/95\/0004964995.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/95\/0004964995_B.JPG","ts":1604042395,"gp":"6","comment":1,"btitle":"Cosplay達人","userid":"monolulu","csn":"4964995","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4964330","title":"【Fate\/Grand Order】就算是鬼也要過萬聖節！","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/30\/0004964330.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/30\/0004964330_B.JPG","ts":1603980390,"gp":"75","comment":6,"btitle":"Cosplay達人","userid":"s912046","csn":"4964330","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4963658","title":"「Fate\/Grand Order 5th Anniversary」","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/58\/0004963658.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/58\/0004963658_B.JPG","ts":1603911747,"gp":"21","comment":4,"btitle":"Cosplay達人","userid":"ryo0105","csn":"4963658","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4962955","title":"光遇 蝴蝶結 外拍正片","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/55\/0004962955.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/55\/0004962955_B.JPG","ts":1603872696,"gp":"15","comment":2,"btitle":"Cosplay達人","userid":"karashi12","csn":"4962955","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4962845","title":"佩可泳裝的完成...","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/45\/0004962845.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/45\/0004962845_B.JPG","ts":1603858148,"gp":"45","comment":9,"btitle":"Cosplay達人","userid":"jingayu057","csn":"4962845","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4962838","title":"我最愛的本命!!(改版再發文 大劍已完成)","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/38\/0004962838.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/38\/0004962838_B.JPG","ts":1603857133,"gp":"49","comment":8,"btitle":"Cosplay達人","userid":"jingayu057","csn":"4962838","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4962822","title":"鬼滅之刃 戀柱 萬聖節","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/22\/0004962822.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/22\/0004962822_B.JPG","ts":1603855903,"gp":"27","comment":9,"btitle":"Cosplay達人","userid":"jingayu057","csn":"4962822","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4962308","title":"【COS】Vsinger - 言和 刀劍春秋ver.","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/08\/0004962308.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/08\/0004962308_B.JPG","ts":1603807074,"gp":"14","comment":2,"btitle":"Cosplay達人","userid":"tsubasa951","csn":"4962308","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4962184","title":"來懺悔吧  獸耳修女(`・ω・´)","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/84\/0004962184.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/84\/0004962184_B.JPG","ts":1603802102,"gp":"136","comment":20,"btitle":"Cosplay達人","userid":"smp030","csn":"4962184","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4962032","title":"COS 自創角\/萬聖節\/修女系列 CN:海琳","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/32\/0004962032.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/32\/0004962032_B.JPG","ts":1603792504,"gp":"82","comment":6,"btitle":"Cosplay達人","userid":"helene0207","csn":"4962032","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4961100","title":"BanG Dream! - 湊友希那 一單團服","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/00\/0004961100.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/00\/0004961100_B.JPG","ts":1603713150,"gp":"39","comment":4,"btitle":"Cosplay達人","userid":"bigbang84427","csn":"4961100","flag_more":"2"}],"loft":[{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977679","title":"好久沒畫男角了","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/79\/0004977679.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/79\/0004977679_B.JPG","ts":1605053531,"gp":"5","comment":2,"btitle":"插畫","userid":"ys1009","csn":"4977679","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977655","title":"靈夢","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/55\/0004977655.PNG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/55\/0004977655_B.PNG","ts":1605051206,"gp":"20","comment":4,"btitle":"插畫","userid":"jjo677754","csn":"4977655","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977641","title":"無題","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/41\/0004977641.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/41\/0004977641_B.JPG","ts":1605048304,"gp":"14","comment":3,"btitle":"插畫","userid":"orz04870487","csn":"4977641","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977625","title":"羊森","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/25\/0004977625.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/25\/0004977625_B.JPG","ts":1605041347,"gp":"5","comment":2,"btitle":"插畫","userid":"mmm319999","csn":"4977625","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977586","title":"【噗浪】逆噗幣轉蛋圖","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/86\/0004977586.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/86\/0004977586_B.JPG","ts":1605032472,"gp":"3","comment":1,"btitle":"插畫","userid":"fu03hj","csn":"4977586","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977578","title":"【繪圖】獸耳學妹","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/78\/0004977578.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/78\/0004977578_B.JPG","ts":1605031274,"gp":"17","comment":5,"btitle":"插畫","userid":"sky9781432","csn":"4977578","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977374","title":"【遊戲心得】小製圖師卡朵的大冒險《Carto》","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/74\/0004977374.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/74\/0004977374_B.JPG","ts":1605022852,"gp":"33","comment":1,"btitle":"日誌","userid":"fliris003","csn":"4977374","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977207","title":"【HoloVT】facking cute wolf  大神澪","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/07\/0004977207.PNG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/07\/0004977207_B.PNG","ts":1605015169,"gp":"102","comment":14,"btitle":"插畫","userid":"jefflink","csn":"4977207","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977100","title":"魔法師的旅途","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/00\/0004977100.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/00\/0004977100_B.JPG","ts":1605010981,"gp":"25","comment":6,"btitle":"插畫","userid":"mahaha456","csn":"4977100","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977088","title":"繡球花x惡魔","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/88\/0004977088.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/88\/0004977088_B.JPG","ts":1605010180,"gp":"8","comment":2,"btitle":"插畫","userid":"finaleand","csn":"4977088","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977045","title":"阿露絲·阿爾瑪爾","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/45\/0004977045.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/45\/0004977045_B.JPG","ts":1605006934,"gp":"71","comment":21,"btitle":"插畫","userid":"red19961010","csn":"4977045","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977014","title":"Watson Library","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/14\/0004977014.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/14\/0004977014_B.JPG","ts":1605004210,"gp":"12","comment":3,"btitle":"插畫","userid":"dvre8324","csn":"4977014","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976947","title":"[原創企劃] Code01-鬼","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/47\/0004976947.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/47\/0004976947_B.JPG","ts":1605000081,"gp":"27","comment":3,"btitle":"插畫","userid":"a84830304","csn":"4976947","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976768","title":"守望傳說","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/68\/0004976768.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/68\/0004976768_B.JPG","ts":1604981085,"gp":"7","comment":4,"btitle":"插畫","userid":"xup6ej040lin","csn":"4976768","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976746","title":"[鬼滅之刃] [煉獄杏壽郎] [塗鴉][炎]","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/46\/0004976746.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/46\/0004976746_B.JPG","ts":1604978182,"gp":"5","comment":2,"btitle":"插畫","userid":"yah1727","csn":"4976746","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976707","title":"《虛偽的善惡》第十章．一體兩面(2)","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/35\/creation_joy2012.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/35\/creation_joy2012_B.JPG","ts":1604972800,"gp":"12","comment":1,"btitle":"小說","userid":"joy2012","csn":"4976707","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976587","title":"【HololiveEN】甜點鯊","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/87\/0004976587.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/87\/0004976587_B.JPG","ts":1604944741,"gp":"29","comment":3,"btitle":"插畫","userid":"wolf9293987","csn":"4976587","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976576","title":"BBA船長 讚啦","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/76\/0004976576.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/76\/0004976576_B.JPG","ts":1604943830,"gp":"112","comment":9,"btitle":"插畫","userid":"ikaros","csn":"4976576","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976516","title":"碧藍 泡沫夢幻蝴蝶刀 姊姊","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/16\/0004976516.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/16\/0004976516_B.JPG","ts":1604940255,"gp":"96","comment":9,"btitle":"插畫","userid":"sty375678","csn":"4976516","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976356","title":"今日もかわいい！","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/56\/0004976356.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/56\/0004976356_B.JPG","ts":1604935778,"gp":"16","comment":2,"btitle":"插畫","userid":"user23","csn":"4976356","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976279","title":"ユウマリ","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/79\/0004976279.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/79\/0004976279_B.JPG","ts":1604932203,"gp":"19","comment":3,"btitle":"插畫","userid":"JamesAmigo","csn":"4976279","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976270","title":"這東西比劍好用呀~~ヤバイですね！☆","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/70\/0004976270.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/70\/0004976270_B.JPG","ts":1604931692,"gp":"44","comment":8,"btitle":"插畫","userid":"hit187ler","csn":"4976270","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976250","title":"【RPG四期創作】加深的羈絆","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/50\/0004976250.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/50\/0004976250_B.JPG","ts":1604930486,"gp":"10","comment":1,"btitle":"小說","userid":"frank51512","csn":"4976250","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976237","title":"KDA 阿卡莉","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/37\/0004976237.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/37\/0004976237_B.JPG","ts":1604929594,"gp":"28","comment":14,"btitle":"插畫","userid":"vlittlelu","csn":"4976237","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976209","title":"夏日向日葵長門❤","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/09\/0004976209.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/09\/0004976209_B.JPG","ts":1604927909,"gp":"61","comment":17,"btitle":"插畫","userid":"maxft2","csn":"4976209","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976161","title":"Halloween","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/61\/0004976161.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/61\/0004976161_B.JPG","ts":1604926021,"gp":"15","comment":2,"btitle":"插畫","userid":"relm","csn":"4976161","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976068","title":"雪之銀山","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/68\/0004976068.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/68\/0004976068_B.JPG","ts":1604921871,"gp":"6","comment":1,"btitle":"插畫","userid":"t8552753966","csn":"4976068","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976034","title":"天使INA","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/34\/0004976034.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/34\/0004976034_B.JPG","ts":1604919223,"gp":"64","comment":7,"btitle":"插畫","userid":"michaeler25","csn":"4976034","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4975874","title":"【繪圖】鯊鯊","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/74\/0004975874.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/74\/0004975874_B.JPG","ts":1604907537,"gp":"31","comment":6,"btitle":"插畫","userid":"jackson2444","csn":"4975874","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4975754","title":"(● ∸ ●)","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/54\/0004975754.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/54\/0004975754_B.JPG","ts":1604896884,"gp":"10","comment":1,"btitle":"插畫","userid":"as153968","csn":"4975754","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4975543","title":"【少女歌劇】茶會光晝","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/43\/0004975543.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/43\/0004975543_B.JPG","ts":1604858684,"gp":"38","comment":6,"btitle":"插畫","userid":"su1116111","csn":"4975543","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4975186","title":"[開箱]ENTRY GRADE 綠谷出久","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/86\/0004975186.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/86\/0004975186_B.JPG","ts":1604844749,"gp":"9","comment":3,"btitle":"日誌","userid":"az20590","csn":"4975186","flag_more":"0"}],"others":[{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977719","title":"【模型】F:NEX《魔女之旅》伊蕾娜，開放預購中！","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/19\/0004977719.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/19\/0004977719.JPG","ts":1605059907,"gp":0,"comment":0,"flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977703","title":"《七大罪 憤怒的審判》動畫公開主視覺圖與首支宣傳影片 預計2021年1月6日開播","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/03\/0004977703.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/03\/0004977703.JPG","ts":1605057434,"gp":0,"comment":0,"flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977384","title":"『記錄的地平線』動畫第3期《ログ・ホライズン 円卓崩壊》新海報公開！","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/84\/0004977384.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/84\/0004977384.JPG","ts":1605023387,"gp":0,"comment":0,"flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4974670","title":"「日高里菜×富田美憂」加入2021年1月電視動畫版《裏世界遠足》推出全新海報！","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/70\/0004974670.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/70\/0004974670.JPG","ts":1604812739,"gp":0,"comment":0,"flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4976274","title":"『劇場版 刀劍神域Progressive 無星之夜的詠嘆調』2021年公開決定！","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/74\/0004976274.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/74\/0004976274.JPG","ts":1604932079,"gp":0,"comment":0,"flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4975158","title":"聲優歌手・水瀨祈 第 9 張單曲「Starlight Museum」釋出短版 MUSIC CLIP","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/58\/0004975158.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/58\/0004975158.JPG","ts":1604843281,"gp":0,"comment":0,"flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4974120","title":"動畫《LoveLive！虹咲學園學園偶像同好會》釋出插入曲單曲第二彈與 Blu-ray 特典資訊","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/20\/0004974120.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/20\/0004974120.JPG","ts":1604759680,"gp":0,"comment":0,"flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4972549","title":"【模型】BellFine《約會大作戰 DATE A LIVE》七罪，預定明年 3 月發售！","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/49\/0004972549.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/49\/0004972549.JPG","ts":1604640368,"gp":0,"comment":0,"flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4974671","title":"動畫《不要欺負我，長瀞同學》於2021年春天放送、新聲優&amp;新海報發表！","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/71\/0004974671.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/71\/0004974671.JPG","ts":1604812961,"gp":0,"comment":0,"flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4971968","title":"《2.43 清陰高中男子排球社》動畫釋出新視覺圖、正式宣傳影片等情報","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/68\/0004971968.PNG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/68\/0004971968.PNG","ts":1604582693,"gp":0,"comment":0,"flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4971577","title":"【情報】Figure-rise LABO  研究主題四 緊身衣「EVA破  式波 · 明日香」明年3月登場","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/77\/0004971577.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/77\/0004971577.JPG","ts":1604549613,"gp":0,"comment":0,"flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4972467","title":"《弱角友崎同學》動畫公開新視覺圖 預計2021年1月8日開播","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/67\/0004972467.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/67\/0004972467.JPG","ts":1604628646,"gp":0,"comment":0,"flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4971559","title":"【模型】OR《Fate\/Grand Order -神聖圓桌領域卡美洛-》黏土人 貝德維爾 明年6月發售","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/59\/0004971559.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/59\/0004971559.JPG","ts":1604547563,"gp":0,"comment":0,"flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4971517","title":"【模型】壽屋《庫特後記》能美．庫特涅芙卡，預定明年 4 月發售！","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/17\/0004971517.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/17\/0004971517.JPG","ts":1604542395,"gp":0,"comment":0,"flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4969729","title":"動畫完結篇《進撃的巨人 The Final Season》確定12\/6放送！","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/29\/0004969729.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/29\/0004969729.JPG","ts":1604382848,"gp":0,"comment":0,"flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4971478","title":"動畫電影《海岬的迷途之家》公開視覺圖、前導宣傳影片 預計2021年上映","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/78\/0004971478.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/78\/0004971478.JPG","ts":1604538674,"gp":0,"comment":0,"flag_more":"0"}],"urge":[{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978776","title":"【電繪】新商品製作","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/76\/0004978776.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/76\/0004978776_B.JPG","ts":1605134102,"gp":"9","comment":4,"btitle":"插畫","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978670","title":"【原創／蝉丸】親吻日","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/70\/0004978670.PNG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/70\/0004978670_B.PNG","ts":1605115560,"gp":"71","comment":15,"btitle":"漫畫","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978739","title":"[翻譯] 偶像大師 灰姑娘劇場 第1469話「泡泡甜甜圈之歌」","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/39\/0004978739.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/39\/0004978739_B.JPG","ts":1605123893,"gp":"20","comment":5,"btitle":"日誌","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978564","title":"【翻譯漫畫】【toufu】toufu的寶可夢世界(35)","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/64\/0004978564.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/64\/0004978564_B.JPG","ts":1605110851,"gp":"93","comment":13,"btitle":"漫畫","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978736","title":"[翻譯] 偶像大師 灰姑娘劇場寬版☆ 第321話","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/36\/0004978736.PNG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/36\/0004978736_B.PNG","ts":1605123592,"gp":"16","comment":4,"btitle":"日誌","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978675","title":"(翻譯)  猫をやめなさい！","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/75\/0004978675.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/75\/0004978675_B.JPG","ts":1605115764,"gp":"118","comment":14,"btitle":"漫畫","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978619","title":"結城七瀬模型化決定!","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/19\/0004978619.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/19\/0004978619_B.JPG","ts":1605113256,"gp":"117","comment":16,"btitle":"插畫","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978146","title":"【漫畫翻譯．Hololive】プリンアラモード - 替代品","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/46\/0004978146.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/46\/0004978146_B.JPG","ts":1605095909,"gp":"278","comment":28,"btitle":"日誌","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978710","title":"【東方廚】最終型態的黃頭貓","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/10\/0004978710.PNG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/10\/0004978710_B.PNG","ts":1605119053,"gp":"28","comment":4,"btitle":"插畫","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978606","title":"11\/12","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/06\/0004978606.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/06\/0004978606_B.JPG","ts":1605112576,"gp":"12","comment":0,"btitle":"插畫","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978133","title":"《Hololive》寶鐘瑪琳","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/33\/0004978133.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/33\/0004978133_B.JPG","ts":1605095338,"gp":"50","comment":9,"btitle":"插畫","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978686","title":"又活過惹一天","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/86\/0004978686.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/86\/0004978686_B.JPG","ts":1605116963,"gp":"12","comment":6,"btitle":"插畫","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978849","title":"【あ吹】聲援情竇初開的妹妹 上【艦隊收藏】","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/49\/0004978849.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/49\/0004978849_B.JPG","ts":1605146346,"gp":"22","comment":5,"btitle":"日誌","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978123","title":"【漫畫翻譯】將棋部學姐-萬聖節","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/23\/0004978123.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/23\/0004978123_B.JPG","ts":1605094806,"gp":"301","comment":22,"btitle":"日誌","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978120","title":"【漫畫翻譯．FGO】藤原たつろ - 回憶如湧泉","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/20\/0004978120.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/20\/0004978120_B.JPG","ts":1605094655,"gp":"347","comment":22,"btitle":"日誌","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978021","title":"【マスクザＪ】加賀的戒指一周年 7","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/21\/0004978021.PNG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/21\/0004978021_B.PNG","ts":1605089307,"gp":"236","comment":9,"btitle":"漫畫","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978678","title":"11\/11 考試期間，我還想著你❤(更新","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/78\/0004978678.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/78\/0004978678_B.JPG","ts":1605115928,"gp":"16","comment":4,"btitle":"日誌","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978644","title":"好好喔 1111耶","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/44\/0004978644.PNG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/44\/0004978644_B.PNG","ts":1605113876,"gp":"56","comment":30,"btitle":"日誌","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978793","title":"〈賭與博〉","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/76\/creation_l82a21994.PNG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/76\/creation_l82a21994_B.PNG","ts":1605136615,"gp":"21","comment":8,"btitle":"日誌","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978568","title":"【日誌】女僕真的香","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/68\/0004978568.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/68\/0004978568_B.JPG","ts":1605111000,"gp":"19","comment":7,"btitle":"日誌","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978649","title":"【二創委託】立繪！","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/49\/0004978649.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/49\/0004978649_B.JPG","ts":1605113968,"gp":"79","comment":8,"btitle":"插畫","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977992","title":"【第七史詩】勝戰的騎士","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/92\/0004977992.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/92\/0004977992_B.JPG","ts":1605088072,"gp":"82","comment":7,"btitle":"插畫","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978516","title":"【繪】pocky日繪圖","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/16\/0004978516.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/16\/0004978516_B.JPG","ts":1605110377,"gp":"214","comment":23,"btitle":"插畫","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978570","title":"短篇動畫《土下座跪求給看》釋出第二波視覺圖與第 13 話先行圖","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/70\/0004978570.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/70\/0004978570_B.JPG","ts":1605111020,"gp":"23","comment":4,"btitle":"日誌","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978789","title":"【模型】eStream《OVERLORD 第三季》夏提雅 -泳裝Ver-，開放預購中！","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/89\/0004978789.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/89\/0004978789_B.JPG","ts":1605135970,"gp":"20","comment":8,"btitle":"日誌","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978788","title":"【模型】eStream《OVERLORD 第三季》雅兒貝德 -泳裝Ver-，開放預購中！","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/88\/0004978788.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/88\/0004978788_B.JPG","ts":1605135938,"gp":"20","comment":5,"btitle":"日誌","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978334","title":"【漫畫翻譯】[こうじ]驅逐艦島風的忘卻3「贏不過哭泣小孩和明石」","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/34\/0004978334.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/34\/0004978334_B.JPG","ts":1605103577,"gp":"157","comment":7,"btitle":"日誌","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978127","title":"草莓口味我喜歡","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/27\/0004978127.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/27\/0004978127_B.JPG","ts":1605095050,"gp":"29","comment":13,"btitle":"插畫","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978368","title":"【漫畫翻譯】[夕海]艦隊ジャーナル428","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/68\/0004978368.PNG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/68\/0004978368_B.PNG","ts":1605104851,"gp":"128","comment":22,"btitle":"日誌","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978313","title":"【漫畫】kamicat-掃購","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/13\/0004978313.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/13\/0004978313_B.JPG","ts":1605102993,"gp":"265","comment":19,"btitle":"日誌","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978166","title":"【翻譯】隨叫隨到一日女僕","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/66\/0004978166.PNG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/66\/0004978166_B.PNG","ts":1605096691,"gp":"136","comment":9,"btitle":"日誌","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978235","title":"【VRoid】秘境的妖精（9）","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/35\/0004978235.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/35\/0004978235_B.JPG","ts":1605100242,"gp":"11","comment":4,"btitle":"插畫","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978160","title":"【繪圖】11\/11","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/60\/0004978160.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/60\/0004978160_B.JPG","ts":1605096297,"gp":"33","comment":3,"btitle":"插畫","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978622","title":"11\/11","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/22\/0004978622.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/22\/0004978622_B.JPG","ts":1605113284,"gp":"47","comment":7,"btitle":"漫畫","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978339","title":"燒毀一切的帕蜜蘭！","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/39\/0004978339.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/39\/0004978339_B.JPG","ts":1605103768,"gp":"86","comment":19,"btitle":"插畫","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978425","title":"【プリコネR公主連結四格漫畫翻譯】第 225 話：特訓的時間","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/25\/0004978425.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/25\/0004978425_B.JPG","ts":1605107304,"gp":"47","comment":13,"btitle":"日誌","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978372","title":"鯊鯊草稿-二創","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/72\/0004978372.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/72\/0004978372_B.JPG","ts":1605105038,"gp":"34","comment":14,"btitle":"插畫","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978153","title":"間桐桜","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/53\/0004978153.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/53\/0004978153_B.JPG","ts":1605096051,"gp":"11","comment":2,"btitle":"插畫","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978683","title":"不用點進來==","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/49\/creation_ricky4124.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/49\/creation_ricky4124_B.JPG","ts":1605116552,"gp":"12","comment":7,"btitle":"日誌","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978448","title":"【漫畫翻譯】《空向》時雨的Pocky Day","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/48\/0004978448.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/48\/0004978448_B.JPG","ts":1605108200,"gp":"118","comment":9,"btitle":"日誌","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978704","title":"少女歌劇立牌印調","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/04\/0004978704.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/04\/0004978704_B.JPG","ts":1605118632,"gp":"9","comment":0,"btitle":"同人商品","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978492","title":"【-ZI-】代號 : ZI-001","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/92\/0004978492.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/92\/0004978492_B.JPG","ts":1605109735,"gp":"19","comment":2,"btitle":"插畫","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978452","title":"【原創】pocky跟雜圖","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/52\/0004978452.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/52\/0004978452_B.JPG","ts":1605108316,"gp":"13","comment":3,"btitle":"插畫","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978596","title":"【動畫雜談】什麼賣萌賣肉？大肌肌脖起才是王道！兩部動畫短感","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/96\/0004978596.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/96\/0004978596_B.JPG","ts":1605112233,"gp":"16","comment":1,"btitle":"日誌","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978411","title":"【艦これ】響Pocky之日","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/11\/0004978411.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/11\/0004978411_B.JPG","ts":1605106984,"gp":"20","comment":2,"btitle":"插畫","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4977979","title":"POCKY之日","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/79\/0004977979.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/79\/0004977979_B.JPG","ts":1605087356,"gp":"140","comment":11,"btitle":"插畫","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978136","title":"【日誌】天氣冷 需要這兩把劍 幫助回溫一下","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/36\/0004978136.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/36\/0004978136_B.JPG","ts":1605095498,"gp":"16","comment":5,"btitle":"日誌","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978220","title":"【漫畫翻譯】大意／あきづき弥","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/20\/0004978220.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/20\/0004978220_B.JPG","ts":1605099398,"gp":"162","comment":23,"btitle":"日誌","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978420","title":"《15歲的神明遊戲02》單行本出版!","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/20\/0004978420.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/20\/0004978420_B.JPG","ts":1605107185,"gp":"5","comment":1,"btitle":"漫畫","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978172","title":"表達愛意(父與子)","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/72\/0004978172.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/72\/0004978172_B.JPG","ts":1605097112,"gp":"5","comment":4,"btitle":"插畫","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978404","title":"Gawr Gura","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/04\/0004978404.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/04\/0004978404_B.JPG","ts":1605106748,"gp":"31","comment":2,"btitle":"插畫","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978599","title":"阿澤姆","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/99\/0004978599.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/99\/0004978599_B.JPG","ts":1605112330,"gp":"8","comment":0,"btitle":"插畫","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978306","title":"ねね人生day39","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/06\/0004978306.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/06\/0004978306_B.JPG","ts":1605102636,"gp":"10","comment":7,"btitle":"日誌","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978244","title":"2020\/11\/11 光棍節快樂","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/44\/0004978244.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/44\/0004978244_B.JPG","ts":1605100514,"gp":"20","comment":15,"btitle":"日誌","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978386","title":"IG日塗1108~1111「pocky日」","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/86\/0004978386.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/86\/0004978386_B.JPG","ts":1605105951,"gp":"62","comment":10,"btitle":"插畫","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978583","title":"鬼滅之刃  炎柱 煉獄杏壽郎","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/83\/0004978583.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/83\/0004978583_B.JPG","ts":1605111532,"gp":"7","comment":3,"btitle":"插畫","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978301","title":"Pocky日 繪圖募集","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/01\/0004978301.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/01\/0004978301_B.JPG","ts":1605102396,"gp":"95","comment":7,"btitle":"插畫","flag_more":"0"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978271","title":"&lt;近期委託筆記本&gt;蜜璃2.0 &amp; 九尾狐","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/71\/0004978271.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/71\/0004978271_B.JPG","ts":1605101593,"gp":"7","comment":4,"btitle":"插畫","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978464","title":"【繪圖】市谷有咲下廚","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/64\/0004978464.JPG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/creationCover\/64\/0004978464_B.JPG","ts":1605108835,"gp":"8","comment":4,"btitle":"插畫","flag_more":"2"},{"sn":"\/\/home.gamer.com.tw\/creationDetail.php?sn=4978423","title":"11.11   好爽","pic":"https:\/\/p2.bahamut.com.tw\/HOME\/86\/creation_memoryq93.PNG","picB":"https:\/\/p2.bahamut.com.tw\/HOME\/86\/creation_memoryq93_B.PNG","ts":1605107260,"gp":"11","comment":7,"btitle":"日誌","flag_more":"0"}]};

for (var key in home_list_o) {
var array = home_list_o[key];
array.forEach(function(item){
if(item.flag_more & 1 && (typeof Cookies.get('age_limit_content') == 'undefined' || Cookies.get('age_limit_content') == 0)){
var uid = item.userid.toLowerCase();
item.picB = 'https://' + 'avatar2.bahamut.com.tw' + '/avataruserpic' + '/' + uid.split("")[0]+ '/' + uid.split("")[1] + '/'+ uid + '/' + uid + '.png';
item.pic = item.picB;
}
});
}
var home_showRows = 8;

var home_list = [];
home_list['hot'] = home_list_o['hot'];
home_list['loft'] = home_list_o['loft'];
home_list['others'] = home_list_o['others'];
home_list['daily'] = home_list_o['daily'];
home_list['art'] = home_list_o['art'];
home_list['comic'] = home_list_o['comic'];
home_list['novel'] = home_list_o['novel'];
home_list['cos'] = home_list_o['cos'];

var home = {
homedata: function() {
return home_list_o;
},

homeShowBlockT: function(s) {
hometimer = setTimeout(function(){ HOME_HOME.homeShowBlock(s); }, 200);
},

homeShowBlock: function(s) {
/*分頁*/

//home_showRowsStart:當前區塊要顯示的第一筆資料的index
var home_showRowsStart = 0;
var totalPage = home_list[s].length / home_showRows; //總頁數

//如果home_groupPage(s) == 1 代表是第二頁在前的類型所以當前頁數1要顯示的資料是第二頁的資料,與頁數2顛倒
if (home_groupPage[s] == 1 && home_nowPage[s] == 0) {
home_showRowsStart = home_showRows;
} else if(home_groupPage[s] == 1 && home_nowPage[s] == 1) {
home_showRowsStart = 0;
} else {
home_showRowsStart = home_nowPage[s] * home_showRows;
}

/*最末頁*/
if( home_showRowsStart >= home_list[s].length ){
home_nowPage[s] = 0;
home_showRowsStart = 0;
}

var urlLink = '';
var urlPic = '';
var countLimit = 0;

var odata = '';

for (var home_no=0; home_no<2; home_no++) {
if (home_showRows <= countLimit) {
break;
}

odata += '<div class="BA-cbox BA-cbox10"'+((1==home_no) ? ' style="padding-bottom:10px;"' : '')+'>';

for (var liveB=0; liveB<4; liveB++) {
var homeCover = home_list[s][home_showRowsStart].pic;
if (window.devicePixelRatio < 1.5) {
homeCover = homeCover + '?w=300';
}
odata += '<div><a href="'+home_list[s][home_showRowsStart].sn+'"><img src="'+homeCover+'"></a><a href="'+home_list[s][home_showRowsStart].sn+'">'+home_list[s][home_showRowsStart].title+'</a></div>';

home_showRowsStart++;
countLimit++;
}

odata += '</div>';
}

odata += '<p class="BA-cbox10B"><a href="'+home_more[s].url+'">&gt; '+home_more[s].str+'</a></p>';

$('#homeOdata').html(odata);

$('.BA-ctag1now').filter('[id^="homeTab_"]').removeClass('BA-ctag1now');
$('#homeTab_'+s).addClass('BA-ctag1now');

/*重設頁碼*/
if( 'hot' != s ){
home_nowPage[s] += 1;

if(home_nowPage[s] >= totalPage){ home_nowPage[s] = 0; }
}
},

homeShowIni: function() {
var homeBlockAry = ["hot","daily","novel","art", "comic", "loft","cos","others"];
var homeBlockIniKey = Math.floor( Math.random() * homeBlockAry.length );
homeBlockIniKey = (8 <= homeBlockIniKey || 0 >= homeBlockIniKey ) ? 0 : homeBlockIniKey;

HOME_HOME.homeShowBlock(homeBlockAry[homeBlockIniKey]);
}
};

window.HOME_HOME = home;
})(window, jQuery);
</script>
<ul class="BA-ctag1" sizcache="0" sizset="1"><!--小屋區塊-->
<li sizcache="0" sizset="1"><a href="javascript:;" onClick="HOME_HOME.homeShowBlock('hot')" id="homeTab_hot" onmouseover="HOME_HOME.homeShowBlockT('hot')" onmouseout="clearTimeout(hometimer)" class="BA-ctag1now">熱門創作</a></li>
<li><a href="javascript:;" onClick="HOME_HOME.homeShowBlock('daily')" id="homeTab_daily" onmouseover="HOME_HOME.homeShowBlockT('daily')" onmouseout="clearTimeout(hometimer)">部落格達人</a></li>
<li><a href="javascript:;" onClick="HOME_HOME.homeShowBlock('novel')" id="homeTab_novel" onmouseover="HOME_HOME.homeShowBlockT('novel')" onmouseout="clearTimeout(hometimer)">小說達人</a></li>
<li><a href="javascript:;" onClick="HOME_HOME.homeShowBlock('art')" id="homeTab_art" onmouseover="HOME_HOME.homeShowBlockT('art')" onmouseout="clearTimeout(hometimer)">插畫達人</a></li>
<li><a href="javascript:;" onClick="HOME_HOME.homeShowBlock('comic')" id="homeTab_comic" onmouseover="HOME_HOME.homeShowBlockT('comic')" onmouseout="clearTimeout(hometimer)">漫畫達人</a></li>
<li><a href="javascript:;" onClick="HOME_HOME.homeShowBlock('loft')" id="homeTab_loft" onmouseover="HOME_HOME.homeShowBlockT('loft')" onmouseout="clearTimeout(hometimer)">精選閣樓</a></li>
<li><a href="javascript:;" onClick="HOME_HOME.homeShowBlock('cos')" id="homeTab_cos" onmouseover="HOME_HOME.homeShowBlockT('cos')" onmouseout="clearTimeout(hometimer)">Cosplay</a></li>
<li><a href="javascript:;" onClick="HOME_HOME.homeShowBlock('others')" id="homeTab_others" onmouseover="HOME_HOME.homeShowBlockT('others')" onmouseout="clearTimeout(hometimer)">巴卦快報</a></li>
</ul>
<div id="homeOdata"></div>
<script type="text/JavaScript">HOME_HOME.homeShowIni();</script>
<script>
var livetimer;
(function(window, $, undefined){
var live1 = "";

var _ad = [];
if (_ad.length) {
live1 = AdBaha.output(_ad, 9);
}

var live2 = [{"broadcaster":"q87112391","title":"\u4e9e\u723e-\u9748\u9b42WARFRAME ?   TW Steam","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_mr0623-1280x720.jpg","machine":"\u5176\u4ed6"},{"broadcaster":"aliceu3c","title":"\u96a8\u4fbf\u73a9\u73a9  \u6253\u5b8c\u5c31\u7761\u89ba \u4f60\u96a8\u4fbf\u770b  (\u00b4\u25d3\u0414\u25d4`)","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_minasey-1280x720.jpg","machine":"\u5176\u4ed6"},{"broadcaster":"crazywolf","title":"\u3010\u7d04\u7ff0\u3011[PS4]{\u523a\u5ba2\u6559\u689d \u7dad\u4eac}\u89c0\u5149\u6a21\u64ec\u5668\u4f86\u5566 \u523a\u5ba2\u53ea\u662f\u9644\u9001  day3","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_bluewildjohn-1280x720.jpg","machine":"PC \u55ae\u6a5f"},{"broadcaster":"bigchoir","title":"11\/12 \u523a\u5ba2\u6559\u689d \u7dad\u4eac\u7d00\u5143  \u807d\u8aaa\u4e3b\u7dda60\u5c0f\u6642....","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_iroisakai-1280x720.jpg","machine":"Switch"},{"broadcaster":"takagishi","title":"\u3010\u4f4f\u904e\u53f0\u7063\u7684\u65e5\u672c\u4eba\u3011\u3066\u3059\u3068\u3010\u4e2d\u65e5\u96d9\u8a9e\u76f4\u64ad\u3011","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_kenjijp_tw-1280x720.jpg","machine":"\u5176\u4ed6"},{"broadcaster":"srwfe","title":"\u3010\u958b\u4e00\u5c0f\u3011\u6c89\u8457\u51b7\u975c\u7684\u5929\u7a57\u4e4b\u54b2\u7a3b\u59ec\u3010\u65b0\u624b\u4e82\u73a9\u3011","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_srwfe-1280x720.jpg","machine":"PS4"},{"broadcaster":"g1208g","title":"\u67cf\u5fb7\u4e4b\u9580 3\uff1a\u5e7d\u6697\u5730\u57df\u7684\u8548\u4eba\u6751","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_twboethius-1280x720.jpg","machine":"PC \u55ae\u6a5f"},{"broadcaster":"miky52029","title":"\u3010\u9ea5\u5947\ud83c\udfc6\u53f0\u3011PSV IS2\u611b\u8207\u6de8\u5316","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_xmikysan-1280x720.jpg","machine":"PS4"},{"broadcaster":"cgz830826","title":"\u300e\uff34\uff23\uff27\uff3a\u300f\u3010\uff34\uff34\u301111\/11 \u7121\u8ab2\u5e73\u6c11\u76dc\u8cca \u4eca\u5929\u9047\u5230\u91ce\u751f\u6797\u5fd7\u7a4e\u6253\u526f\u672c \u4f3c\u4e4e\u6703\u6253\u5230\u5361\u7247??!\u639b\u7db2\u4e2d","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_tcgz19940826-1280x720.jpg","machine":"Android"},{"broadcaster":"Hao0408","title":"[\u7210\u53e3]  \u8996\u89ba\u5c0f\u8aaa\u300a\u82b1\u958b\u516c\u8def\u300bHighway Blossoms episode 2 20201111","cover":"","machine":"\u5176\u4ed6"},{"broadcaster":"runa170907","title":"\u76ae\u5361\u4e18\u5361\u76ae","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_kurochi-1280x720.jpg","machine":"PC \u7dda\u4e0a"},{"broadcaster":"ice710128","title":"\u4eca\u665a\uff0c\u6211\u5148\u4e0d\u9ede...","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_cola700721-1280x720.jpg","machine":"PC \u7dda\u4e0a"},{"broadcaster":"MagicEarly","title":"\u3010Android\u3011\u5922\u5e7b\u6a21\u64ec\u6230","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_kalrto-1280x720.jpg","machine":"Android"},{"broadcaster":"ponhoun","title":"11\/12\u3010\u8001\u83ab\u3011[PC]\u958b\u62d3\u65b0\u64da\u9ede","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_karawmowna-1280x720.jpg","machine":"\u5176\u4ed6"},{"broadcaster":"steven8887","title":"\u3010Risk of Rain 2\u3011\u9700\u8981\u66f4\u591a\u653b\u901f\u3010\u95c7\u96f2\u3042\u304f\u308a\u3011","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_aku_ri-1280x720.jpg","machine":"\u5176\u4ed6"},{"broadcaster":"happygoboy","title":"\u3010\u4ed9\u5883\u50b3\u8aaa\u65b0\u4e16\u4ee3\u7684\u8a95\u751f\u3011\u5403\u738b\u5566 \u8077\u696d \u5deb\u5e2b \u4f3a\u670d\u5668 \u58c1\u756b\u8ff4\u5eca 11\/12","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_happygoboyyo-1280x720.jpg","machine":"Android"},{"broadcaster":"link52052012","title":"\u3010DNF\u3011\u807d\u8aaa\u65e9\u4e0a\u6253\u5bb9\u6613\u51fa\u91d1\u724c\u3002","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_link120520-1280x720.jpg","machine":"PC \u7dda\u4e0a"},{"broadcaster":"Lilia0427","title":"[\u6b23\u4f69] Assassin&#039;s Creed Valhalla \uff08\u523a\u5ba2\u6559\u689d\uff1a\u7dad\u4eac\u7d00\u5143\uff092020.11.12","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_sisterlilia0427-1280x720.jpg","machine":"PS4"},{"broadcaster":"icewindful","title":"\u3010ICE\u3011\u5929\u7a57\u4e4b\u54b2\u7a3b\u59ec  Sakuna: Of Rice and Ruin (\u5929\u7a42\u306e\u30b5\u30af\u30ca\u30d2\u30e1) EP 03","cover":"","machine":"PC \u55ae\u6a5f"},{"broadcaster":"g8bp65k3","title":"\u3010G\u7238\u4eba\u738b\u53f0\u3011109\/11\/12 \u6211\u7684\u706b\u5f88\u5927\uff0c\u4f46\u4e0d\u71d9\u3002\u5927\u63a8\u9b3c\u6ec5\u96fb\u5f71\u7248!","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_g8bp65k3-1280x720.jpg","machine":"PS4"},{"broadcaster":"dada80502","title":"\u3010\u718a\u8c93\u5be6\u6cc1\u3011 \u738b\u570b\u4e4b\u5fc3:\u8a18\u61b6\u65cb\u5f8b #1 |  KINGDOM HEARTS Melody of Memory","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_dada80502-1280x720.jpg","machine":"Switch"},{"broadcaster":"midnightjie","title":"\u3010\u96f6\u8fb0\u3011\u539f\u795e Genshin Impact \u4e9e\u670d\u4e16\u754c6 \u5237\u91d1\u5347\u4e16\u754c7\u524d\u6e96\u5099 11\/12","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_midnight_jie-1280x720.jpg","machine":"PC \u55ae\u6a5f"},{"broadcaster":"cooldog","title":"\ud83e\udd91\u82b1\u679d\ud83e\udd91PC\u2694\ufe0f\u4eca\u665a\u4e3b\u7dda\u6c92\u7834\u5243\u5149\u982d\ud83e\udd70\u9664\u5915\u62bdASUS 3090 24G\u7b49\u5927\u79ae\ud83e\udd73!\u62bd\u734e\ud83d\udc9c\u770b\u53f0\u6b50\u904b\u9f20\u4e0d\u5b8c\ud83d\ude18GOGOGO","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_cooldog0725-1280x720.jpg","machine":"PC \u55ae\u6a5f"},{"broadcaster":"ivygood24","title":"\ufe5dNori\ufe5e \u3010PS4\u3011\u7a7a\u6d1e\u9a0e\u58eb - \u8036\u563f \u6211\u5077\u73a9!","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_nori0117-1280x720.jpg","machine":"\u5176\u4ed6"},{"broadcaster":"READYTRY","title":"\u3010ICE\u3011\u5929\u7a57\u4e4b\u54b2\u7a3b\u59ec  Sakuna: Of Rice and Ruin (\u5929\u7a42\u306e\u30b5\u30af\u30ca\u30d2\u30e1) EP 03","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_icewindful-1280x720.jpg","machine":"Switch"},{"broadcaster":"sindy9999","title":"[\u6cf3\u88dd] \u523a\u5ba2\u7dad\u4eac \u4e0d\u7834\u95dc\u8981\u7761\u89ba | \u5be6\u6cc1\u96dc\u97f3\u3001\u8072\u97f3\u592a\u5c0f\u8acb\u901a\u77e5 |","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_twasd80602-1280x720.jpg","machine":"PC \u55ae\u6a5f"},{"broadcaster":"LORDZEPHYR","title":"\u4f60\u597d \u6211\u6383\u96f7\u52c7\u8005(\u67f1\u67b4\u6756) \u4f86GODFALL  [\u5927\u738b\u4e00\u8d77\u73a9] \u4e0d\u5b9a\u6642\u66ec\u8c93","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_lordzephyrhs-1280x720.jpg","machine":"PC \u55ae\u6a5f"},{"broadcaster":"TomAlber","title":"\u53ea\u8981\u4f60\u61c2\u5361\uff0c\u5361\u5c31\u6703\u5e6b\u52a9\u4f60\uff01(\u25cf-\u25cf) \u4e2d\u592e\u5674\u6cc9\u97f3\u6a02\u53f0","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_ladelfin4-1280x720.jpg","machine":"Android"},{"broadcaster":"o2jamkuso","title":"\u3010Reo\u3011\u9ed1\u9b42\u7248\u7267\u5834\u7269\u8a9e\u3010Taiwan Channel \u3011jp\/ch","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_reoininder-1280x720.jpg","machine":"PS4"},{"broadcaster":"makiserena","title":"\u3010\u5eab\u5eab\u3011\u4eca\u65e5\u63d2\u79e7\ud83c\udf91\ud83c\udf91\ud83c\udf91  \uff89  \u67e5\u8a62\u4eca\u65e5\u904a\u6232\u3010!\u958b\u53f0\u3011\uff89 \u770b\u53f0\u62ff\u5eab\u5e63\u63db\u904a\u6232\ud83c\udfae \u3010!\u5546\u5e97\u3011","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_kukuley_97-1280x720.jpg","machine":"Switch"},{"broadcaster":"z60411123","title":"\u3010\u7da0\u5bf6\u301111\/12 \u65e9\u5b89","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_z60411-1280x720.jpg","machine":"Android"},{"broadcaster":"hsj4526","title":"\u301048hr\u23f0\u3011\u2728\u4e0d\u7834\u95dc\u4e0d\u95dc-\u885d\u5927\u7d50\u5c40\u2728\u770b\u53f0\u62ffSKIN\u3010\u523a\u5ba2\u6559\u689d-\u7dad\u4eac\u3011\u27a4\u3010\ud83c\udf4a\u9001\u8c9d\u6bbc\u5e63\/Mycard\ud83c\udf82 - \u8f38\u5165 !gp\u514c\u63db\u3011\u3010FW\ud83c\udf4a\u8766\u611b\u6a58\u5b50\u3011","cover":"https:\/\/static-cdn.jtvnw.net\/previews-ttv\/live_user_shuteye_orange-1280x720.jpg","machine":"PS4"}];
var livePage=0, html1 = '', len = 0, pit = 6;

//重新組合陣列
var randNo = Math.floor(Math.random() * live2.length);
if( 0 != randNo ) {
var tmp1 = live2.slice(0, randNo);
live2 = live2.slice(randNo);
live2 = live2.concat(tmp1);
}

len = live2.length;

if (live1) {
html1 += '<div class="live_film">';
html1 += '  <span>'+live1.title1+'</span>';
html1 += '  <a href="'+live1.ad_link+'" target="_blank" class="a-mercy-d" data-si="'+live1.datasi+'" data-ssn="'+live1.s_sn+'"><img src="'+live1.cover+'"></a>';
html1 += '  <a href="'+live1.ad_link+'" target="_blank" class="a-mercy-d" data-si="'+live1.datasi+'" data-ssn="'+live1.s_sn+'">'+live1.title2+'</a>';
html1 += '</div>';

len = len + 1;
pit = pit - 1;
}

live = {
LiveShowBlockT: function(s) {
livetimer = setTimeout(function(){ HOME_LIVE.LiveShowBlock(s); }, 200);
},

LiveShowBlock: function(s) {
var livedata = [], html = '', start = 0, pitB = 3;

if( 7 > len ) {
livedata = live2;
} else {
while (livedata.length < pit) {
var tmp = live2.shift();
livedata.push(tmp);
live2.push(tmp);
}
}

for(var i=0; i<2; i++) {
if( !livedata[start] ){
break;
}

html += '<div class="BA-cbox BA-cbox9A"'+((1==i) ? ' style="padding-bottom:10px;"' : '')+'>';

pitB = 3;
if (0 == i && live1) {
html += html1;
pitB = 2;
}

for (var j=0; j<pitB; j++) {
html += '  <div class="liveAction">';
html += '    <span>'+livedata[start].machine+'</span>';
html += '    <a href="//home.gamer.com.tw/live.php?u='+livedata[start].broadcaster+'"><img src="'+livedata[start].cover+'"></a>';
html += '    <a href="//home.gamer.com.tw/live.php?u='+livedata[start].broadcaster+'">'+livedata[start].title+'</a>';
html += '  </div>';

start += 1;

if (!livedata[start]) {
break;
}
}

html += '</div>';
}

html += '<p class="BA-cbox9C"><a href="//home.gamer.com.tw/liveindex.php">&gt; 看更多</a></p>';

$("#LiveBlock").html(html);
}
};

window.HOME_LIVE = live;
})(window, jQuery);
</script>
<ul class="BA-ctag1">
<li><a href="javascript:;" onClick="HOME_LIVE.LiveShowBlock('live')" class="BA-ctag1now" onmouseover="HOME_LIVE.LiveShowBlockT('live')" onmouseout="clearTimeout(livetimer)">實況大廳</a></li>
</ul>
<div id="LiveBlock"></div>
<script>HOME_LIVE.LiveShowBlock('live');</script>
<script>
var guildtimer;
(function(window, $, undefined){
var recommendWiki = null;
var guild = {
guilddata: function() {
return recommendWiki;
},

guildShowBlockDelay: function(s) {
guildtimer = setTimeout(function(){ IndexGuild.guildShowBlock(s); }, 200);
},

guildShowBlock: function(s) {
$('#guild_showdata').html('').removeClass('BA-cbox BA-cbox8 BA-cbox4');

switch(s) {
case 'hotwiki':
$('#guild_showdata').html($('#guild_hotwiki').html()).addClass('BA-cbox BA-cbox8');
break;
case 'hot':
$('#guild_showdata').html($('#guild_hotguild').html()).addClass('BA-cbox BA-cbox8');
break;
case 'pushguild':
break;
default:
}

$('.BA-ctag1now').filter('[id^="guildTab_"]').removeClass('BA-ctag1now');
$('#guildTab_'+s).addClass('BA-ctag1now');
},

guildShowIni: function() {
var guildBlockAry = ["hot","hotwiki"];
var guildBlockIniKey = Math.floor( Math.random() * guildBlockAry.length );
guildBlockIniKey = (guildBlockAry.length <= guildBlockIniKey || 0 >= guildBlockIniKey ) ? 0 : guildBlockIniKey;

IndexGuild.guildShowBlock(guildBlockAry[guildBlockIniKey]);
}
};

window.IndexGuild = guild;
})(window, jQuery);
</script>
<ul class="BA-ctag1"><!--公會區塊-->
<li><a href="javascript:;" onClick="IndexGuild.guildShowBlock('hotwiki')" id="guildTab_hotwiki" onmouseover="IndexGuild.guildShowBlockDelay('hotwiki')" onmouseout="clearTimeout(guildtimer)">熱門Wiki</a></li>
<li><a href="javascript:;" onClick="IndexGuild.guildShowBlock('hot')" id="guildTab_hot" onmouseover="IndexGuild.guildShowBlockDelay('hot')" onmouseout="clearTimeout(guildtimer)">熱門公會</a></li>
</ul>

<div id="guild_showdata"></div>

<div id="guild_hotguild" style="display:none;">
<table style="FLOAT: left"><tbody>
<tr>
<td width="20" align="middle"><span>1</span></td>
<td><p class="BA-cbox5B"><a href="//guild.gamer.com.tw/wikimenu.php?sn=3014">RPG之幻想國度</a></p></td>
<td align="right">3369</td>
</tr>
</tbody><tbody>
<tr>
<td width="20" align="middle"><span>2</span></td>
<td><p class="BA-cbox5B"><a href="//guild.gamer.com.tw/wikimenu.php?sn=8946">PAD場歪肥宅社</a></p></td>
<td align="right">1487</td>
</tr>
</tbody><tbody>
<tr>
<td width="20" align="middle"><span>3</span></td>
<td><p class="BA-cbox5B"><a href="//guild.gamer.com.tw/wikimenu.php?sn=12324">Fate/Grand Order</a></p></td>
<td align="right">1389</td>
</tr>
</tbody><tbody>
<tr>
<td width="20" align="middle"><span>4</span></td>
<td><p class="BA-cbox5B"><a href="//guild.gamer.com.tw/wikimenu.php?sn=4294">Fate/Parallel Worlds</a></p></td>
<td align="right">1189</td>
</tr>
</tbody><tbody>
<tr>
<td width="20" align="middle"><span>5</span></td>
<td><p class="BA-cbox5B"><a href="//guild.gamer.com.tw/wikimenu.php?sn=9969">刀劍亂舞◇審神者神社</a></p></td>
<td align="right">1005</td>
</tr>
</tbody></table><table style="float:right;"><tbody>
<tr>
<td width="20" align="middle"><span>6</span></td>
<td><p class="BA-cbox5B"><a href="//guild.gamer.com.tw/wikimenu.php?sn=13207">原神</a></p></td>
<td align="right">634</td>
</tr>
</tbody><tbody>
<tr>
<td width="20" align="middle"><span>7</span></td>
<td><p class="BA-cbox5B"><a href="//guild.gamer.com.tw/wikimenu.php?sn=11758">真・恋姫†夢想～天下統一伝</a></p></td>
<td align="right">619</td>
</tr>
</tbody><tbody>
<tr>
<td width="20" align="middle"><span>8</span></td>
<td><p class="BA-cbox5B"><a href="//guild.gamer.com.tw/wikimenu.php?sn=786">STEAM平台交流社團</a></p></td>
<td align="right">516</td>
</tr>
</tbody><tbody>
<tr>
<td width="20" align="middle"><span>9</span></td>
<td><p class="BA-cbox5B"><a href="//guild.gamer.com.tw/wikimenu.php?sn=5030">干物妹！</a></p></td>
<td align="right">495</td>
</tr>
</tbody><tbody>
<tr>
<td width="20" align="middle"><span>10</span></td>
<td><p class="BA-cbox5B"><a href="//guild.gamer.com.tw/wikimenu.php?sn=13241">RO 仙境傳說：新世代的誕生</a></p></td>
<td align="right">460</td>
</tr>
</tbody></table><p class="BA-cbox8A"><a href="//guild.gamer.com.tw">&gt; 公會大廳</a></p></div>

<div id="guild_hotwiki" style="display:none;">
<table style="FLOAT: left"><tbody>
<tr>
<td width="20" align="middle"><span>1</span></td>
<td><p class="BA-cbox5B"><a href="//guild.gamer.com.tw/wikimenu.php?sn=3014">RPG之幻想國度</a></p></td>
</tr>
</tbody><tbody>
<tr>
<td width="20" align="middle"><span>2</span></td>
<td><p class="BA-cbox5B"><a href="//guild.gamer.com.tw/wikimenu.php?sn=4294">Fate/Parallel Worlds</a></p></td>
</tr>
</tbody><tbody>
<tr>
<td width="20" align="middle"><span>3</span></td>
<td><p class="BA-cbox5B"><a href="//guild.gamer.com.tw/wikimenu.php?sn=11758">真・恋姫†夢想～天下統一伝</a></p></td>
</tr>
</tbody><tbody>
<tr>
<td width="20" align="middle"><span>4</span></td>
<td><p class="BA-cbox5B"><a href="//guild.gamer.com.tw/wikimenu.php?sn=9617">萌獸學園圖文創作</a></p></td>
</tr>
</tbody><tbody>
<tr>
<td width="20" align="middle"><span>5</span></td>
<td><p class="BA-cbox5B"><a href="//guild.gamer.com.tw/wikimenu.php?sn=13241">RO 仙境傳說：新世代的誕生</a></p></td>
</tr>
</tbody></table><table style="float:right;"><tbody>
<tr>
<td width="20" align="middle"><span>6</span></td>
<td><p class="BA-cbox5B"><a href="//guild.gamer.com.tw/wikimenu.php?sn=7149">箱庭の世界</a></p></td>
</tr>
</tbody><tbody>
<tr>
<td width="20" align="middle"><span>7</span></td>
<td><p class="BA-cbox5B"><a href="//guild.gamer.com.tw/wikimenu.php?sn=11372">我有死亡回歸</a></p></td>
</tr>
</tbody><tbody>
<tr>
<td width="20" align="middle"><span>8</span></td>
<td><p class="BA-cbox5B"><a href="//guild.gamer.com.tw/wikimenu.php?sn=11803">烏托邦</a></p></td>
</tr>
</tbody><tbody>
<tr>
<td width="20" align="middle"><span>9</span></td>
<td><p class="BA-cbox5B"><a href="//guild.gamer.com.tw/wikimenu.php?sn=11400">《星湧大地》</a></p></td>
</tr>
</tbody><tbody>
<tr>
<td width="20" align="middle"><span>10</span></td>
<td><p class="BA-cbox5B"><a href="//guild.gamer.com.tw/wikimenu.php?sn=4951">嫁コレ同好會</a></p></td>
</tr>
</tbody></table><p class="BA-cbox8A"><a href="//guild.gamer.com.tw/?k=2">&gt; 公會大廳</a></p></div>

<script>IndexGuild.guildShowIni()</script>
<script>
(function(window, $, undefined){
var data = {"all":[{"sn":"37030","title":"RO \u4ed9\u5883\u50b3\u8aaa\uff1a\u65b0\u4e16\u4ee3\u7684\u8a95\u751f","click":"1264737"},{"sn":"23805","title":"\u795e\u9b54\u4e4b\u5854","click":"743226"},{"sn":"36730","title":"\u539f\u795e","click":"475632"},{"sn":"38113","title":"\u5b88\u671b\u50b3\u8aaa Guardian Tales ","click":"350993"},{"sn":"7650","title":"\u65b0\u6953\u4e4b\u8c37 ","click":"339932"},{"sn":"9703","title":"\u523a\u5ba2\u6559\u689d","click":"292281"},{"sn":"35604","title":"A3: STILL ALIVE \u5016\u5b58\u8005 ","click":"269646"},{"sn":"17532","title":"\u82f1\u96c4\u806f\u76df League of Legends ","click":"267289"},{"sn":"30861","title":"\u8d85\u7570\u57df\u516c\u4e3b\u9023\u7d50\u2606Re:Dive ","click":"252520"},{"sn":"25052","title":"\u602a\u7269\u5f48\u73e0","click":"246954"},{"sn":"4212","title":"RO \u4ed9\u5883\u50b3\u8aaa Online","click":"222996"},{"sn":"38144","title":"\u704c\u7c43\u9ad8\u624b SLAM DUNK","click":"220801"},{"sn":"33651","title":"\u660e\u65e5\u65b9\u821f ","click":"204657"},{"sn":"21911","title":"\u9748\u9b42\u884c\u8005","click":"199518"},{"sn":"37219","title":"\u6708\u5149\u96d5\u523b\u5e2b","click":"171537"},{"sn":"36833","title":"\u528d\u8207\u9060\u5f81","click":"158645"},{"sn":"18966","title":"\u6d41\u4ea1\u9eef\u9053 Path of Exile","click":"151254"},{"sn":"26742","title":"Fate\/Grand Order","click":"145266"},{"sn":"22153","title":"\u9f8d\u65cf\u62fc\u5716 Puzzle &amp; Dragons ","click":"136890"},{"sn":"29220","title":"Another Eden\uff1a\u7a7f\u8d8a\u6642\u7a7a\u7684\u8c93 ","click":"133717"}], "online":[{"sn":"7650","title":"\u65b0\u6953\u4e4b\u8c37 ","click":"339932"},{"sn":"17532","title":"\u82f1\u96c4\u806f\u76df League of Legends ","click":"267289"},{"sn":"4212","title":"RO \u4ed9\u5883\u50b3\u8aaa Online","click":"222996"},{"sn":"21911","title":"\u9748\u9b42\u884c\u8005","click":"199518"},{"sn":"18966","title":"\u6d41\u4ea1\u9eef\u9053 Path of Exile","click":"151254"},{"sn":"19017","title":"\u9ed1\u8272\u6c99\u6f20 BLACK DESERT ","click":"126512"},{"sn":"5219","title":"WOW \u9b54\u7378\u4e16\u754c","click":"77228"},{"sn":"36072","title":"APEX \u82f1\u96c4 ","click":"76203"},{"sn":"29919","title":"Dead by Daylight\uff08\u9ece\u660e\u6b7b\u7dda\uff09","click":"58833"},{"sn":"16583","title":"\u65b0\u746a\u5947\u82f1\u96c4\u50b3","click":"26135"},{"sn":"8897","title":"\u98c4\u6d41\u5e7b\u5883 Online","click":"20844"},{"sn":"12980","title":"\u528d\u9748 Blade &amp; Soul","click":"20565"},{"sn":"19552","title":"\u53e4\u528d\u5947\u8b5a\u7db2\u8def\u7248","click":"18269"},{"sn":"22797","title":"Warframe","click":"17957"},{"sn":"12259","title":"\u827e\u723e\u4e4b\u5149","click":"17533"},{"sn":"21400","title":"\u6697\u9ed1\u7834\u58de\u795e 3\uff1a\u596a\u9b42\u4e4b\u942e","click":"16422"},{"sn":"30786","title":"\u6230\u610f","click":"15116"},{"sn":"24044","title":"\u7210\u77f3\u6230\u8a18","click":"14088"},{"sn":"17608","title":"Final Fantasy XIV","click":"13834"},{"sn":"7422","title":"\u65b0\u746a\u5947 Mabinogi","click":"13226"}], "tv":[{"sn":"9703","title":"\u523a\u5ba2\u6559\u689d","click":"292281"},{"sn":"5786","title":"\u9b54\u7269\u7375\u4eba","click":"113304"},{"sn":"1647","title":"\u795e\u5947\u5bf6\u8c9d\uff08\u7cbe\u9748\u5bf6\u53ef\u5922\uff09\u7cfb\u5217 ","click":"81147"},{"sn":"8448","title":"\u4ec1\u738b","click":"25573"},{"sn":"725","title":"\u904a\u6232\u738b \u7cfb\u5217","click":"17960"},{"sn":"7287","title":"\u52d5\u7269\u4e4b\u68ee \u7cfb\u5217 \uff08\u52d5\u7269\u68ee\u53cb\u6703\uff09 ","click":"17304"},{"sn":"4737","title":"\u4fe0\u76dc\u7375\u8eca\u624b \u7cfb\u5217","click":"15321"},{"sn":"37154","title":"\u5c0d\u99ac\u6230\u9b3c ","click":"11788"},{"sn":"24460","title":"\u6e6f\u59c6\u514b\u862d\u897f\uff1a\u5168\u5883\u5c01\u9396 \u7cfb\u5217 ","click":"11092"},{"sn":"5371","title":"\u6c7a\u52dd\u6642\u523b","click":"9297"},{"sn":"1689","title":"\u85a9\u723e\u9054\u50b3\u8aaa \u7cfb\u5217","click":"8930"},{"sn":"1405","title":"\u7267\u5834\u7269\u8a9e \u7cfb\u5217","click":"8653"},{"sn":"24507","title":"Destiny","click":"7637"},{"sn":"17341","title":"\u78a7\u8840\u72c2\u6bba","click":"7616"},{"sn":"1916","title":"\u60e1\u9b54\u7375\u4eba ","click":"6168"},{"sn":"34434","title":"\u96bb\u72fc","click":"5855"},{"sn":"25","title":"\u60e1\u9748\u53e4\u5821","click":"5418"},{"sn":"197","title":"\u5922\u5e7b\u4e4b\u661f\u7db2\u8def\u4fc3\u9032\u6703","click":"5217"},{"sn":"19967","title":"\u9f8d\u65cf\u6559\u7fa9","click":"5183"},{"sn":"179","title":"FF \u6700\u7d42\u5e7b\u60f3 \u7cfb\u5217\uff08\u592a\u7a7a\u6230\u58eb\uff09","click":"4886"}], "pc":[{"sn":"18673","title":"Minecraft \u6211\u7684\u4e16\u754c\uff08\u7576\u500b\u5275\u4e16\u795e\uff09","click":"53674"},{"sn":"1054","title":"\u8679\u5f69\u516d\u865f \u7cfb\u5217","click":"13414"},{"sn":"22699","title":"\u770b\u9580\u72d7 Watch Dogs","click":"12084"},{"sn":"2023","title":"\u7570\u5875\u9918\u751f \u7cfb\u5217","click":"10195"},{"sn":"1223","title":"\u8ed2\u8f45\u528d \u55ae\u6a5f\u7cfb\u5217","click":"9949"},{"sn":"2526","title":"\u4e0a\u53e4\u5377\u8ef8 \u7cfb\u5217(The Elder Scrolls)","click":"7338"},{"sn":"1122","title":"\u4e16\u7d00\u5e1d\u570b \u7cfb\u5217","click":"6895"},{"sn":"19441","title":"Grim Dawn","click":"6270"},{"sn":"10859","title":"\u91d1\u5eb8\u7fa4\u4fe0\u50b3","click":"6159"},{"sn":"27487","title":"\u7a9f\u7abf\u9a0e\u58eb","click":"6135"},{"sn":"7364","title":"\u5deb\u5e2b The Witcher \u7cfb\u5217","click":"5997"},{"sn":"3044","title":"\u9b54\u7378\u722d\u9738","click":"5973"},{"sn":"23379","title":"\u96fb\u99ad\u53db\u5ba2 2077 ","click":"5593"},{"sn":"3148","title":"\u4fe0\u5ba2\u98a8\u96f2\u50b3 \u7cfb\u5217 ","click":"5148"},{"sn":"4236","title":"\u5e55\u5e9c\u5c07\u8ecd\u3001\u5168\u8ecd\u7834\u6575\u7cfb\u5217","click":"4485"},{"sn":"742","title":"\u6697\u9ed1\u7834\u58de\u795e","click":"4457"},{"sn":"39247","title":"Among Us","click":"4115"},{"sn":"35739","title":"\u9ed1\u5e1d\u65af","click":"3999"},{"sn":"36340","title":"Chronicon","click":"3884"},{"sn":"4918","title":"RPG\u88fd\u4f5c\u5927\u5e2b","click":"3679"}], "web":[{"sn":"24698","title":"\u8266\u968a Collection","click":"5772"},{"sn":"21259","title":"\u7cbe\u9748\u5bf6\u53ef\u5922 TCG Online ","click":"4789"},{"sn":"35303","title":"\u5c0d\u9b54\u5fcd RPG","click":"3921"},{"sn":"36987","title":"MGCM\uff08\u30de\u30b8\u30ab\u30df\uff09 ","click":"2959"},{"sn":"26608","title":"\u5fa1\u57ce\u6536\u85cf","click":"2421"},{"sn":"27417","title":"\u5343\u5e74\u6230\u722d Aigis\uff5e\u4e00\u822c\u7248\uff5e","click":"2154"},{"sn":"34832","title":"\u51cd\u4eac NECRO \u81ea\u6bba\u4efb\u52d9","click":"2140"},{"sn":"28000","title":"\u7f8e\u5c11\u5973\u82b1\u9a0e\u58eb","click":"1950"},{"sn":"30939","title":"\u795e\u59ec project","click":"1630"},{"sn":"38985","title":"\u8ff7\u9727\u5217\u8eca\u5c11\u5973\uff5e\u5f9e\u9727\u4e4b\u4e16\u754c\u7684\u8eca\u7a97\uff5e","click":"1575"},{"sn":"34345","title":"\u30aa\u30c8\u30ae\u30d5\u30ed\u30f3\u30c6\u30a3\u30a2","click":"999"},{"sn":"36476","title":"\u96c0\u9b42\u9ebb\u5c07majsoul","click":"808"},{"sn":"33791","title":"\u5076\u50cf\u5927\u5e2b \u9583\u8000\u8272\u5f69","click":"722"},{"sn":"20848","title":"\u81e5\u9f8d\u541f","click":"653","api":"http%3A%2F%2Fport.efunfun.com%2Fmc%2Fgamer.jsp%3Fgc%3Dwly","acg_sn":50060},{"sn":"36958","title":"\u6b0a\u529b\u904a\u6232\uff1a\u51dc\u51ac\u5c07\u81f3","click":"583"},{"sn":"60320","title":"\u7db2\u9801\u904a\u6232\u7d9c\u5408\u8a0e\u8ad6\u5340","click":"429"},{"sn":"27058","title":"\u5200\u528d\u4e82\u821e","click":"424"},{"sn":"11799","title":"OGame","click":"384"},{"sn":"20111","title":"Unlight","click":"367"},{"sn":"18630","title":"\u4e09\u570b\u6bba Online","click":"364"}], "ac":[{"sn":"46988","title":"\u9b3c\u6ec5\u4e4b\u5203","click":"62567","animeSn":"98149"},{"sn":"109","title":"\u92fc\u5f48","click":"14240","animeSn":"96881"},{"sn":"47157","title":"\u661f\u671f\u4e00\u7684\u8c50\u6eff","click":"11466"},{"sn":"5818","title":"\u822a\u6d77\u738b One Piece\uff08\u6d77\u8cca\u738b\uff09","click":"11040"},{"sn":"43473","title":"\u9032\u64ca\u7684\u5de8\u4eba","click":"11026","animeSn":"59221"},{"sn":"60073","title":"\u71c3\u3048\u308d!!\u7279\u651d\u9b42","click":"10413","animeSn":"80411"},{"sn":"48102","title":"\u5492\u8853\u8ff4\u6230","click":"8693","animeSn":"112457"},{"sn":"45581","title":"\u62f3\u9858\u963f\u4fee\u7f85","click":"7796"},{"sn":"45499","title":"\u5728\u5730\u4e0b\u57ce\u5c0b\u6c42\u9082\u9005\u662f\u5426\u641e\u932f\u4e86\u4ec0\u9ebc","click":"6765","animeSn":"82306"},{"sn":"46197","title":"\u8d64\u5742\u30a2\u30ab \u4f5c\u54c1\u96c6\uff08\u8f1d\u591c\u59ec\u60f3\u8b93\u4eba\u544a\u767d\uff09 ","click":"6183","animeSn":"98176"},{"sn":"42388","title":"\u5ddd\u539f\u792b \u4f5c\u54c1\u96c6","click":"6108","animeSn":"52206"},{"sn":"46236","title":"\u5800\u8d8a\u8015\u5e73 \u4f5c\u54c1\u96c6\uff08\u6211\u7684\u82f1\u96c4\u5b78\u9662\uff09 ","click":"5327","animeSn":"91866"},{"sn":"47377","title":"\u9b54\u5973\u4e4b\u65c5","click":"4451","animeSn":"112458"},{"sn":"47521","title":"\u5bae\u5cf6\u79ae\u540f \u4f5c\u54c1\u96c6\uff08\u51fa\u79df\u5973\u53cb\uff09","click":"4335","animeSn":"109203"},{"sn":"7255","title":"\u6578\u78bc\u5bf6\u8c9d\u7cfb\u5217 Digimon","click":"4039"},{"sn":"40005","title":"\u66ae\u87ec\u9cf4\u6ce3\u6642","click":"3960","animeSn":"112456"},{"sn":"44924","title":"\u9b54\u6cd5\u79d1\u9ad8\u4e2d\u7684\u52a3\u7b49\u751f","click":"3948","animeSn":"82332"},{"sn":"9339","title":"\u6751\u7530\u96c4\u4ecb \u4f5c\u54c1\u96c6\uff08\u4e00\u62f3\u8d85\u4eba\uff09","click":"3932","animeSn":"75930"},{"sn":"47340","title":"\u5728\u9b54\u738b\u57ce\u8aaa\u665a\u5b89","click":"3877","animeSn":"112452"},{"sn":"46542","title":"Re\uff1a\u5f9e\u96f6\u958b\u59cb\u7684\u7570\u4e16\u754c\u751f\u6d3b","click":"3838","animeSn":"78471"}], "talk":[{"sn":"60030","title":"\u96fb\u8166\u61c9\u7528\u7d9c\u5408\u8a0e\u8ad6 ","click":"335142"},{"sn":"60599","title":"Steam \u7d9c\u5408\u8a0e\u8ad6\u677f","click":"92969"},{"sn":"60036","title":"\u7d9c\u5408\u516c\u4ed4\u73a9\u5177\u8a0e\u8ad6\u5340","click":"90900"},{"sn":"60559","title":"\u667a\u6167\u578b\u624b\u6a5f ","click":"70790"},{"sn":"60001","title":"\u96fb\u8996\u904a\u6a02\u5668\u7d9c\u5408\u8a0e\u8ad6\u5340","click":"32308"},{"sn":"60037","title":"\u52d5\u6f2b\u76f8\u95dc\u7d9c\u5408","click":"19447"},{"sn":"60053","title":"\u6a21\u578b\u6280\u8853\u8207\u8cc7\u8a0a","click":"13977"},{"sn":"867","title":"\u5e03\u888b\u6232\u6587\u5316\u7d9c\u5408\u8a0e\u8ad6\u5340","click":"12501"},{"sn":"60552","title":"\u507d\u5a18\u5411\u4e0a\u59d4\u54e1\u6703","click":"4178"},{"sn":"60143","title":"\u756b\u6280\u4ea4\u6d41\u5340","click":"4010"},{"sn":"60415","title":"\u8f15\u5c0f\u8aaa\u7d9c\u5408","click":"2661"},{"sn":"60606","title":"VR \u865b\u64ec\u5be6\u5883\u7d9c\u5408\u8a0e\u8ad6","click":"2619"},{"sn":"60253","title":"Cosplay \u96fb\u6f2b\u89d2\u8272\u626e\u6f14\u54c8\u5566\u677f","click":"1275"},{"sn":"60602","title":"Unity3D \u904a\u6232\u5f15\u64ce","click":"1254"},{"sn":"60059","title":"\u624b\u6a5f\u8207\u884c\u52d5\u904a\u6232\u8a0e\u8ad6\u5340","click":"1246"},{"sn":"60429","title":"\u684c\u4e0a\u89d2\u8272\u626e\u6f14\u904a\u6232(TRPG)\u8a0e\u8ad6","click":"1237"},{"sn":"60503","title":"\u9b54\u7378\u6069\u4ec7\u9304","click":"1224"},{"sn":"60605","title":"\u6b50\u7f8e\u52d5\u756b\u7d9c\u5408\u8a0e\u8ad6","click":"988"},{"sn":"60183","title":"\u5c0f\u54c1\u904a\u6232\u5206\u4eab\u8a0e\u8ad6\u54c8\u5566\u677f","click":"740"},{"sn":"60094","title":"\u5df4\u54c8\u59c6\u7279\u52c7\u8005\u9020\u578b\u54c8\u5566\u677f ","click":"709"}], "rest":[{"sn":"60076","title":"\u5834\u5916\u4f11\u61a9\u5340 ","click":"4410332"},{"sn":"60084","title":"\u6b61\u6a02\u60e1\u641e KUSO","click":"71437"},{"sn":"60608","title":"\u865b\u64ec Youtuber\uff08Vtuber\uff09 ","click":"51437"},{"sn":"60561","title":"\u8077\u5834\u7518\u82e6\u8ac7 ","click":"33910"},{"sn":"60535","title":"\u5f71\u97f3\u8996\u807d\u8a0e\u8ad6\u5340","click":"13078"},{"sn":"60208","title":"\u8ecd\u4e8b\u7b56\u7565","click":"11640"},{"sn":"60091","title":"\u9178\u751c\u82e6\u8fa3\u7559\u8a00\u677f","click":"11457"},{"sn":"60545","title":"\u6c7d\u6a5f\u8eca\u8a0e\u8ad6","click":"10925"},{"sn":"60433","title":"\u4e16\u754c\u4e4b\u4e0d\u53ef\u601d\u8b70","click":"7592"},{"sn":"60200","title":"\u96fb\u5f71\u5a1b\u6a02\u65b0\u8996\u754c","click":"7120"},{"sn":"60201","title":"\u6050\u6016\u9a5a\u609a","click":"4869"},{"sn":"60534","title":"\u91ce\u6230 (\u751f\u5b58) \u904a\u6232","click":"3478"},{"sn":"4373","title":"NBA \u7cfb\u5217","click":"3346"},{"sn":"60481","title":"\u71b1\u9580\u7f8e\u570b\u5f71\u96c6","click":"1644"},{"sn":"60592","title":"\u7d9c\u5408\u5be6\u6cc1\u8a0e\u8ad6\u677f","click":"1298"},{"sn":"60546","title":"\u5e78\u798f\u5bf5\u7269\u4ea4\u6d41\u5340 ","click":"1150"},{"sn":"60440","title":"\u8b1b\u8ac7\u8aaa\u8ad6","click":"1047"},{"sn":"60564","title":"\u8ecd\u65c5\u751f\u6d3b","click":"876"},{"sn":"60292","title":"\u7a0b\u5f0f\u8a2d\u8a08\u677f","click":"664"},{"sn":"60490","title":"\u96fb\u73a9\u4eba\u6c42\u8077\u4e2d\u5fc3 ","click":"620"}], "hand":[{"sn":"37030","title":"RO \u4ed9\u5883\u50b3\u8aaa\uff1a\u65b0\u4e16\u4ee3\u7684\u8a95\u751f","click":"1264737"},{"sn":"23805","title":"\u795e\u9b54\u4e4b\u5854","click":"743226"},{"sn":"36730","title":"\u539f\u795e","click":"475632"},{"sn":"38113","title":"\u5b88\u671b\u50b3\u8aaa Guardian Tales ","click":"350993"},{"sn":"35604","title":"A3: STILL ALIVE \u5016\u5b58\u8005 ","click":"269646"},{"sn":"30861","title":"\u8d85\u7570\u57df\u516c\u4e3b\u9023\u7d50\u2606Re:Dive ","click":"252520"},{"sn":"25052","title":"\u602a\u7269\u5f48\u73e0","click":"246954"},{"sn":"38144","title":"\u704c\u7c43\u9ad8\u624b SLAM DUNK","click":"220801"},{"sn":"33651","title":"\u660e\u65e5\u65b9\u821f ","click":"204657"},{"sn":"37219","title":"\u6708\u5149\u96d5\u523b\u5e2b","click":"171537"},{"sn":"36833","title":"\u528d\u8207\u9060\u5f81","click":"158645"},{"sn":"26742","title":"Fate\/Grand Order","click":"145266"},{"sn":"22153","title":"\u9f8d\u65cf\u62fc\u5716 Puzzle &amp; Dragons ","click":"136890"},{"sn":"29220","title":"Another Eden\uff1a\u7a7f\u8d8a\u6642\u7a7a\u7684\u8c93 ","click":"133717"},{"sn":"39034","title":"\u9ed1\u6f6e\uff1a\u6df1\u6d77\u89ba\u9192","click":"106932"},{"sn":"34880","title":"\u7b2c\u4e03\u53f2\u8a69 ","click":"103236"},{"sn":"25908","title":"\u5929\u5802 Mobile ","click":"102894"},{"sn":"37563","title":"\u6700\u5f37\u8778\u725b","click":"91594"},{"sn":"30518","title":"\u50b3\u8aaa\u5c0d\u6c7a Arena of Valor ","click":"88873"},{"sn":"33596","title":"\u5922\u5e7b\u6a21\u64ec\u6230 \u624b\u6a5f\u7248 ","click":"85425"}]};

var more = {"all":"//forum.gamer.com.tw/","online":"//forum.gamer.com.tw/?c=400","tv":"//forum.gamer.com.tw/?c=52","pc":"//forum.gamer.com.tw/?c=40","web":"//forum.gamer.com.tw/?c=80","hand":"//forum.gamer.com.tw/?c=94","ac":"//forum.gamer.com.tw/?c=22","talk":"//forum.gamer.com.tw/?c=61","rest":"//forum.gamer.com.tw/?c=95"};

var forumhot = {
'hotboard_change': function(who) {
hotboardflag = setTimeout("forumhot.hotboard_changea('"+who+"')",200);
},

'hotboard_changea': function(who) {
var contx = '';
contx += '<p class="BA-cbox5A"><a href="//forum.gamer.com.tw/A.php?bsn=60645">PS5</a>|<a href="//forum.gamer.com.tw/A.php?bsn=60596">PS4</a>|<a href="//forum.gamer.com.tw/A.php?bsn=60646">XBSX</a>|<a href="//forum.gamer.com.tw/A.php?bsn=60597">XBOne</a>|<a href="//forum.gamer.com.tw/A.php?bsn=31587">NS</a>|<a href="//forum.gamer.com.tw/A.php?bsn=60521">iOS</a>|<a href="//forum.gamer.com.tw/A.php?bsn=60528">Android</a><span class="pop-update">11/11 12:00 更新</span></p><table style="float:left;"><tr><td align="middle"><img src="https://i2.bahamut.com.tw/index_w/new_icon1.gif" width="12" height="10" /></td><td>哈啦板</td><td align="right">昨日人氣</td></tr>';

for(i=0;i<data[who].length;i++) {
if( 10 == i ) {
contx += '</table><table style="float:right;"><tr><td align="middle"><img src="https://i2.bahamut.com.tw/index_w/new_icon1.gif" width="12" height="10" /></td><td>哈啦板</td><td align="right" style="white-space:nowrap;">昨日人氣</td></tr>';
}

contx += '<tr><td width="20" align="middle"><span>'+(i+1)+'</span></td><td><p class="BA-cbox5B"><a href="//forum.gamer.com.tw/A.php?bsn='+data[who][i].sn+'">'+data[who][i].title+'</a>';

if( data[who][i].animeSn > 0) {
contx += '<a href="//ani.gamer.com.tw/animeRef.php?sn='+data[who][i].animeSn+'" class="BA-freewatch" target="_blank">免費看</a>';
}

contx += '</p></td><td align="right">'+data[who][i].click+'</td></tr>';
}

contx += '</table><p class="BA-cbox5C"><a href="'+more[who]+'" target="_blank">&gt; 看更多</a></p>';

$('.BA-ctag1now').filter('[id^="hotboard_"]').removeClass('BA-ctag1now');
$('#hotboard_'+who).addClass('BA-ctag1now');
$('#hotboard').html(contx);
}
};

window.forumhot = forumhot;
})(window, jQuery);
</script>
<ul class="BA-ctag1">
<li><a id="hotboard_all" href="javascript:;" onclick="forumhot.hotboard_changea('all')" onmouseover="forumhot.hotboard_change('all')" onmouseout="clearTimeout(hotboardflag)" class="BA-ctag1now">熱門看板</a></li>
<li><a id="hotboard_hand" href="javascript:;" onclick="forumhot.hotboard_changea('hand')" onmouseover="forumhot.hotboard_change('hand')" onmouseout="clearTimeout(hotboardflag)">手機遊戲</a></li>
<li><a id="hotboard_online" href="javascript:;" onclick="forumhot.hotboard_changea('online')" onmouseover="forumhot.hotboard_change('online')" onmouseout="clearTimeout(hotboardflag)">線上遊戲</a></li>
<li><a id="hotboard_tv" href="javascript:;" onclick="forumhot.hotboard_changea('tv')" onmouseover="forumhot.hotboard_change('tv')" onmouseout="clearTimeout(hotboardflag)">TV 掌機</a></li>
<li><a id="hotboard_pc" href="javascript:;" onclick="forumhot.hotboard_changea('pc')" onmouseover="forumhot.hotboard_change('pc')" onmouseout="clearTimeout(hotboardflag)">PC Game</a></li>
<li><a id="hotboard_web" href="javascript:;" onclick="forumhot.hotboard_changea('web')" onmouseover="forumhot.hotboard_change('web')" onmouseout="clearTimeout(hotboardflag)">網頁</a></li>
<li><a id="hotboard_ac" href="javascript:;" onclick="forumhot.hotboard_changea('ac')" onmouseover="forumhot.hotboard_change('ac')" onmouseout="clearTimeout(hotboardflag)">動漫</a></li>
<li><a id="hotboard_talk" href="javascript:;" onclick="forumhot.hotboard_changea('talk')" onmouseover="forumhot.hotboard_change('talk')" onmouseout="clearTimeout(hotboardflag)">主題</a></li>
<li><a id="hotboard_rest" href="javascript:;" onclick="forumhot.hotboard_changea('rest')" onmouseover="forumhot.hotboard_change('rest')" onmouseout="clearTimeout(hotboardflag)">場外</a></li>
</ul>
<div id="hotboard" class="BA-cbox BA-cbox5">
<p class="BA-cbox5A"><a href="//forum.gamer.com.tw/A.php?bsn=60645">PS5</a>|<a href="//forum.gamer.com.tw/A.php?bsn=60596">PS4</a>|<a href="//forum.gamer.com.tw/A.php?bsn=60646">XBSX</a>|<a href="//forum.gamer.com.tw/A.php?bsn=60597">XBOne</a>|<a href="//forum.gamer.com.tw/A.php?bsn=31587">NS</a>|<a href="//forum.gamer.com.tw/A.php?bsn=60521">iOS</a>|<a href="//forum.gamer.com.tw/A.php?bsn=60528">Android</a><span class="pop-update">11/11 12:00 更新</span></p>
<table style=" FLOAT: left">
<tr>
<td align="middle"><img src="https://i2.bahamut.com.tw/index_w/new_icon1.gif" width="12" height="10" /></td>
<td>哈啦板</td>
<td align="right">昨日人氣</td>
</tr><tr>
<td width="20" align="middle"><span>1</span></td>
<td><p class="BA-cbox5B"><a href="//forum.gamer.com.tw/A.php?bsn=37030">RO 仙境傳說：新世代的誕生</a></p></td>
<td align="right">1264737</td>
</tr><tr>
<td width="20" align="middle"><span>2</span></td>
<td><p class="BA-cbox5B"><a href="//forum.gamer.com.tw/A.php?bsn=23805">神魔之塔</a></p></td>
<td align="right">743226</td>
</tr><tr>
<td width="20" align="middle"><span>3</span></td>
<td><p class="BA-cbox5B"><a href="//forum.gamer.com.tw/A.php?bsn=36730">原神</a></p></td>
<td align="right">475632</td>
</tr><tr>
<td width="20" align="middle"><span>4</span></td>
<td><p class="BA-cbox5B"><a href="//forum.gamer.com.tw/A.php?bsn=38113">守望傳說 Guardian Tales </a></p></td>
<td align="right">350993</td>
</tr><tr>
<td width="20" align="middle"><span>5</span></td>
<td><p class="BA-cbox5B"><a href="//forum.gamer.com.tw/A.php?bsn=7650">新楓之谷 </a></p></td>
<td align="right">339932</td>
</tr><tr>
<td width="20" align="middle"><span>6</span></td>
<td><p class="BA-cbox5B"><a href="//forum.gamer.com.tw/A.php?bsn=9703">刺客教條</a></p></td>
<td align="right">292281</td>
</tr><tr>
<td width="20" align="middle"><span>7</span></td>
<td><p class="BA-cbox5B"><a href="//forum.gamer.com.tw/A.php?bsn=35604">A3: STILL ALIVE 倖存者 </a></p></td>
<td align="right">269646</td>
</tr><tr>
<td width="20" align="middle"><span>8</span></td>
<td><p class="BA-cbox5B"><a href="//forum.gamer.com.tw/A.php?bsn=17532">英雄聯盟 League of Legends </a></p></td>
<td align="right">267289</td>
</tr><tr>
<td width="20" align="middle"><span>9</span></td>
<td><p class="BA-cbox5B"><a href="//forum.gamer.com.tw/A.php?bsn=30861">超異域公主連結☆Re:Dive </a></p></td>
<td align="right">252520</td>
</tr><tr>
<td width="20" align="middle"><span>10</span></td>
<td><p class="BA-cbox5B"><a href="//forum.gamer.com.tw/A.php?bsn=25052">怪物彈珠</a></p></td>
<td align="right">246954</td>
</tr></table>
<table style="float:right;">
<tr>
<td align="middle"><img src="https://i2.bahamut.com.tw/index_w/new_icon1.gif" width="12" height="10" /></td>
<td>哈啦板</td>
<td align="right" style="white-space:nowrap;">昨日人氣</td>
</tr><tr>
<td width="20" align="middle"><span>11</span></td>
<td><p class="BA-cbox5B"><a href="//forum.gamer.com.tw/A.php?bsn=4212">RO 仙境傳說 Online</a></p></td>
<td align="right">222996</td>
</tr><tr>
<td width="20" align="middle"><span>12</span></td>
<td><p class="BA-cbox5B"><a href="//forum.gamer.com.tw/A.php?bsn=38144">灌籃高手 SLAM DUNK</a></p></td>
<td align="right">220801</td>
</tr><tr>
<td width="20" align="middle"><span>13</span></td>
<td><p class="BA-cbox5B"><a href="//forum.gamer.com.tw/A.php?bsn=33651">明日方舟 </a></p></td>
<td align="right">204657</td>
</tr><tr>
<td width="20" align="middle"><span>14</span></td>
<td><p class="BA-cbox5B"><a href="//forum.gamer.com.tw/A.php?bsn=21911">靈魂行者</a></p></td>
<td align="right">199518</td>
</tr><tr>
<td width="20" align="middle"><span>15</span></td>
<td><p class="BA-cbox5B"><a href="//forum.gamer.com.tw/A.php?bsn=37219">月光雕刻師</a></p></td>
<td align="right">171537</td>
</tr><tr>
<td width="20" align="middle"><span>16</span></td>
<td><p class="BA-cbox5B"><a href="//forum.gamer.com.tw/A.php?bsn=36833">劍與遠征</a></p></td>
<td align="right">158645</td>
</tr><tr>
<td width="20" align="middle"><span>17</span></td>
<td><p class="BA-cbox5B"><a href="//forum.gamer.com.tw/A.php?bsn=18966">流亡黯道 Path of Exile</a></p></td>
<td align="right">151254</td>
</tr><tr>
<td width="20" align="middle"><span>18</span></td>
<td><p class="BA-cbox5B"><a href="//forum.gamer.com.tw/A.php?bsn=26742">Fate/Grand Order</a></p></td>
<td align="right">145266</td>
</tr><tr>
<td width="20" align="middle"><span>19</span></td>
<td><p class="BA-cbox5B"><a href="//forum.gamer.com.tw/A.php?bsn=22153">龍族拼圖 Puzzle &amp; Dragons </a></p></td>
<td align="right">136890</td>
</tr><tr>
<td width="20" align="middle"><span>20</span></td>
<td><p class="BA-cbox5B"><a href="//forum.gamer.com.tw/A.php?bsn=29220">Another Eden：穿越時空的貓 </a></p></td>
<td align="right">133717</td>
</tr></table>
<p class="BA-cbox5C"><a href="//forum.gamer.com.tw/" target="_blank">&gt; 看更多</a></p>
</div><script>
(function(window, $, undefined) {
'use strict';

var _ad = [];
if (_ad.length) {
document.write('<div class="BA-cboxAD">' + AdBaha.output(_ad, 15) + '</div>');
}
})(window, jQuery);
</script>
<script>
var forumtimer;
(function(window, $, undefined){
var data = {"other":[{"bsn":"60608","snA":"1807","title":"\u3010\u6f2b\u756b\u7ffb\u8b6f\u3011Hololive\u56db\u683c - \u30d7\u30ea\u30f3\u30a2\u30e9\u30e2\u30fc\u30c9","author":"b775232000","wtime":"2020-11-11 20:24:59","pic":"https:\/\/i.imgur.com\/qKjq0Cq.jpg","gp":"174","tnum":185,"comment":10,"ts":1605097499,"btitle":"\u865b\u64ec Youtuber\uff08Vtuber\uff09 "},{"bsn":"60030","snA":"563561","title":"\u3010\u554f\u984c\u3011\u9019\u53f0\u4e8c\u624b\u96fb\u8166\u50f9\u4f4dOK\u55ce?","author":"mark134689","wtime":"2020-11-11 12:22:26","pic":"","gp":"0","tnum":143,"comment":137,"ts":1605068546,"btitle":"\u96fb\u8166\u61c9\u7528\u7d9c\u5408\u8a0e\u8ad6 "},{"bsn":"60084","snA":"210700","title":"\u3010\u5fc3\u5f97\u3011\u3010\u5410\u5d3d\u4e00\u5410\u3011\u56de\u982d","author":"ro8230k","wtime":"2020-11-11 11:11:33","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/862081880a0e866f6f119c1b54cfd3e0.JPG","gp":"83","tnum":108,"comment":15,"ts":1605064293,"btitle":"\u6b61\u6a02\u60e1\u641e KUSO"},{"bsn":"60030","snA":"563615","title":"\u3010\u5fc3\u5f97\u3011\u6b61\u559c\u5165\u624bROG3070\u5361\u7687\uff0c\u7d50\u679c\u60b2\u5287\uff01\u6a5f\u6bbc\u5403\u4e0d\u4e0b\uff01MONTECH Air 900 Black \u8f15\u958b\u7bb1\uff08\u5716\u591a\uff09","author":"a5342793","wtime":"2020-11-11 23:35:03","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/0277be590b943166ef377c46747cc90f.JPG","gp":"38","tnum":69,"comment":27,"ts":1605108903,"btitle":"\u96fb\u8166\u61c9\u7528\u7d9c\u5408\u8a0e\u8ad6 "},{"bsn":"60036","snA":"54301","title":"\u3010\u5176\u4ed6\u3011\uff27\uff2b\u6a21\u578b\u4e0a\u8272\u3010Fate\u3011\u963f\u723e\u6258\u8389\u96c5\u30fb\u6f58\u5fb7\u62c9\u8ca2","author":"rbpencil","wtime":"2020-11-11 18:42:19","pic":"https:\/\/i.imgur.com\/r3kn37s.jpg","gp":"54","tnum":55,"comment":0,"ts":1605091339,"btitle":"\u7d9c\u5408\u516c\u4ed4\u73a9\u5177\u8a0e\u8ad6\u5340"},{"bsn":"60599","snA":"31100","title":"\u3010\u5fc3\u5f97\u3011\u8ddf\u8457\u863f\u8389\u5deb\u5973\u4e00\u8d77\u5192\u96aa\u5427! \/ \u5df2\u4e0a\u67b6Steam","author":"d88931122","wtime":"2020-11-11 13:48:49","pic":"https:\/\/i1.ytimg.com\/vi\/4HQ_krla0pY\/hqdefault.jpg","gp":"27","tnum":54,"comment":26,"ts":1605073729,"btitle":"Steam \u7d9c\u5408\u8a0e\u8ad6\u677f"},{"bsn":"60559","snA":"52728","title":"\u3010\u554f\u984c\u3011\u95dc\u65bc\u6700\u8fd1\u5e38\u5e38\u770b\u5230\u7684\u5145\u96fb\u5668\u8ddf\u884c\u52d5\u96fb\u6e90","author":"bvphbcvr","wtime":"2020-11-11 13:40:55","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/d0cf1c5e99f70c97e5c243d98da2430d.JPG","gp":"0","tnum":48,"comment":46,"ts":1605073255,"btitle":"\u667a\u6167\u578b\u624b\u6a5f "},{"bsn":"60497","snA":"1042","title":"\u3010\u8a0e\u8ad6\u3011\u65e5\u672c\u5404\u7a2e\u7f6a\u72af\u88ab\u902e\u6355\u7684\u6a23\u5b50 PART2","author":"copydog","wtime":"2020-11-11 19:52:42","pic":"https:\/\/i.imgur.com\/xTFJmrR.png","gp":"22","tnum":48,"comment":15,"ts":1605095562,"btitle":"\u6d77\u5916\u751f\u6d3b\u53ca\u65c5\u904a\u76f8\u95dc "},{"bsn":"60036","snA":"54304","title":"\u3010\u554f\u984c\u3011\u60f3\u554f\u5927\u5bb6\u9019\u9593\u73a9\u5177\u5e97\u8cb7PVC\u53ef\u4ee5\u55ce?","author":"a2234527","wtime":"2020-11-11 23:51:09","pic":"https:\/\/i.imgur.com\/k4Fs6vG.jpg","gp":"1","tnum":31,"comment":28,"ts":1605109869,"btitle":"\u7d9c\u5408\u516c\u4ed4\u73a9\u5177\u8a0e\u8ad6\u5340"},{"bsn":"60084","snA":"210702","title":"\u3010\u8a0e\u8ad6\u3011\u70ba\u4ec0\u9ebc\u53eb\u9019\u540d\u5b57\uff1f","author":"ash62646","wtime":"2020-11-11 20:17:45","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/3ccceb1c32e84d15650ba2716e97a8c6.JPG","gp":"14","tnum":25,"comment":2,"ts":1605097065,"btitle":"\u6b61\u6a02\u60e1\u641e KUSO"},{"bsn":"60608","snA":"1806","title":"[\u7e6a\u5716]\u30cf\u30c3\u30d4\u30fc\u30b7\u30f3\u30bb\u30b5\u30a4\u30b6","author":"z4031259","wtime":"2020-11-11 20:10:16","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/a77ac46277f02519113ab0f71396ba5c.JPG","gp":"21","tnum":24,"comment":2,"ts":1605096616,"btitle":"\u865b\u64ec Youtuber\uff08Vtuber\uff09 "},{"bsn":"60001","snA":"47673","title":"\u3010\u9592\u804a\u3011\u60e1\u9b54\u7375\u4eba5 \u7279\u5225\u7248 \u96d9\u5e73\u53f0\u6e2c\u8a66","author":"aladar563563","wtime":"2020-11-12 00:12:01","pic":"https:\/\/i1.ytimg.com\/vi\/GIIhJqTERls\/hqdefault.jpg","gp":"2","tnum":22,"comment":18,"ts":1605111121,"btitle":"\u96fb\u8996\u904a\u6a02\u5668\u7d9c\u5408\u8a0e\u8ad6\u5340"},{"bsn":"60559","snA":"52730","title":"\u3010\u8a0e\u8ad6\u3011\u4e00\u4e9b\u63db\u6a5f\u4e0a\u7684\u7591\u616e\u60f3\u8a62\u554f\u5ee3\u5927\u5df4\u53cb\u610f\u898b","author":"WeiGe","wtime":"2020-11-11 15:02:41","pic":"","gp":"0","tnum":21,"comment":14,"ts":1605078161,"btitle":"\u667a\u6167\u578b\u624b\u6a5f "},{"bsn":"60561","snA":"18512","title":"\u3010\u8a0e\u8ad6\u3011\u5927\u5bb6\u5de5\u4f5c\u5982\u679c\u4e0d\u60f3\u5047\u65e5\u52a0\u73ed\u6709\u5565\u597d\u7406\u7531\u5462?","author":"zxcasd2266","wtime":"2020-11-11 21:38:31","pic":"","gp":"2","tnum":20,"comment":16,"ts":1605101911,"btitle":"\u8077\u5834\u7518\u82e6\u8ac7 "},{"bsn":"60143","snA":"44723","title":"\u3010\u5fc3\u5f97\u3011\u6492\u897f\u4e0d\u7406~\u4e45\u9055\u7684\u925b\u7b46\u7d20\u63cf\u7df4\u7fd2","author":"MULDERW","wtime":"2020-11-11 23:59:06","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/a7aefdd6743afea80088da567df564a4.JPG","gp":"17","tnum":19,"comment":1,"ts":1605110346,"btitle":"\u756b\u6280\u4ea4\u6d41\u5340"},{"bsn":"60001","snA":"47674","title":"\u3010\u8a0e\u8ad6\u30113080\u8207\u6700\u65b0\u4e16\u4ee3\u4e3b\u6a5f4K\u5c0d\u6bd4","author":"zrn24886","wtime":"2020-11-12 08:15:20","pic":"https:\/\/i1.ytimg.com\/vi\/9X_Duv7IMfI\/hqdefault.jpg","gp":"3","tnum":18,"comment":14,"ts":1605140120,"btitle":"\u96fb\u8996\u904a\u6a02\u5668\u7d9c\u5408\u8a0e\u8ad6\u5340"},{"bsn":"60545","snA":"18819","title":"\u3010\u554f\u984c\u3011\u60f3\u8cb7 Suzuki Gixxer 250 sf","author":"aerrylum77","wtime":"2020-11-11 18:55:51","pic":"","gp":"0","tnum":17,"comment":14,"ts":1605092151,"btitle":"\u6c7d\u6a5f\u8eca\u8a0e\u8ad6"},{"bsn":"60208","snA":"12703","title":"\u3010\u60c5\u5831\u30112.4\u842c\u5104\u8ecd\u552e\u7372\u6279\uff0c\u5305\u62ec\u5c0d\u963f\u806f\u914b\u7684F-35A\uff0c\u662f\u7f8e\u570b\u6b77\u53f2\u4e0a\u7b2c\u4e8c\u5927\u4ea4\u6613\u3002","author":"s20012797","wtime":"2020-11-11 14:47:29","pic":"","gp":"3","tnum":17,"comment":13,"ts":1605077249,"btitle":"\u8ecd\u4e8b\u7b56\u7565"},{"bsn":"60292","snA":"6779","title":"\u3010\u554f\u984c\u3011java \u8cc7\u6599\u7d50\u69cb","author":"lee38954521","wtime":"2020-11-11 23:35:50","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/366e695b4a62195e9d8eb54760242c9a.JPG","gp":"0","tnum":15,"comment":14,"ts":1605108950,"btitle":"\u7a0b\u5f0f\u8a2d\u8a08\u677f"},{"bsn":"60037","snA":"75591","title":"\u3010\u60c5\u5831\u3011\u300a\u6025\u62305\u79d2\u6b8a\u6b7b\u9b25\u300bTV\u52d5\u756b\u5316\u6c7a\u5b9a\uff01","author":"ready0204","wtime":"2020-11-11 23:00:32","pic":"https:\/\/i.imgur.com\/EGXG7kk.jpg","gp":"6","tnum":14,"comment":0,"ts":1605106832,"btitle":"\u52d5\u6f2b\u76f8\u95dc\u7d9c\u5408"}],"ac":[{"bsn":"47521","snA":"621","title":"\u3010\u60c5\u5831\u3011\u6f2b\u756b\u300a\u51fa\u79df\u5973\u53cb\u300b164 \u8a71\u300c\u5f7c\u5973\u3068\u5f7c\u6c0f\u2467\u300d\u5287\u900f \u300a\u91cd\u96f7\u614e\u5165\u300b","author":"a75257525","wtime":"2020-11-11 10:42:05","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/a6ebd3cd7249d47cd832515eae8cdbbd.JPG","gp":"121","tnum":220,"comment":84,"ts":1605062525,"btitle":"\u5bae\u5cf6\u79ae\u540f \u4f5c\u54c1\u96c6\uff08\u51fa\u79df\u5973\u53cb\uff09"},{"bsn":"47157","snA":"367","title":"\u3010\u60c5\u5831\u3011\u300a\u661f\u671f\u4e00\u7684\u8c50\u6eff\u300b\u6f2b\u756b\u7b2c\u4e00\u8a71\u300c\u5c0f\u611b \u2460\u300d\u5f69\u9801&amp;\u5167\u5bb9\u9810\u89bd","author":"kanonhg","wtime":"2020-11-12 00:21:07","pic":"https:\/\/pbs.twimg.com\/media\/EmjjlR7UYAEE2BH.jpg","gp":"142","tnum":155,"comment":12,"ts":1605111667,"btitle":"\u661f\u671f\u4e00\u7684\u8c50\u6eff"},{"bsn":"41226","snA":"10979","title":"\u3010\u5206\u4eab\u3011\u9b5a \u6240\u756b\u7684\u300aK-ON\uff01\u300b11\/11 \u4e2d\u91ce\u6893 \u751f\u65e5\u8cc0\u5716","author":"kanonhg","wtime":"2020-11-11 17:44:55","pic":"https:\/\/pbs.twimg.com\/media\/EmfW3sAVcAE360q.jpg","gp":"138","tnum":150,"comment":10,"ts":1605087895,"btitle":"K-ON!"},{"bsn":"45581","snA":"987","title":"\u3010\u60c5\u5831\u301185\u8a71\uff0c\u80fd\u6d3b\u8457\u4e0b\u53bb\u7684\u53ea\u6709\u4e00\u4eba","author":"jug986532","wtime":"2020-11-11 23:28:57","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/a2d6022a44236089b9056f0c86c50d0a.JPG","gp":"18","tnum":56,"comment":15,"ts":1605108537,"btitle":"\u62f3\u9858\u963f\u4fee\u7f85"},{"bsn":"48428","snA":"53","title":"\u3010\u5206\u4eab\u3011\u3074\u3063\u3064\u3041 @Pizzaniacompany \u6240\u756b\u7684\u300a\u7121\u80fd\u529b\u8005\u5a1c\u5a1c\u300b\u67ca\u5a1c\u5a1c","author":"saliyaaa","wtime":"2020-11-11 12:55:56","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/f5fa1b42b3e3eb3fecb82bd261f3f6fd.JPG","gp":"36","tnum":42,"comment":5,"ts":1605070556,"btitle":"\u7121\u80fd\u529b\u8005\u5a1c\u5a1c "},{"bsn":"109","snA":"58737","title":"\u3010\u60c5\u5831\u3011\u92fc\u666e\u62c940\u9031\u5e74 \u516c\u958b4\/12 Xi ver \u5287\u5834\u9583\u54c8","author":"a128098527","wtime":"2020-11-11 13:04:41","pic":"https:\/\/upload.cc\/i1\/2020\/11\/12\/C1dtyw.jpeg","gp":"11","tnum":40,"comment":18,"ts":1605071081,"btitle":"\u92fc\u5f48"},{"bsn":"47551","snA":"1642","title":"\u3010\u5fc3\u5f97\u3011\u958b\u7bb1\u6587 \u4e94\u7b49\u5206\u7684\u65b0\u5a18\u222c \u4e8c\u4e43 \u6795\u982d\u5957","author":"x810058","wtime":"2020-11-11 11:53:49","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/5f6edcc1a2105f25408ab4e51cae5a7a.JPG","gp":"19","tnum":34,"comment":14,"ts":1605066829,"btitle":"\u4e94\u7b49\u5206\u7684\u65b0\u5a18 "},{"bsn":"46884","snA":"934","title":"\u3010\u60c5\u5831\u3011\u30b2\u30c3\u30b5\u30f3 12 \u6708\u865f\u5c01\u9762\u662f\u300a\u64c5\u9577\u6349\u5f04\u4eba\u7684\u9ad8\u6728\u540c\u5b78\u300b\u5915\u967d\u4e0b\u7684\u9ad8\u6728&amp;\u897f\u7247","author":"kanonhg","wtime":"2020-11-11 22:03:31","pic":"https:\/\/pbs.twimg.com\/media\/EmjDusoVoAYv9y2.jpg","gp":"33","tnum":34,"comment":0,"ts":1605103411,"btitle":"\u5c71\u672c\u5d07\u4e00\u6717 \u4f5c\u54c1\u96c6\uff08\u64c5\u9577\u6349\u5f04\u4eba\u7684\u9ad8\u6728\u540c\u5b78\uff09"},{"bsn":"48427","snA":"115","title":"\u3010\u554f\u984c\u3011\u93c8\u92f8\u4eba\u5982\u679c\u52d5\u756b\u5316\uff0c\u54ea\u9593\u516c\u53f8\u505a\u6703\u6700\u597d?","author":"ipadxbox360","wtime":"2020-11-11 11:36:31","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/e41721cebadb08f5181708b2009aeadc.JPG","gp":"8","tnum":32,"comment":23,"ts":1605065791,"btitle":"\u93c8\u92f8\u4eba"},{"bsn":"46004","snA":"191","title":"\u3010\u60c5\u5831\u3011\u7121\u8077\u8f49\u751f\uff5e\u5230\u4e86\u7570\u4e16\u754c\u5c31\u62ff\u51fa\u771f\u672c\u4e8b\uff5e \u6d1b\u742a\u5e0c \u6a21\u578b \u539f\u578b\u5716\u8a0a","author":"seino999","wtime":"2020-11-11 16:46:27","pic":"https:\/\/pbs.twimg.com\/media\/EmhH866WEAAxD5R.jpg","gp":"12","tnum":30,"comment":15,"ts":1605084387,"btitle":"\u7121\u8077\u8ee2\u751f"},{"bsn":"40793","snA":"51","title":"\u3010\u60c5\u5831\u3011\u52d5\u756b\u300a\u571f\u4e0b\u5ea7\u8dea\u6c42\u7d66\u770b\u300b\u7b2c5\u8a71\u7247\u5c3e\u63d2\u5716\u300c\u306f\u308a\u3082\u3058\u300d","author":"n1028459","wtime":"2020-11-11 23:42:46","pic":"https:\/\/upload.cc\/i1\/2020\/11\/11\/o8lMXC.jpg","gp":"29","tnum":30,"comment":0,"ts":1605109366,"btitle":"\u8239\u6d25\u4e00\u8f1d\u4f5c\u54c1\u96c6\uff08\u5996\u602a\u5c11\u5973\uff09"},{"bsn":"46988","snA":"3702","title":"\u3010\u60c5\u5831\u3011\u300a\u9b3c\u6ec5\u4e4b\u5203\u300b\u7b2c23\u96c6\u5c01\u9762\u516c\u958b\uff08\u65e5\u7248","author":"b09818172170","wtime":"2020-11-11 21:02:40","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/1690dbb2cc48ee94bff886ee4032c27c.JPG","gp":"23","tnum":29,"comment":4,"ts":1605099760,"btitle":"\u9b3c\u6ec5\u4e4b\u5203"},{"bsn":"46829","snA":"166","title":"\u3010\u5206\u4eab\u3011TR\u6240\u756b\u7684\u300a\u9ec3\u91d1\u795e\u5a01\u300b\u5973\u50d5\u88dd\u6708\u5cf6\u57fa","author":"pegasusking","wtime":"2020-11-12 06:06:12","pic":"https:\/\/i.imgur.com\/Dgdwk5D.jpg","gp":"23","tnum":27,"comment":3,"ts":1605132372,"btitle":"\u9ec3\u91d1\u795e\u5a01"},{"bsn":"46197","snA":"1686","title":"\u3010\u9592\u804a\u3011\u7531\u300a\u8f1d\u591c\u59ec\u300b\u7684\u300c\u60f3\u8981\u6478\u300d\u4f86\u4ecb\u7d39\u300c\u4e03\u60c5\u4e94\u5fd7\u300d(\u4e0b)","author":"shalott988","wtime":"2020-11-11 21:18:34","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/66520ade02b8e4335d8b505184576e2d.JPG","gp":"21","tnum":26,"comment":4,"ts":1605100714,"btitle":"\u8d64\u5742\u30a2\u30ab \u4f5c\u54c1\u96c6\uff08\u8f1d\u591c\u59ec\u60f3\u8b93\u4eba\u544a\u767d\uff09 "},{"bsn":"9009","snA":"17374","title":"\u3010\u60c5\u5831\u3011ANIPLEX+\u300aFate\/Grand Order\u300bForeigner\/\u845b\u98fe\u5317\u9f4b\u3001\u963f\u6bd4\u84cb\u723e\uff0e\u5a01\u5ec9\u65af \u82f1\u9748\u796d\u88ddver. PVC \u88fd\u4f5c\u78ba\u5b9a","author":"kanonhg","wtime":"2020-11-11 18:40:29","pic":"https:\/\/pbs.twimg.com\/media\/EmiU5cBU0AAY_dQ.jpg","gp":"17","tnum":26,"comment":8,"ts":1605091229,"btitle":"TYPE-MOON \u7cfb\u5217"},{"bsn":"45654","snA":"177","title":"\u3010\u60c5\u5831\u3011\u30d5\u30a3\u30fc\u30eb\uff08feel.\uff09\u7684\u300a\u6eff\u6ea2\u7684\u6c34\u679c\u5854\u300b\u95dc\u91ce\u77e5\u5b50\u7684\u8a95\u751f\u65e5\u795d\u8cc0\u5716","author":"n1028459","wtime":"2020-11-11 18:55:11","pic":"https:\/\/pbs.twimg.com\/media\/EmiDWyaUcAAJVc6.jpg","gp":"23","tnum":24,"comment":0,"ts":1605092111,"btitle":"\u6ff1\u5f13\u5834\u96d9 \u4f5c\u54c1\u96c6 "},{"bsn":"45499","snA":"1361","title":"\u3010\u60c5\u5831\u3011\u30b7\u30fc\u30ba\u30ca\u30eb\u30d7\u30e9\u30f3\u30c4\u300a\u5728\u5730\u4e0b\u57ce\u5c0b\u6c42\u9082\u9005\u662f\u5426\u641e\u932f\u4e86\u4ec0\u9ebc \u7b2c\u4e09\u5b63\u300bB2 \u639b\u8ef8\u3001\u6ed1\u9f20\u588a","author":"kanonhg","wtime":"2020-11-11 21:37:24","pic":"https:\/\/img.amiami.jp\/images\/product\/main\/204\/GOODS-04073162.jpg","gp":"21","tnum":23,"comment":1,"ts":1605101844,"btitle":"\u5728\u5730\u4e0b\u57ce\u5c0b\u6c42\u9082\u9005\u662f\u5426\u641e\u932f\u4e86\u4ec0\u9ebc"},{"bsn":"47340","snA":"127","title":"\u3010\u60c5\u5831\u3011\u5b98\u65b9\u737b\u4e0a\u7684\u300a\u5728\u9b54\u738b\u57ce\u8aaa\u665a\u5b89\u300b11\u670811\u65e5\u60e1\u9b54\u4fee\u9053\u58eb\u7684\u751f\u65e5\u795d\u8cc0\u5716\uff01","author":"pegasusking","wtime":"2020-11-11 19:17:50","pic":"https:\/\/i.imgur.com\/5v4Zw78.jpg","gp":"14","tnum":16,"comment":1,"ts":1605093470,"btitle":"\u5728\u9b54\u738b\u57ce\u8aaa\u665a\u5b89"},{"bsn":"48295","snA":"266","title":"\u3010\u5176\u4ed6\u3011\u3010\u6b4c\u8a5e\u7ffb\u8b6f\u3011Moonlight\/\u7acb\u5ddd\u7d62\u9999","author":"yusinpeng","wtime":"2020-11-11 21:27:00","pic":"https:\/\/i1.ytimg.com\/vi\/qFEjzUXdInQ\/hqdefault.jpg","gp":"11","tnum":15,"comment":3,"ts":1605101220,"btitle":"22\/7"},{"bsn":"46542","snA":"4735","title":"\u3010\u60c5\u5831\u3011COSPA\u300aRe\uff1a\u5f9e\u96f6\u958b\u59cb\u7684\u7570\u4e16\u754c\u751f\u6d3b\u300b\u96f7\u59c6 \u4f4f\u5bbf\u5957\u7d44\uff0c\u958b\u653e\u9810\u8cfc\u4e2d\uff01","author":"kanonhg","wtime":"2020-11-11 20:43:53","pic":"https:\/\/img.amiami.jp\/images\/product\/main\/203\/GOODS-00401022.jpg","gp":"14","tnum":15,"comment":0,"ts":1605098633,"btitle":"Re\uff1a\u5f9e\u96f6\u958b\u59cb\u7684\u7570\u4e16\u754c\u751f\u6d3b"}],"game":[{"bsn":"29461","snA":"16460","title":"\u3010\u63d2\u756b\u3011\u6b63\u592a\u76ae\u5361\u557e","author":"jye830518","wtime":"2020-11-11 12:10:17","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/31049b7651224623b3ec98927d655c7f.JPG","gp":"128","tnum":237,"comment":104,"ts":1605067817,"btitle":"\u6953\u4e4b\u8c37 M"},{"bsn":"37030","snA":"4464","title":"\u3010\u9592\u804a\u3011\u6839\u672c\u5c31\u6c92\u6709\u4ec0\u9ebc60\u9000\u5751\u6f6e","author":"plokplok2","wtime":"2020-11-11 17:47:42","pic":"","gp":"126","tnum":229,"comment":74,"ts":1605088062,"btitle":"RO \u4ed9\u5883\u50b3\u8aaa\uff1a\u65b0\u4e16\u4ee3\u7684\u8a95\u751f"},{"bsn":"21911","snA":"6237","title":"\u3010\u9592\u804a\u3011\u6709\u4eba\u8981\u4e00\u8d77\u5206\u4eab\u65b0\u89d2\u51fa\u4e4b\u524d\u7684\u4ea4\u6613\u6240\u4e82\u8c61\u55ce","author":"willie990316","wtime":"2020-11-11 12:12:12","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/5eba02a227558eca53defee1a1cc4b01.JPG","gp":"38","tnum":199,"comment":149,"ts":1605067932,"btitle":"\u9748\u9b42\u884c\u8005"},{"bsn":"36730","snA":"6378","title":"\u3010\u60c5\u5831\u3011\u706b\u7cfb\u53f2\u8a69\u7d1a\u524a\u5f31","author":"fdff7474","wtime":"2020-11-11 11:22:26","pic":"","gp":"46","tnum":176,"comment":106,"ts":1605064946,"btitle":"\u539f\u795e"},{"bsn":"26742","snA":"69828","title":"\u3010\u7e6a\u5716\u3011 \u5973\u6885\u6797\u2606","author":"kyaroru","wtime":"2020-11-11 18:26:18","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/2d0fd412b11d7e81e768667db5fdda62.JPG","gp":"156","tnum":164,"comment":7,"ts":1605090378,"btitle":"Fate\/Grand Order"},{"bsn":"32550","snA":"10976","title":"\u3010\u7e6a\u5716\u3011\u7f85\u6069\u03bc","author":"Jason99827","wtime":"2020-11-11 12:19:56","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/1cda45fb8292d1d71dbaba3df0a358c5.JPG","gp":"141","tnum":153,"comment":11,"ts":1605068396,"btitle":"\u78a7\u85cd\u822a\u7dda"},{"bsn":"30861","snA":"27567","title":"\u3010\u8a0e\u8ad6\u3011\u53f0\u7248\u7af6\u6280\u5834\u91cd\u7f6e \u6253NPC\u968a\u4f0d\u63a8\u85a6","author":"nielnieh345","wtime":"2020-11-11 11:46:57","pic":"https:\/\/i1.ytimg.com\/vi\/6bEFJcev1qo\/hqdefault.jpg","gp":"130","tnum":147,"comment":16,"ts":1605066417,"btitle":"\u8d85\u7570\u57df\u516c\u4e3b\u9023\u7d50\u2606Re:Dive "},{"bsn":"38113","snA":"7027","title":"\u3010\u60c5\u5831\u3011\u570b\u969b\u670dfb-\u586b\u554f\u5377\u9001600\u947d\u77f3","author":"Thkuo0924","wtime":"2020-11-11 20:01:54","pic":"","gp":"70","tnum":141,"comment":48,"ts":1605096114,"btitle":"\u5b88\u671b\u50b3\u8aaa Guardian Tales "},{"bsn":"39113","snA":"249","title":"\u3010\u554f\u984c\u3011\u9000\u4e86\u516c\u6703\u4e4b\u5f8c\u7a81\u7136\u9593\u9084\u6c92\u9032\u65b0\u516c\u6703\u5c31\u88ab\u8e22\u4e86 \u771f\u7121\u5948","author":"leetim1997","wtime":"2020-11-11 18:35:20","pic":"https:\/\/cdn.discordapp.com\/attachments\/282323480354095104\/776028302355726336\/unknown.png","gp":"3","tnum":134,"comment":129,"ts":1605090920,"btitle":"\u91ce\u751f\u5c11\u5973"},{"bsn":"36833","snA":"16356","title":"\u3010\u60c5\u5831\u3011\u5148\u92d2\u670d 1.51\u7248\u672c\u66f4\u65b0\u516c\u544a","author":"diablo80364","wtime":"2020-11-11 18:23:24","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/6c3893fb12d994a61e5d3f710d70ac91.JPG","gp":"76","tnum":124,"comment":47,"ts":1605090204,"btitle":"\u528d\u8207\u9060\u5f81"},{"bsn":"23805","snA":"647419","title":"\u3010\u60c5\u5831\u3011\u5b98\u65b9\u63a8\u51fa\u300c\u52a0\u8afe\u5967\u65af\u7d00\u5ff5\u982d\u50cf\u6846\u300d","author":"j0800449","wtime":"2020-11-11 17:14:55","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/e826c168042ab9c236c91e9d9661ba6a.JPG","gp":"60","tnum":124,"comment":50,"ts":1605086095,"btitle":"\u795e\u9b54\u4e4b\u5854"},{"bsn":"26686","snA":"55010","title":"\u3010\u60c5\u5831\u3011SERIOUS BREAKER PV","author":"lineage52","wtime":"2020-11-11 14:47:28","pic":"https:\/\/i1.ytimg.com\/vi\/utJkkYAJv-A\/hqdefault.jpg","gp":"25","tnum":118,"comment":89,"ts":1605077248,"btitle":"\u767d\u8c93 Project"},{"bsn":"37459","snA":"623","title":"\u3010\u60c5\u5831\u3011\u4eca\u5929\u662f\u6211\u7b2c\u4e00\u6b21\u5728\u6fc0\u9b25\u5cfd\u8c37\u653e\u63a8","author":"enil115","wtime":"2020-11-11 18:15:21","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/1e8f879ab13527ec17176dbb76f56740.JPG","gp":"84","tnum":114,"comment":20,"ts":1605089721,"btitle":"\u82f1\u96c4\u806f\u76df\uff1a\u6fc0\u9b25\u5cfd\u8c37"},{"bsn":"33651","snA":"4162","title":"\u3010\u9592\u804a\u3011\u53f0\u670d\u6d3b\u52d5\u6392\u7a0b\u9810\u6e2c\/\u672a\u4f86\u8996 \u7c21\u6613\u6574\u7406_11\/11\u66f4\u65b0","author":"david199444","wtime":"2020-11-11 15:57:38","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/b6f4122aa5e13b2a0956f085d4ae1743.PNG","gp":"80","tnum":110,"comment":28,"ts":1605081458,"btitle":"\u660e\u65e5\u65b9\u821f "},{"bsn":"60596","snA":"51690","title":"\u3010\u60c5\u5831\u3011CDPR :\u96fb\u99ad\u53db\u5ba22077 \u4e9e\u6d32\u7248\u4e0d\u6703\u548c\u8ae7","author":"jobdark","wtime":"2020-11-11 17:19:23","pic":"https:\/\/i.imgur.com\/5wXfIq1.jpg","gp":"65","tnum":106,"comment":34,"ts":1605086363,"btitle":"PS4 \/ PlayStation4 "},{"bsn":"25052","snA":"83519","title":"\u3010\u60c5\u5831\u3011\u300a\u602a\u7269\u5f48\u73e0\u300b\u8207\u300a\u4e03\u5927\u7f6a \u61a4\u6012\u7684\u5be9\u5224\u300b\u7684\u7b2c2\u5f48\u5408\u4f5c\u6d3b\u52d5\u65bc11\u670814\u65e5\uff08\u516d\uff09\u4e2d\u5348\u8d77\u958b\u8dd1\uff01","author":"sm70987","wtime":"2020-11-11 16:36:17","pic":"https:\/\/www.monster-strike.com.tw\/entryimages\/5V_%25BnfjJerE%23e_TYeburjhV_CWyE_C3vwbr5.png","gp":"66","tnum":104,"comment":31,"ts":1605083777,"btitle":"\u602a\u7269\u5f48\u73e0"},{"bsn":"5786","snA":"158598","title":"\u3010\u5bc6\u6280\u3011\u51b0\u539f\u8c46\u77e5\u8b58\uff01\uff08\u6b61\u8fce\u5927\u5bb6\u5e6b\u5fd9\u88dc\u5b8c\uff01\uff09","author":"jackhammer93","wtime":"2020-11-11 23:38:28","pic":"","gp":"74","tnum":102,"comment":19,"ts":1605109108,"btitle":"\u9b54\u7269\u7375\u4eba"},{"bsn":"35284","snA":"2348","title":"\u3010\u60c5\u5831\u30111111\u76ee\u524d\u6d3b\u52d5\u60c5\u5831\u5c0f\u6574\u7406","author":"king88s88","wtime":"2020-11-11 11:01:35","pic":"","gp":"72","tnum":91,"comment":17,"ts":1605063695,"btitle":"\u5fa9\u6d3b\u90aa\u795e Re ; universe"},{"bsn":"34173","snA":"10657","title":"\u3010\u5fc3\u5f97\u3011\u8ab0\u4e5f\u64cb\u4e0d\u4f4f\u8fa3\u500b\u81ea\u7531\u7684\u5973\u4eba x \u95c7\u54a2\u8d85\u7d1a x \u81ea\u7531\u7684\u5973\u4eba\uff10\uff1a\uff13\uff18\u8d85\u9ad8\u901f\u5468\u56de","author":"k810728","wtime":"2020-11-11 15:28:03","pic":"https:\/\/i1.ytimg.com\/vi\/uYNoE2IVjdQ\/hqdefault.jpg","gp":"43","tnum":86,"comment":41,"ts":1605079683,"btitle":"Dragalia Lost \uff5e\u5931\u843d\u7684\u9f8d\u7d46\uff5e"},{"bsn":"60597","snA":"12166","title":"\u3010\u9592\u804a\u3011\u5df4\u54c8\u8fa6\u7684XSS\u62bd\u734e","author":"twps3","wtime":"2020-11-11 14:23:40","pic":"","gp":"2","tnum":82,"comment":7,"ts":1605075820,"btitle":"Xbox One"}],"game_tv":[{"bsn":"60596","snA":"51690","title":"\u3010\u60c5\u5831\u3011CDPR :\u96fb\u99ad\u53db\u5ba22077 \u4e9e\u6d32\u7248\u4e0d\u6703\u548c\u8ae7","author":"jobdark","wtime":"2020-11-11 17:19:23","pic":"https:\/\/i.imgur.com\/5wXfIq1.jpg","gp":"65","tnum":106,"comment":34,"ts":1605086363,"btitle":"PS4 \/ PlayStation4 "},{"bsn":"5786","snA":"158598","title":"\u3010\u5bc6\u6280\u3011\u51b0\u539f\u8c46\u77e5\u8b58\uff01\uff08\u6b61\u8fce\u5927\u5bb6\u5e6b\u5fd9\u88dc\u5b8c\uff01\uff09","author":"jackhammer93","wtime":"2020-11-11 23:38:28","pic":"","gp":"74","tnum":102,"comment":19,"ts":1605109108,"btitle":"\u9b54\u7269\u7375\u4eba"},{"bsn":"60597","snA":"12166","title":"\u3010\u9592\u804a\u3011\u5df4\u54c8\u8fa6\u7684XSS\u62bd\u734e","author":"twps3","wtime":"2020-11-11 14:23:40","pic":"","gp":"2","tnum":82,"comment":7,"ts":1605075820,"btitle":"Xbox One"},{"bsn":"60645","snA":"500","title":"\u3010\u60c5\u5831\u301111\u670811\u865f  PlayStation 5 \u7dda\u4e0a\u7279\u5225\u7bc0\u76ee","author":"esz6988","wtime":"2020-11-11 22:40:44","pic":"https:\/\/i1.ytimg.com\/vi\/a_O8EMcCTY4\/hqdefault.jpg","gp":"0","tnum":73,"comment":68,"ts":1605105644,"btitle":"PS5 \/ PlayStation5 "},{"bsn":"60646","snA":"177","title":"\u5373\u65e5\u8d77\u53c3\u8207 Xbox \u904a\u6232\u9054\u4eba\u554f\u7b54\u5927\u8cfd \u5c31\u6709\u6a5f\u6703\u8d0f\u5f97\u6b21\u4e16\u4ee3\u4e3b\u6a5f\u7b49\u8c6a\u83ef\u734e\u54c1\uff01","author":"fuligirl","wtime":"2020-11-11 12:00:01","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/f88eea6071eef032667a3aca2dfd020c.JPG","gp":"0","tnum":69,"comment":18,"ts":1605067201,"btitle":"Xbox  \/ Xbox Series X "},{"bsn":"30532","snA":"1214","title":"\u3010\u5fc3\u5f97\u3011\u62ef\u6551\u6211\u7684\u904a\u6232\u51b7\u611f\uff0c\u6211\u7d42\u65bc\u61c2\u9a62\u4eba\u7684\u5fc3\u4e86 \u5c0f\u5cf6\u6211\u611b\u4f60","author":"JintrepiT","wtime":"2020-11-11 13:31:03","pic":"","gp":"29","tnum":50,"comment":18,"ts":1605072663,"btitle":"\u6b7b\u4ea1\u64f1\u6dfa Death Stranding "},{"bsn":"9703","snA":"31408","title":"\u3010\u60c5\u5831\u3011\u5206\u4eab\uff1a\u958b\u73a9\u524d\u671f\u53ef\u514c\u63db\u7684\u8d08\u9001\u6b66\u5668","author":"stillnick","wtime":"2020-11-11 13:50:40","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/a76c6796a5186dadd563a1a383a44db6.JPG","gp":"12","tnum":45,"comment":25,"ts":1605073840,"btitle":"\u523a\u5ba2\u6559\u689d"},{"bsn":"31587","snA":"19667","title":"\u3010\u60c5\u5831\u3011\u4e0d\u6182\u5fc3\u6b21\u4e16\u4ee3\u5230\u4f86\uff0c\u7a2e\u985e\u7e41\u591a\u7684\u7b2c\u4e09\u65b9\u904a\u6232\u5c07\u767b\u9678 Switch","author":"jacket6719","wtime":"2020-11-11 16:49:37","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/4f5ff77e1313cc23e141e213bd59ede0.JPG","gp":"8","tnum":35,"comment":26,"ts":1605084577,"btitle":"NS \/ Nintendo Switch"},{"bsn":"8448","snA":"8561","title":"\u3010\u60c5\u5831\u3011\u95dc\u65bc\u935b\u9020\u8f2a\u8ff4 SL\u7121\u7528QQ","author":"z033136344","wtime":"2020-11-12 08:59:12","pic":"","gp":"1","tnum":25,"comment":23,"ts":1605142752,"btitle":"\u4ec1\u738b"},{"bsn":"7287","snA":"13533","title":"\u3010\u5fc3\u5f97\u3011\u6539\u5cf6\u653b\u7565\uff1a\u6a5f\u5834\u5165\u53e3\u8a2d\u8a08\uff5c\u96e2\u670d\u52d9\u8655\u592a\u8fd1\u7684\u89e3\u6c7a\u65b9\u5f0f","author":"v35202","wtime":"2020-11-11 21:26:36","pic":"https:\/\/i1.ytimg.com\/vi\/cFNptXXOY9U\/hqdefault.jpg","gp":"18","tnum":21,"comment":1,"ts":1605101196,"btitle":"\u52d5\u7269\u4e4b\u68ee \u7cfb\u5217 \uff08\u52d5\u7269\u68ee\u53cb\u6703\uff09 "}],"game_pc":[{"bsn":"21911","snA":"6237","title":"\u3010\u9592\u804a\u3011\u6709\u4eba\u8981\u4e00\u8d77\u5206\u4eab\u65b0\u89d2\u51fa\u4e4b\u524d\u7684\u4ea4\u6613\u6240\u4e82\u8c61\u55ce","author":"willie990316","wtime":"2020-11-11 12:12:12","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/5eba02a227558eca53defee1a1cc4b01.JPG","gp":"38","tnum":199,"comment":149,"ts":1605067932,"btitle":"\u9748\u9b42\u884c\u8005"},{"bsn":"4212","snA":"427821","title":"\u3010\u554f\u984c\u3011\u65ac\u9996120~150\u6cbb\u88dd","author":"aa205011","wtime":"2020-11-11 16:44:09","pic":"","gp":"0","tnum":70,"comment":68,"ts":1605084249,"btitle":"RO \u4ed9\u5883\u50b3\u8aaa Online"},{"bsn":"13211","snA":"163272","title":"\u3010\u5176\u4ed6\u30112020\u5468\u5e74\u796d-\u6a58\u661f\u82b1\u706b \u9080\u8acb\u78bc\u5206\u4eab\/\u5c0b\u627e\u5340","author":"adriancheung","wtime":"2020-11-11 20:08:48","pic":"","gp":"3","tnum":67,"comment":26,"ts":1605096528,"btitle":"\u65b0\u9f8d\u4e4b\u8c37 Dragon Nest"},{"bsn":"4236","snA":"14257","title":"\u3010\u554f\u984c\u3011\u95dc\u65bc\u820a\u4e16\u754c\u7684\u5438\u8840\u9b3c","author":"duranliu","wtime":"2020-11-11 11:04:01","pic":"","gp":"0","tnum":66,"comment":65,"ts":1605063841,"btitle":"\u5e55\u5e9c\u5c07\u8ecd\u3001\u5168\u8ecd\u7834\u6575\u7cfb\u5217"},{"bsn":"17532","snA":"668516","title":"\u3010\u5fc3\u5f97\u3011S11\u88dd\u5099\u6539\u52d5\uff0c\u8f14\u52a9\u5148\u884c\u63a2\u8def\u3002","author":"seventeemo","wtime":"2020-11-11 23:00:25","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/af93cabc6f59b5c835e5c771b33793fe.JPG","gp":"37","tnum":61,"comment":19,"ts":1605106825,"btitle":"\u82f1\u96c4\u806f\u76df League of Legends "},{"bsn":"7650","snA":"1011592","title":"\u3010\u60c5\u5831\u3011MESA v199 AWAKE (\u6230\u570bV4)","author":"g555555g","wtime":"2020-11-11 14:22:30","pic":"https:\/\/i.imgur.com\/dZYUhy4.png","gp":"19","tnum":57,"comment":34,"ts":1605075750,"btitle":"\u65b0\u6953\u4e4b\u8c37 "},{"bsn":"12980","snA":"77144","title":"\u3010\u5176\u4ed6\u3011\u95dc\u65bc\u6539\u7248\u5f8c\u8840\u91cf\u63d0\u6607","author":"norm0010","wtime":"2020-11-12 01:17:12","pic":"https:\/\/cdn.discordapp.com\/attachments\/336870819013722114\/776131983599730728\/unknown.png","gp":"5","tnum":53,"comment":46,"ts":1605115032,"btitle":"\u528d\u9748 Blade &amp; Soul"},{"bsn":"29919","snA":"7902","title":"\u3010\u5fc3\u5f97\u3011140\u5c0f\u6642-\u5f9e\u7d14\u7d14\u65b0\u624b\u9b3c\u98db\u5347\u5230R1\u6bba\u624b\u7684\u5206\u4eab","author":"a33886610","wtime":"2020-11-12 03:07:18","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/f33f6da6163b0453f873887a45b8b105.PNG","gp":"20","tnum":49,"comment":22,"ts":1605121638,"btitle":"Dead by Daylight\uff08\u9ece\u660e\u6b7b\u7dda\uff09"},{"bsn":"1223","snA":"23840","title":"\u3010\u5fc3\u5f97\u301123\u5c0f\u6642\u7834\u95dc\uff0c\u8001\u73a9\u5bb6\u5c0f\u5fc3\u5f97","author":"bbc111","wtime":"2020-11-11 10:44:50","pic":"","gp":"14","tnum":49,"comment":32,"ts":1605062690,"btitle":"\u8ed2\u8f45\u528d \u55ae\u6a5f\u7cfb\u5217"},{"bsn":"21052","snA":"17469","title":"\u3010\u5176\u4ed6\u3011\u734e\u52f5\u4ee3\u78bc","author":"andy799181","wtime":"2020-11-11 11:23:30","pic":"","gp":"31","tnum":46,"comment":14,"ts":1605065010,"btitle":"\u6230\u8266\u4e16\u754c World of Warships"}],"game_mobile":[{"bsn":"29461","snA":"16460","title":"\u3010\u63d2\u756b\u3011\u6b63\u592a\u76ae\u5361\u557e","author":"jye830518","wtime":"2020-11-11 12:10:17","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/31049b7651224623b3ec98927d655c7f.JPG","gp":"128","tnum":237,"comment":104,"ts":1605067817,"btitle":"\u6953\u4e4b\u8c37 M"},{"bsn":"37030","snA":"4464","title":"\u3010\u9592\u804a\u3011\u6839\u672c\u5c31\u6c92\u6709\u4ec0\u9ebc60\u9000\u5751\u6f6e","author":"plokplok2","wtime":"2020-11-11 17:47:42","pic":"","gp":"126","tnum":229,"comment":74,"ts":1605088062,"btitle":"RO \u4ed9\u5883\u50b3\u8aaa\uff1a\u65b0\u4e16\u4ee3\u7684\u8a95\u751f"},{"bsn":"36730","snA":"6378","title":"\u3010\u60c5\u5831\u3011\u706b\u7cfb\u53f2\u8a69\u7d1a\u524a\u5f31","author":"fdff7474","wtime":"2020-11-11 11:22:26","pic":"","gp":"46","tnum":176,"comment":106,"ts":1605064946,"btitle":"\u539f\u795e"},{"bsn":"26742","snA":"69828","title":"\u3010\u7e6a\u5716\u3011 \u5973\u6885\u6797\u2606","author":"kyaroru","wtime":"2020-11-11 18:26:18","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/2d0fd412b11d7e81e768667db5fdda62.JPG","gp":"156","tnum":164,"comment":7,"ts":1605090378,"btitle":"Fate\/Grand Order"},{"bsn":"32550","snA":"10976","title":"\u3010\u7e6a\u5716\u3011\u7f85\u6069\u03bc","author":"Jason99827","wtime":"2020-11-11 12:19:56","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/1cda45fb8292d1d71dbaba3df0a358c5.JPG","gp":"141","tnum":153,"comment":11,"ts":1605068396,"btitle":"\u78a7\u85cd\u822a\u7dda"},{"bsn":"30861","snA":"27567","title":"\u3010\u8a0e\u8ad6\u3011\u53f0\u7248\u7af6\u6280\u5834\u91cd\u7f6e \u6253NPC\u968a\u4f0d\u63a8\u85a6","author":"nielnieh345","wtime":"2020-11-11 11:46:57","pic":"https:\/\/i1.ytimg.com\/vi\/6bEFJcev1qo\/hqdefault.jpg","gp":"130","tnum":147,"comment":16,"ts":1605066417,"btitle":"\u8d85\u7570\u57df\u516c\u4e3b\u9023\u7d50\u2606Re:Dive "},{"bsn":"38113","snA":"7027","title":"\u3010\u60c5\u5831\u3011\u570b\u969b\u670dfb-\u586b\u554f\u5377\u9001600\u947d\u77f3","author":"Thkuo0924","wtime":"2020-11-11 20:01:54","pic":"","gp":"70","tnum":141,"comment":48,"ts":1605096114,"btitle":"\u5b88\u671b\u50b3\u8aaa Guardian Tales "},{"bsn":"39113","snA":"249","title":"\u3010\u554f\u984c\u3011\u9000\u4e86\u516c\u6703\u4e4b\u5f8c\u7a81\u7136\u9593\u9084\u6c92\u9032\u65b0\u516c\u6703\u5c31\u88ab\u8e22\u4e86 \u771f\u7121\u5948","author":"leetim1997","wtime":"2020-11-11 18:35:20","pic":"https:\/\/cdn.discordapp.com\/attachments\/282323480354095104\/776028302355726336\/unknown.png","gp":"3","tnum":134,"comment":129,"ts":1605090920,"btitle":"\u91ce\u751f\u5c11\u5973"},{"bsn":"36833","snA":"16356","title":"\u3010\u60c5\u5831\u3011\u5148\u92d2\u670d 1.51\u7248\u672c\u66f4\u65b0\u516c\u544a","author":"diablo80364","wtime":"2020-11-11 18:23:24","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/6c3893fb12d994a61e5d3f710d70ac91.JPG","gp":"76","tnum":124,"comment":47,"ts":1605090204,"btitle":"\u528d\u8207\u9060\u5f81"},{"bsn":"23805","snA":"647419","title":"\u3010\u60c5\u5831\u3011\u5b98\u65b9\u63a8\u51fa\u300c\u52a0\u8afe\u5967\u65af\u7d00\u5ff5\u982d\u50cf\u6846\u300d","author":"j0800449","wtime":"2020-11-11 17:14:55","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/e826c168042ab9c236c91e9d9661ba6a.JPG","gp":"60","tnum":124,"comment":50,"ts":1605086095,"btitle":"\u795e\u9b54\u4e4b\u5854"}]};

var forum = {
hothala_change: function(who) {
forumtimer = setTimeout("IndexFORUM.hothala_changea('"+who+"')",200);
},

hothala_changea: function(who) {
var html = '';
html += '<table style="width:100%;" class="EXA8">';
html += '  <tr>';
html += '    <td width="15" align="center" valign="middle"><img src="https://i2.bahamut.com.tw/index_w/new_icon1.gif" /></td>';
html += '    <td width="125">哈啦板</td>';
html += '    <td>話題</td>';
html += '    <td align="right">迴響</td>';
html += '  </tr>';

for(i=0; i<data[who].length; i++) {
html += '  <tr>';
html += '    <td width="15" align="center" valign="middle"><span>'+(i+1)+'</span></td>';
html += '    <td width="125"><a href="//forum.gamer.com.tw/A.php?bsn='+data[who][i].bsn+'" class="style2">'+data[who][i].btitle+'</a></td>';
html += '    <td><a href="//forum.gamer.com.tw/C.php?bsn='+data[who][i].bsn+'&snA='+data[who][i].snA+'">'+data[who][i].title+'</a></td>';
html += '    <td align="right">'+data[who][i].tnum+'</td>';
html += '  </tr>';
}

html += '</table>';
html += '<a rel="feedurl" href="//www.gamer.com.tw/ie8_slice.php#sliceHaLa" style="display:none"></a>';
html += '<p><a href="//forum.gamer.com.tw/A.php?bsn=60148">&gt; 申請新板</a> <a href="//forum.gamer.com.tw">&gt; 前往哈啦區</a></p>';

$('.BA-ctag1now').filter('[id^="hothala_"]').removeClass('BA-ctag1now');
$('#hothala_'+who).addClass('BA-ctag1now');
$('#hothala').html(html);
}
};

window.IndexFORUM = forum;
})(window, jQuery);
</script>
<ul class="BA-ctag1">
<li><a id="hothala_other" href="javascript:;" class="BA-ctag1now" onclick="IndexFORUM.hothala_changea('other')" onmouseover="IndexFORUM.hothala_change('other')" onmouseout="clearTimeout(forumtimer)">熱門話題</a></li>
<li><a id="hothala_game" href="javascript:;" onclick="IndexFORUM.hothala_changea('game')" onmouseover="IndexFORUM.hothala_change('game')" onmouseout="clearTimeout(forumtimer)">遊戲話題</a></li>
<li><a id="hothala_ac" href="javascript:;" onclick="IndexFORUM.hothala_changea('ac')" onmouseover="IndexFORUM.hothala_change('ac')" onmouseout="clearTimeout(forumtimer)">動漫話題</a></li>
</ul>
<div class="BA-cbox BA-cbox7" id="hothala">
<table style="width:100%;" class="EXA8">
<tr>
<td width="15" align="center" valign="middle"><img src="https://i2.bahamut.com.tw/index_w/new_icon1.gif" /></td>
<td width="125">哈啦板</td>
<td>話題</td>
<td align="right">迴響</td>
</tr>
<tr>
<td width="15" align="center" valign="middle"><span>1</span></td>
<td width="125"><a href="//forum.gamer.com.tw/A.php?bsn=60608" class="style2">虛擬 Youtuber（Vtuber） </a></td>
<td><a href="//forum.gamer.com.tw/C.php?bsn=60608&snA=1807">【漫畫翻譯】Hololive四格 - プリンアラモード</a></td>
<td align="right">185</td>
</tr><tr>
<td width="15" align="center" valign="middle"><span>2</span></td>
<td width="125"><a href="//forum.gamer.com.tw/A.php?bsn=60030" class="style2">電腦應用綜合討論 </a></td>
<td><a href="//forum.gamer.com.tw/C.php?bsn=60030&snA=563561">【問題】這台二手電腦價位OK嗎?</a></td>
<td align="right">143</td>
</tr><tr>
<td width="15" align="center" valign="middle"><span>3</span></td>
<td width="125"><a href="//forum.gamer.com.tw/A.php?bsn=60084" class="style2">歡樂惡搞 KUSO</a></td>
<td><a href="//forum.gamer.com.tw/C.php?bsn=60084&snA=210700">【心得】【吐崽一吐】回頭</a></td>
<td align="right">108</td>
</tr><tr>
<td width="15" align="center" valign="middle"><span>4</span></td>
<td width="125"><a href="//forum.gamer.com.tw/A.php?bsn=60030" class="style2">電腦應用綜合討論 </a></td>
<td><a href="//forum.gamer.com.tw/C.php?bsn=60030&snA=563615">【心得】歡喜入手ROG3070卡皇，結果悲劇！機殼吃不下！MONTECH Air 900 Black 輕開箱（圖多）</a></td>
<td align="right">69</td>
</tr><tr>
<td width="15" align="center" valign="middle"><span>5</span></td>
<td width="125"><a href="//forum.gamer.com.tw/A.php?bsn=60036" class="style2">綜合公仔玩具討論區</a></td>
<td><a href="//forum.gamer.com.tw/C.php?bsn=60036&snA=54301">【其他】ＧＫ模型上色【Fate】阿爾托莉雅・潘德拉貢</a></td>
<td align="right">55</td>
</tr><tr>
<td width="15" align="center" valign="middle"><span>6</span></td>
<td width="125"><a href="//forum.gamer.com.tw/A.php?bsn=60599" class="style2">Steam 綜合討論板</a></td>
<td><a href="//forum.gamer.com.tw/C.php?bsn=60599&snA=31100">【心得】跟著蘿莉巫女一起冒險吧! / 已上架Steam</a></td>
<td align="right">54</td>
</tr><tr>
<td width="15" align="center" valign="middle"><span>7</span></td>
<td width="125"><a href="//forum.gamer.com.tw/A.php?bsn=60559" class="style2">智慧型手機 </a></td>
<td><a href="//forum.gamer.com.tw/C.php?bsn=60559&snA=52728">【問題】關於最近常常看到的充電器跟行動電源</a></td>
<td align="right">48</td>
</tr><tr>
<td width="15" align="center" valign="middle"><span>8</span></td>
<td width="125"><a href="//forum.gamer.com.tw/A.php?bsn=60497" class="style2">海外生活及旅遊相關 </a></td>
<td><a href="//forum.gamer.com.tw/C.php?bsn=60497&snA=1042">【討論】日本各種罪犯被逮捕的樣子 PART2</a></td>
<td align="right">48</td>
</tr><tr>
<td width="15" align="center" valign="middle"><span>9</span></td>
<td width="125"><a href="//forum.gamer.com.tw/A.php?bsn=60036" class="style2">綜合公仔玩具討論區</a></td>
<td><a href="//forum.gamer.com.tw/C.php?bsn=60036&snA=54304">【問題】想問大家這間玩具店買PVC可以嗎?</a></td>
<td align="right">31</td>
</tr><tr>
<td width="15" align="center" valign="middle"><span>10</span></td>
<td width="125"><a href="//forum.gamer.com.tw/A.php?bsn=60084" class="style2">歡樂惡搞 KUSO</a></td>
<td><a href="//forum.gamer.com.tw/C.php?bsn=60084&snA=210702">【討論】為什麼叫這名字？</a></td>
<td align="right">25</td>
</tr><tr>
<td width="15" align="center" valign="middle"><span>11</span></td>
<td width="125"><a href="//forum.gamer.com.tw/A.php?bsn=60608" class="style2">虛擬 Youtuber（Vtuber） </a></td>
<td><a href="//forum.gamer.com.tw/C.php?bsn=60608&snA=1806">[繪圖]ハッピーシンセサイザ</a></td>
<td align="right">24</td>
</tr><tr>
<td width="15" align="center" valign="middle"><span>12</span></td>
<td width="125"><a href="//forum.gamer.com.tw/A.php?bsn=60001" class="style2">電視遊樂器綜合討論區</a></td>
<td><a href="//forum.gamer.com.tw/C.php?bsn=60001&snA=47673">【閒聊】惡魔獵人5 特別版 雙平台測試</a></td>
<td align="right">22</td>
</tr><tr>
<td width="15" align="center" valign="middle"><span>13</span></td>
<td width="125"><a href="//forum.gamer.com.tw/A.php?bsn=60559" class="style2">智慧型手機 </a></td>
<td><a href="//forum.gamer.com.tw/C.php?bsn=60559&snA=52730">【討論】一些換機上的疑慮想詢問廣大巴友意見</a></td>
<td align="right">21</td>
</tr><tr>
<td width="15" align="center" valign="middle"><span>14</span></td>
<td width="125"><a href="//forum.gamer.com.tw/A.php?bsn=60561" class="style2">職場甘苦談 </a></td>
<td><a href="//forum.gamer.com.tw/C.php?bsn=60561&snA=18512">【討論】大家工作如果不想假日加班有啥好理由呢?</a></td>
<td align="right">20</td>
</tr><tr>
<td width="15" align="center" valign="middle"><span>15</span></td>
<td width="125"><a href="//forum.gamer.com.tw/A.php?bsn=60143" class="style2">畫技交流區</a></td>
<td><a href="//forum.gamer.com.tw/C.php?bsn=60143&snA=44723">【心得】撒西不理~久違的鉛筆素描練習</a></td>
<td align="right">19</td>
</tr><tr>
<td width="15" align="center" valign="middle"><span>16</span></td>
<td width="125"><a href="//forum.gamer.com.tw/A.php?bsn=60001" class="style2">電視遊樂器綜合討論區</a></td>
<td><a href="//forum.gamer.com.tw/C.php?bsn=60001&snA=47674">【討論】3080與最新世代主機4K對比</a></td>
<td align="right">18</td>
</tr><tr>
<td width="15" align="center" valign="middle"><span>17</span></td>
<td width="125"><a href="//forum.gamer.com.tw/A.php?bsn=60545" class="style2">汽機車討論</a></td>
<td><a href="//forum.gamer.com.tw/C.php?bsn=60545&snA=18819">【問題】想買 Suzuki Gixxer 250 sf</a></td>
<td align="right">17</td>
</tr><tr>
<td width="15" align="center" valign="middle"><span>18</span></td>
<td width="125"><a href="//forum.gamer.com.tw/A.php?bsn=60208" class="style2">軍事策略</a></td>
<td><a href="//forum.gamer.com.tw/C.php?bsn=60208&snA=12703">【情報】2.4萬億軍售獲批，包括對阿聯酋的F-35A，是美國歷史上第二大交易。</a></td>
<td align="right">17</td>
</tr><tr>
<td width="15" align="center" valign="middle"><span>19</span></td>
<td width="125"><a href="//forum.gamer.com.tw/A.php?bsn=60292" class="style2">程式設計板</a></td>
<td><a href="//forum.gamer.com.tw/C.php?bsn=60292&snA=6779">【問題】java 資料結構</a></td>
<td align="right">15</td>
</tr><tr>
<td width="15" align="center" valign="middle"><span>20</span></td>
<td width="125"><a href="//forum.gamer.com.tw/A.php?bsn=60037" class="style2">動漫相關綜合</a></td>
<td><a href="//forum.gamer.com.tw/C.php?bsn=60037&snA=75591">【情報】《急戰5秒殊死鬥》TV動畫化決定！</a></td>
<td align="right">14</td>
</tr></table>
<a rel="feedurl" href="//www.gamer.com.tw/ie8_slice.php#sliceHaLa" style="display:none"></a>
<p><a href="//forum.gamer.com.tw/A.php?bsn=60148">&gt; 申請新板</a> <a href="//forum.gamer.com.tw">&gt; 前往哈啦區</a></p>
</div>
<script>
(function(window, $, undefined){
var _fresh = [],
_old = [],
_oldest = [],
_ts15 = Math.floor((new Date()).getTime() / 1000);

function _setData(ttt) {
//距離現在經過了幾個15m
ttt.ts15 = Math.ceil((_ts15 - ttt.ts) / 900);

if (_ts15 - ttt.ts <= 3600) {
_fresh.push(ttt);
} else {

if (_ts15 - ttt.ts <= 46800) {
_old.push(ttt);
} else {
_oldest.push(ttt);
}
}
}

function _sortData(a, b) {
return parseFloat(b.ts) - parseFloat(a.ts);
}

function _getColNo() {
if (!$("#rA").height()) {
return 0;
}

if (!$("#rB").height()) {
return 1;
}

if (!$("#rC").height()) {
return 2;
}

if ($("#rA").height() >= $("#rB").height()) {
return ($("#rB").height() > $("#rC").height()) ? 2 : 1;
} else {
return ($("#rA").height() > $("#rC").height()) ? 2 : 0;
}
}

$.each( HOME_HOME.homedata(), function( hk, hv ) {
if( 'daily' == hk || 'novel' == hk || 'art' == hk || 'cos' == hk || 'loft' == hk || 'urge' == hk ) {
if( 'urge' == hk ) {
homev = hv;
} else {
homev = hv.slice(16);
}

$.each( homev, function( hk1, hv1 ) {
var _tmp = {
'pic': hv1.picB,
'sn': hv1.sn,
'title': hv1.title,
'btitle': hv1.btitle,
'ts': hv1.ts,
'area': '小屋',
'gp': hv1.gp,
'comment': hv1.comment
};

_setData(_tmp);
});
}
});
var forum = [{"bsn":"7650","snA":"1011598","sn":"6330945","btitle":["\u65b0\u6953\u4e4b\u8c37 "],"title":"\u3010\u9592\u804a\u3011\u6a58\u661f\u82b1\u706b \u9080\u8acb\u78bc\u96c6\u4e2d\u4e32","pic":"","ts":1605090162,"gp":"7","comment":699},{"bsn":"25908","snA":"44542","sn":"184116","btitle":["\u5929\u5802 Mobile "],"title":"\u3010\u60c5\u5831\u30112020 \u6a58\u661f\u706b\u82b1\u796d \u5e8f\u865f\u5206\u4eab\u5340","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/ac28b61428a562c472aa2ee076448b8e.JPG","ts":1605095100,"gp":"3","comment":451},{"bsn":"29461","snA":"16460","sn":"53533","btitle":["\u6953\u4e4b\u8c37 M"],"title":"\u3010\u63d2\u756b\u3011\u6b63\u592a\u76ae\u5361\u557e","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/31049b7651224623b3ec98927d655c7f.JPG","ts":1605067817,"gp":"128","comment":108},{"bsn":"37030","snA":"4464","sn":"15713","btitle":["RO \u4ed9\u5883\u50b3\u8aaa\uff1a\u65b0\u4e16\u4ee3\u7684\u8a95\u751f"],"title":"\u3010\u9592\u804a\u3011\u6839\u672c\u5c31\u6c92\u6709\u4ec0\u9ebc60\u9000\u5751\u6f6e","pic":"","ts":1605088062,"gp":"126","comment":103},{"bsn":"47521","snA":"621","sn":"1846","btitle":["\u5bae\u5cf6\u79ae\u540f \u4f5c\u54c1\u96c6\uff08\u51fa\u79df\u5973\u53cb\uff09"],"title":"\u3010\u60c5\u5831\u3011\u6f2b\u756b\u300a\u51fa\u79df\u5973\u53cb\u300b164 \u8a71\u300c\u5f7c\u5973\u3068\u5f7c\u6c0f\u2467\u300d\u5287\u900f \u300a\u91cd\u96f7\u614e\u5165\u300b","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/a6ebd3cd7249d47cd832515eae8cdbbd.JPG","ts":1605062525,"gp":"121","comment":96},{"bsn":"21911","snA":"6237","sn":"25640","btitle":["\u9748\u9b42\u884c\u8005"],"title":"\u3010\u9592\u804a\u3011\u6709\u4eba\u8981\u4e00\u8d77\u5206\u4eab\u65b0\u89d2\u51fa\u4e4b\u524d\u7684\u4ea4\u6613\u6240\u4e82\u8c61\u55ce","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/5eba02a227558eca53defee1a1cc4b01.JPG","ts":1605067932,"gp":"38","comment":160},{"bsn":"60608","snA":"1807","sn":"29376","btitle":["\u865b\u64ec Youtuber\uff08Vtuber\uff09 "],"title":"\u3010\u6f2b\u756b\u7ffb\u8b6f\u3011Hololive\u56db\u683c - \u30d7\u30ea\u30f3\u30a2\u30e9\u30e2\u30fc\u30c9","pic":"https:\/\/i.imgur.com\/qKjq0Cq.jpg","ts":1605097499,"gp":"174","comment":11},{"bsn":"36730","snA":"6378","sn":"37166","btitle":["\u539f\u795e"],"title":"\u3010\u60c5\u5831\u3011\u706b\u7cfb\u53f2\u8a69\u7d1a\u524a\u5f31","pic":"","ts":1605064946,"gp":"46","comment":130},{"bsn":"26742","snA":"69828","sn":"569284","btitle":["Fate\/Grand Order"],"title":"\u3010\u7e6a\u5716\u3011 \u5973\u6885\u6797\u2606","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/2d0fd412b11d7e81e768667db5fdda62.JPG","ts":1605090378,"gp":"156","comment":8},{"bsn":"36730","snA":"6444","sn":"37411","btitle":["\u539f\u795e"],"title":"\u3010\u554f\u984c\u3011\u8a0e\u4f10\u61f8\u8cde\u5230\u5e95\u662f\u54ea\u500b\u667a\u969c\u60f3\u51fa\u4f86\u7684?","pic":"","ts":1605085813,"gp":"75","comment":82},{"bsn":"47157","snA":"367","sn":"1537","btitle":["\u661f\u671f\u4e00\u7684\u8c50\u6eff"],"title":"\u3010\u60c5\u5831\u3011\u300a\u661f\u671f\u4e00\u7684\u8c50\u6eff\u300b\u6f2b\u756b\u7b2c\u4e00\u8a71\u300c\u5c0f\u611b \u2460\u300d\u5f69\u9801&amp;\u5167\u5bb9\u9810\u89bd","pic":"https:\/\/pbs.twimg.com\/media\/EmjjlR7UYAEE2BH.jpg","ts":1605111667,"gp":"142","comment":13},{"bsn":"32550","snA":"10976","sn":"59550","btitle":["\u78a7\u85cd\u822a\u7dda"],"title":"\u3010\u7e6a\u5716\u3011\u7f85\u6069\u03bc","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/1cda45fb8292d1d71dbaba3df0a358c5.JPG","ts":1605068396,"gp":"141","comment":12},{"bsn":"41226","snA":"10979","sn":"78257","btitle":["K-ON!"],"title":"\u3010\u5206\u4eab\u3011\u9b5a \u6240\u756b\u7684\u300aK-ON\uff01\u300b11\/11 \u4e2d\u91ce\u6893 \u751f\u65e5\u8cc0\u5716","pic":"https:\/\/pbs.twimg.com\/media\/EmfW3sAVcAE360q.jpg","ts":1605087895,"gp":"139","comment":12},{"bsn":"30861","snA":"27567","sn":"303020","btitle":["\u8d85\u7570\u57df\u516c\u4e3b\u9023\u7d50\u2606Re:Dive "],"title":"\u3010\u8a0e\u8ad6\u3011\u53f0\u7248\u7af6\u6280\u5834\u91cd\u7f6e \u6253NPC\u968a\u4f0d\u63a8\u85a6","pic":"https:\/\/i1.ytimg.com\/vi\/6bEFJcev1qo\/hqdefault.jpg","ts":1605066417,"gp":"130","comment":17},{"bsn":"38113","snA":"7027","sn":"31928","btitle":["\u5b88\u671b\u50b3\u8aaa Guardian Tales "],"title":"\u3010\u60c5\u5831\u3011\u570b\u969b\u670dfb-\u586b\u554f\u5377\u9001600\u947d\u77f3","pic":"","ts":1605096114,"gp":"70","comment":71},{"bsn":"30861","snA":"27581","sn":"303263","btitle":["\u8d85\u7570\u57df\u516c\u4e3b\u9023\u7d50\u2606Re:Dive "],"title":"\u3010\u7e6a\u5716\u3011\u53ef\u53ef\u863f\u5abd\u5abd\u2668\u2668\u2668\u2668\u2668\u2668","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/ee13beaa17f9bfd5a5aaae117051723d.JPG","ts":1605120995,"gp":"120","comment":16},{"bsn":"39113","snA":"249","sn":"741","btitle":["\u91ce\u751f\u5c11\u5973"],"title":"\u3010\u554f\u984c\u3011\u9000\u4e86\u516c\u6703\u4e4b\u5f8c\u7a81\u7136\u9593\u9084\u6c92\u9032\u65b0\u516c\u6703\u5c31\u88ab\u8e22\u4e86 \u771f\u7121\u5948","pic":"https:\/\/cdn.discordapp.com\/attachments\/282323480354095104\/776028302355726336\/unknown.png","ts":1605090920,"gp":"3","comment":131},{"bsn":"38113","snA":"7034","sn":"31990","btitle":["\u5b88\u671b\u50b3\u8aaa Guardian Tales "],"title":"\u3010\u8a0e\u8ad6\u3011\u7af6\u6280\u5834\u8fd1\u8ddd\u96e2\u6230\u58eb\u89d2\u8272\u904e\u65bc\u5f31\u52e2\u7684\u539f\u56e0","pic":"","ts":1605125704,"gp":"17","comment":117},{"bsn":"38113","snA":"7029","sn":"31955","btitle":["\u5b88\u671b\u50b3\u8aaa Guardian Tales "],"title":"\u3010\u9592\u804a\u3011\u5948\u91cc:\u61f2\u7f70\u4e00\u4e0b\u611a\u8822\u7684\u4eba\u985e\uff01","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/fe3ef715970b65df231023183f7f636e.JPG","ts":1605106286,"gp":"109","comment":24},{"bsn":"30861","snA":"27580","sn":"303251","btitle":["\u8d85\u7570\u57df\u516c\u4e3b\u9023\u7d50\u2606Re:Dive "],"title":"\u3010\u7ffb\u8b6f\u3011\uff14\u30b3\u30de\u6f2b\u753b\u7b2c\uff12\uff12\uff15\u8a71","pic":"https:\/\/i.imgur.com\/q9qmIkO.png","ts":1605115027,"gp":"115","comment":15},{"bsn":"60646","snA":"185","sn":"518","btitle":["Xbox  \/ Xbox Series X "],"title":"\u3010\u60c5\u5831\u3011 Xbox \u904a\u6232\u9054\u4eba\u554f\u7b54\u5927\u8cfd \u96c6\u4e2d\u5340","pic":"","ts":1605077933,"gp":"1","comment":127},{"bsn":"36833","snA":"16356","sn":"67664","btitle":["\u528d\u8207\u9060\u5f81"],"title":"\u3010\u60c5\u5831\u3011\u5148\u92d2\u670d 1.51\u7248\u672c\u66f4\u65b0\u516c\u544a","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/6c3893fb12d994a61e5d3f710d70ac91.JPG","ts":1605090204,"gp":"76","comment":48},{"bsn":"23805","snA":"647419","sn":"3814031","btitle":["\u795e\u9b54\u4e4b\u5854"],"title":"\u3010\u60c5\u5831\u3011\u5b98\u65b9\u63a8\u51fa\u300c\u52a0\u8afe\u5967\u65af\u7d00\u5ff5\u982d\u50cf\u6846\u300d","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/e826c168042ab9c236c91e9d9661ba6a.JPG","ts":1605086095,"gp":"60","comment":64},{"bsn":"26742","snA":"69833","sn":"569298","btitle":["Fate\/Grand Order"],"title":"\u3010\u60c5\u5831\u3011\u68b5\u8c37\u548c\u5c3c\u83ab\u7684\u6280\u80fd\u7d44","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/7781895c9ae77d5e33c928a9e39576ac.JPG","ts":1605094893,"gp":"47","comment":74},{"bsn":"26686","snA":"55010","sn":"823637","btitle":["\u767d\u8c93 Project"],"title":"\u3010\u60c5\u5831\u3011SERIOUS BREAKER PV","pic":"https:\/\/i1.ytimg.com\/vi\/utJkkYAJv-A\/hqdefault.jpg","ts":1605077248,"gp":"25","comment":92},{"bsn":"21911","snA":"6247","sn":"25693","btitle":["\u9748\u9b42\u884c\u8005"],"title":"\u3010\u5fc3\u5f97\u3011\u95dc\u65bc\u672a\u4f86\u8996","pic":"https:\/\/cdn.discordapp.com\/attachments\/767631439566864397\/776048985684049920\/unknown.png","ts":1605095620,"gp":"20","comment":97},{"bsn":"36730","snA":"6508","sn":"37689","btitle":["\u539f\u795e"],"title":"\u3010\u5fc3\u5f97\u30110\u547d\u516c\u5b50\u57f9\u80b2\u6cd5--\u5e73\u6c11\u73a9\u5bb6\u904a\u73a9\u5fc3\u5f97","pic":"","ts":1605110450,"gp":"39","comment":77},{"bsn":"37459","snA":"623","sn":"1785","btitle":["\u82f1\u96c4\u806f\u76df\uff1a\u6fc0\u9b25\u5cfd\u8c37"],"title":"\u3010\u60c5\u5831\u3011\u4eca\u5929\u662f\u6211\u7b2c\u4e00\u6b21\u5728\u6fc0\u9b25\u5cfd\u8c37\u653e\u63a8","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/1e8f879ab13527ec17176dbb76f56740.JPG","ts":1605089721,"gp":"84","comment":30},{"bsn":"37030","snA":"4517","sn":"15946","btitle":["RO \u4ed9\u5883\u50b3\u8aaa\uff1a\u65b0\u4e16\u4ee3\u7684\u8a95\u751f"],"title":"\u3010\u653b\u7565\u3011\u55ef..\u6191\u4ec0\u9ebc\u4e03\u5343\u8868\u653b\u4e5f\u80fd\u55ae\u5403\u914b\u9577??(\u542b\u5f71\u7247)~\u6240\u4ee5\u6211\u5230\u5e95\u662f\u5916\u639b\u9084\u662f\u5de5\u8b80\u751f..\u9084\u662f\u795e?","pic":"https:\/\/i1.ytimg.com\/vi\/Cv6XuA5KfhY\/hqdefault.jpg","ts":1605141727,"gp":"43","comment":69},{"bsn":"33651","snA":"4162","sn":"40360","btitle":["\u660e\u65e5\u65b9\u821f "],"title":"\u3010\u9592\u804a\u3011\u53f0\u670d\u6d3b\u52d5\u6392\u7a0b\u9810\u6e2c\/\u672a\u4f86\u8996 \u7c21\u6613\u6574\u7406_11\/11\u66f4\u65b0","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/b6f4122aa5e13b2a0956f085d4ae1743.PNG","ts":1605081458,"gp":"80","comment":30},{"bsn":"36730","snA":"6427","sn":"37333","btitle":["\u539f\u795e"],"title":"\u3010\u60c5\u5831\u3011\u8b66\u544a\uff01\u8b66\u544a\uff01","pic":"","ts":1605078447,"gp":"36","comment":74},{"bsn":"60084","snA":"210700","sn":"2649721","btitle":["\u6b61\u6a02\u60e1\u641e KUSO"],"title":"\u3010\u5fc3\u5f97\u3011\u3010\u5410\u5d3d\u4e00\u5410\u3011\u56de\u982d","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/862081880a0e866f6f119c1b54cfd3e0.JPG","ts":1605064293,"gp":"83","comment":25},{"bsn":"38113","snA":"7026","sn":"31921","btitle":["\u5b88\u671b\u50b3\u8aaa Guardian Tales "],"title":"\u3010\u7e6a\u5716\u3011\u6230\u722d\u5973\u795e-\u666e\u8389\u83f2\u7d72","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/89fd3f2375904c16a5550e468bb13765.JPG","ts":1605090952,"gp":"96","comment":11},{"bsn":"36730","snA":"6459","sn":"37470","btitle":["\u539f\u795e"],"title":"\u3010\u8a0e\u8ad6\u3011\uff3b\u6301\u7e8c\u66f4\u65b0\uff3d\u95dc\u65bc1.1\u7248\u672c\u5b98\u65b9\u6c92\u8aaa\u7684\u6697\u6539","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/b725e969d491357f0c2d123d860f600c.JPG","ts":1605090854,"gp":"31","comment":74},{"bsn":"60596","snA":"51690","sn":"282213","btitle":["PS4 \/ PlayStation4 "],"title":"\u3010\u60c5\u5831\u3011CDPR :\u96fb\u99ad\u53db\u5ba22077 \u4e9e\u6d32\u7248\u4e0d\u6703\u548c\u8ae7","pic":"https:\/\/i.imgur.com\/5wXfIq1.jpg","ts":1605086363,"gp":"65","comment":40},{"bsn":"5786","snA":"158598","sn":"961149","btitle":["\u9b54\u7269\u7375\u4eba"],"title":"\u3010\u5bc6\u6280\u3011\u51b0\u539f\u8c46\u77e5\u8b58\uff01\uff08\u6b61\u8fce\u5927\u5bb6\u5e6b\u5fd9\u88dc\u5b8c\uff01\uff09","pic":"","ts":1605109108,"gp":"74","comment":28},{"bsn":"25052","snA":"83519","sn":"515945","btitle":["\u602a\u7269\u5f48\u73e0"],"title":"\u3010\u60c5\u5831\u3011\u300a\u602a\u7269\u5f48\u73e0\u300b\u8207\u300a\u4e03\u5927\u7f6a \u61a4\u6012\u7684\u5be9\u5224\u300b\u7684\u7b2c2\u5f48\u5408\u4f5c\u6d3b\u52d5\u65bc11\u670814\u65e5\uff08\u516d\uff09\u4e2d\u5348\u8d77\u958b\u8dd1\uff01","pic":"https:\/\/www.monster-strike.com.tw\/entryimages\/5V_%25BnfjJerE%23e_TYeburjhV_CWyE_C3vwbr5.png","ts":1605083777,"gp":"66","comment":35},{"bsn":"37203","snA":"2308","sn":"23888","btitle":["\u70ba\u7f8e\u597d\u7684\u4e16\u754c\u737b\u4e0a\u795d\u798f\uff01Fantastic Days"],"title":"\u3010\u653b\u7565\u3011\u51ac\u5c07\u8ecd\uff08\u6c34\uff09\u7af6\u6280\u5834\uff0811\/11~11\/18\uff09\u653b\u7565&amp;\u96c6\u4e2d\u8a0e\u8ad6\u4e32","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/3bfd9c0d399f408b5d0514eb42f43df5.JPG","ts":1605065384,"gp":"58","comment":42},{"bsn":"38144","snA":"10312","sn":"28595","btitle":["\u704c\u7c43\u9ad8\u624b SLAM DUNK"],"title":"\u3010\u96c6\u4e2d\u4e32\u301111\/11\u66f4\u65b0\u7dad\u4fee\u5f8c\u76f8\u95dc\u8a0e\u8ad6\u96c6\u4e2d\u4e32(\u5305\u542b\u9032\u968e\u5bae\u57ce\u8a0e\u8ad6)","pic":"","ts":1605068444,"gp":"2","comment":97},{"bsn":"36833","snA":"16361","sn":"67687","btitle":["\u528d\u8207\u9060\u5f81"],"title":"\u3010\u60c5\u5831\u3011\u5148\u92d2\u670d \u5947\u5883\u63a2\u96aa \u6b7b\u9727\u6cbc\u6fa4\u734e\u52f5\u4e00\u89bd(\u6b64\u7bc7\u6301\u7e8c\u66f4\u65b0\u81f3\u8def\u7dda\u5716\u5b8c\u6210)","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/7cb10fd33081919c83b46539bdaa12fb.JPG","ts":1605139469,"gp":"79","comment":13},{"bsn":"35284","snA":"2348","sn":"8631","btitle":["\u5fa9\u6d3b\u90aa\u795e Re ; universe"],"title":"\u3010\u60c5\u5831\u30111111\u76ee\u524d\u6d3b\u52d5\u60c5\u5831\u5c0f\u6574\u7406","pic":"","ts":1605063695,"gp":"72","comment":19},{"bsn":"36730","snA":"6374","sn":"37152","btitle":["\u539f\u795e"],"title":"\u3010\u9592\u804a\u3011\u83ab\u5a1c\u7684\u5f85\u6a5f\u52d5\u4f5c\u597d\u50cf\u66f4\u65b0\u4e86 (\u5404\u7a2e\u5f85\u6a5f\u66f4\u65b0\u7684\u89d2\u8272)","pic":"https:\/\/i.imgur.com\/1k4BCeX.jpg","ts":1605063339,"gp":"40","comment":49},{"bsn":"34173","snA":"10657","sn":"64012","btitle":["Dragalia Lost \uff5e\u5931\u843d\u7684\u9f8d\u7d46\uff5e"],"title":"\u3010\u5fc3\u5f97\u3011\u8ab0\u4e5f\u64cb\u4e0d\u4f4f\u8fa3\u500b\u81ea\u7531\u7684\u5973\u4eba x \u95c7\u54a2\u8d85\u7d1a x \u81ea\u7531\u7684\u5973\u4eba\uff10\uff1a\uff13\uff18\u8d85\u9ad8\u901f\u5468\u56de","pic":"https:\/\/i1.ytimg.com\/vi\/uYNoE2IVjdQ\/hqdefault.jpg","ts":1605079683,"gp":"43","comment":43},{"bsn":"23805","snA":"647423","sn":"3814059","btitle":["\u795e\u9b54\u4e4b\u5854"],"title":"\u3010\u60c5\u5831\u3011\u65b0\u7248\u672c\u7684\u63d0\u793a\ufe30\u6c99\u863f\u8036 (Charlouette)","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/41e65f735e8412fc3d3d94683b9a62c8.JPG","ts":1605096597,"gp":"31","comment":53},{"bsn":"36730","snA":"6426","sn":"37329","btitle":["\u539f\u795e"],"title":"\u3010\u8a0e\u8ad6\u30111.1 \u62bd\u5361 \u770b\u770b\u80fd\u96c6\u4e2d\u55ce","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/9f7184f4482e40ebe5586d71f59e639f.JPG","ts":1605078038,"gp":"5","comment":79},{"bsn":"37030","snA":"4472","sn":"15763","btitle":["RO \u4ed9\u5883\u50b3\u8aaa\uff1a\u65b0\u4e16\u4ee3\u7684\u8a95\u751f"],"title":"\u3010\u8a0e\u8ad6\u3011\u6c42\u6c42\u4f60\u5011\u8981\u9000\u5751\u7684\u5feb\u9ede\u8d70","pic":"","ts":1605098628,"gp":"72","comment":10},{"bsn":"34173","snA":"10660","sn":"64017","btitle":["Dragalia Lost \uff5e\u5931\u843d\u7684\u9f8d\u7d46\uff5e"],"title":"\u3010\u653b\u7565\u3011AUTO\u8d85\u6697\u7259 \uff3b\u5149\u88dc\uff0f\u5149\u4e03\uff0f\u8001\u982d\u69cd\uff0f\u76ae\u4e9e\u5c3c\uff3d","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/21314db919b6848106ebb32e66510f45.JPG","ts":1605098538,"gp":"53","comment":29},{"bsn":"28271","snA":"9530","sn":"35503","btitle":["\u6258\u862d\u7570\u4e16\u9304 Toram Online RPG"],"title":"\u3010\u60c5\u5831\u3011\u642c\u904b\u9b3c\u624d\u7389\u679d\u5b50\u503c\u5f97\u9ad4\u9a57\u55ae\u6a5f\u7248\u6258\u862d","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/09d270abfacec5a3f9dc11bde350f916.JPG","ts":1605074543,"gp":"10","comment":72},{"bsn":"21911","snA":"6248","sn":"25701","btitle":["\u9748\u9b42\u884c\u8005"],"title":"\u3010\u5fc3\u5f97\u3011\u540c\u578b\u865f\u52f3\u7ae0\u7684\u9078\u64c7(SD BSK FOT SIN)","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/fb376b9461a675cdfe72e89e752d2101.JPG","ts":1605101137,"gp":"44","comment":38},{"bsn":"36730","snA":"6438","sn":"37381","btitle":["\u539f\u795e"],"title":"\u3010\u554f\u984c\u30111.1\u7248\u66f4\u65b0 \u5730\u7406\u8a8c \u62fe\u53d6\u9ede\u96c6\u4e2d\u4e32 \u3010\u5df2\u66f4\u65b0\u5f59\u6574\u5b8c\u6210!\u3011","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/2e255719ba4dcc3c570e1b36828ca0ce.JPG","ts":1605082520,"gp":"46","comment":34},{"bsn":"36730","snA":"6415","sn":"37290","btitle":["\u539f\u795e"],"title":"\u3010\u5fc3\u5f97\u3011\u7248\u672c\u5b64\u5152\u523b\u6674\u53c8\u65b0\u4e00\u4f8b\u6158\u72c0 \uff08\u5f97\u6551\u4e86\uff09","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/149ea7a49fc66c8d2d4da855b82a2489.JPG","ts":1605073946,"gp":"11","comment":69},{"bsn":"36730","snA":"6392","sn":"37211","btitle":["\u539f\u795e"],"title":"\u3010\u60c5\u5831\u30111.1\u7248\u672c  &quot;\u66f4\u65b0\u7d30\u7bc0&quot;","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202010\/0328d2c47df62e977ad1d66d386b3984.JPG","ts":1605068295,"gp":"48","comment":31},{"bsn":"60597","snA":"12166","sn":"37137","btitle":["Xbox One"],"title":"\u3010\u9592\u804a\u3011\u5df4\u54c8\u8fa6\u7684XSS\u62bd\u734e","pic":"","ts":1605075820,"gp":"2","comment":76},{"bsn":"26686","snA":"55012","sn":"823649","btitle":["\u767d\u8c93 Project"],"title":"\u3010\u9592\u804a\u3011\u75284\u5f35\u5716\u8868\uff0c\u770b\u767d\u8c93\u71df\u6536 (\u7528\u6a02\u57182\u7684\u4f86\u6551\u71df\u6536\u7684\u7b56\u7565\u5230\u5e95...\uff1f\uff01)","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/9b33a208640ea620042c022615275692.PNG","ts":1605117111,"gp":"31","comment":46},{"bsn":"21911","snA":"6241","sn":"25659","btitle":["\u9748\u9b42\u884c\u8005"],"title":"\u3010\u9592\u804a\u3011\u885d\u88dd\u885d\u5230\u5fc3\u614b\u5d29\u4e86\u90fd\u600e\u9ebc\u8abf\u6574","pic":"","ts":1605084587,"gp":"34","comment":40},{"bsn":"23805","snA":"647417","sn":"3814026","btitle":["\u795e\u9b54\u4e4b\u5854"],"title":"\u3010\u60c5\u5831\u3011\u300c\u99ac\u6232\u5718\u5718\u9577 \u2027 \u99ac\u5217\u65af\u300d","pic":"https:\/\/i.imgur.com\/5X1cu1Z.png","ts":1605085255,"gp":"27","comment":44},{"bsn":"23805","snA":"647422","sn":"3814051","btitle":["\u795e\u9b54\u4e4b\u5854"],"title":"\u3010\u5fc3\u5f97\u3011\u4e8c\u842c\u91cc\u4e0d\u8f49\u73e0\u61f6\u4eba\u6253\u6cd5","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/09dfdc75afab365386dced0a2d94958b.JPG","ts":1605094250,"gp":"37","comment":33},{"bsn":"60030","snA":"563615","sn":"2285505","btitle":["\u96fb\u8166\u61c9\u7528\u7d9c\u5408\u8a0e\u8ad6 "],"title":"\u3010\u5fc3\u5f97\u3011\u6b61\u559c\u5165\u624bROG3070\u5361\u7687\uff0c\u7d50\u679c\u60b2\u5287\uff01\u6a5f\u6bbc\u5403\u4e0d\u4e0b\uff01MONTECH Air 900 Black \u8f15\u958b\u7bb1\uff08\u5716\u591a\uff09","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/0277be590b943166ef377c46747cc90f.JPG","ts":1605108903,"gp":"38","comment":31},{"bsn":"36730","snA":"6489","sn":"37619","btitle":["\u539f\u795e"],"title":"\u3010\u9592\u804a\u30111.1\u66f4\u65b0\u62b1\u6028\u96c6\u4e2d\u4e32","pic":"","ts":1605103583,"gp":"12","comment":53},{"bsn":"37030","snA":"4460","sn":"15681","btitle":["RO \u4ed9\u5883\u50b3\u8aaa\uff1a\u65b0\u4e16\u4ee3\u7684\u8a95\u751f"],"title":"\u3010\u554f\u984c\u3011DPS\u6548\u76ca\u6700\u5927\u5316\uff01","pic":"","ts":1605080898,"gp":"41","comment":24},{"bsn":"13211","snA":"163272","sn":"674448","btitle":["\u65b0\u9f8d\u4e4b\u8c37 Dragon Nest"],"title":"\u3010\u5176\u4ed6\u30112020\u5468\u5e74\u796d-\u6a58\u661f\u82b1\u706b \u9080\u8acb\u78bc\u5206\u4eab\/\u5c0b\u627e\u5340","pic":"","ts":1605096528,"gp":"3","comment":62},{"bsn":"26742","snA":"69835","sn":"569311","btitle":["Fate\/Grand Order"],"title":"\u3010\u7e6a\u5716\u3011\u9593\u6850\u685c","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/25491be81554d9d7b685da645bfd029a.JPG","ts":1605096605,"gp":"61","comment":3},{"bsn":"38113","snA":"7025","sn":"31918","btitle":["\u5b88\u671b\u50b3\u8aaa Guardian Tales "],"title":"\u3010\u5fc3\u5f97\u3011\u5b88\u671b\u50b3\u8aaa \u91d1\u50f9\u8b9a\u5566","pic":"","ts":1605087246,"gp":"31","comment":33},{"bsn":"35604","snA":"393","sn":"941","btitle":["A3: STILL ALIVE \u5016\u5b58\u8005 "],"title":"\u3010\u5bc6\u6280\u3011\u9001\u4e8b\u524d\u745e\u8482\u5b89\u5148\u9063\u968a\u5e8f\u865f + \u4e8b\u524d\u9748\u9b42\u4e4b\u661f 11\/12","pic":"","ts":1605084751,"gp":"13","comment":51},{"bsn":"26742","snA":"69827","sn":"569279","btitle":["Fate\/Grand Order"],"title":"\u3010\u60c5\u5831\u3011\u6559\u738b2.22.0\u6280\u80fd\u89e3\u5305","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/c3e3d878fa93a4d4bc8d3f430070198e.PNG","ts":1605081180,"gp":"25","comment":36},{"bsn":"38113","snA":"7024","sn":"31898","btitle":["\u5b88\u671b\u50b3\u8aaa Guardian Tales "],"title":"\u3010\u5fc3\u5f97\u3011(\u5287\u900f)\u5c0f\u72d7\u72d7\uff0c\u5341\u5e74\u5f8c\u4e86\u4f60\u9084\u662f\u6211\u6700\u597d\u7684\u670b\u53cb","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/6546df172bbda271d7d07db519984955.JPG","ts":1605076092,"gp":"34","comment":27},{"bsn":"17532","snA":"668516","sn":"5709735","btitle":["\u82f1\u96c4\u806f\u76df League of Legends "],"title":"\u3010\u5fc3\u5f97\u3011S11\u88dd\u5099\u6539\u52d5\uff0c\u8f14\u52a9\u5148\u884c\u63a2\u8def\u3002","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/af93cabc6f59b5c835e5c771b33793fe.JPG","ts":1605106825,"gp":"37","comment":24},{"bsn":"37203","snA":"2313","sn":"23930","btitle":["\u70ba\u7f8e\u597d\u7684\u4e16\u754c\u737b\u4e0a\u795d\u798f\uff01Fantastic Days"],"title":"\u3010\u5fc3\u5f97\u3011\u4e0d\u4e00\u6a23\u7684\u4e3b\u9801\u9762","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/be61e0761767039453d83ec27cac1d27.JPG","ts":1605091206,"gp":"43","comment":17},{"bsn":"39333","snA":"3","sn":"3","btitle":["\u9748\u9b42\u65b9\u821f"],"title":"\u3010\u5fc3\u5f97\u3011\u89d2\u8272\u6392\u884c\u8868","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/9cb45014d92e100d16ef1df85c3560fb.JPG","ts":1605082183,"gp":"19","comment":40},{"bsn":"60645","snA":"494","sn":"2395","btitle":["PS5 \/ PlayStation5 "],"title":"\u3010\u60c5\u5831\u3011CDPR :\u96fb\u99ad\u53db\u5ba22077 \u4e9e\u6d32\u7248\u4e0d\u6703\u548c\u8ae7","pic":"https:\/\/i.imgur.com\/5wXfIq1.jpg","ts":1605086033,"gp":"40","comment":19},{"bsn":"7650","snA":"1011592","sn":"6330920","btitle":["\u65b0\u6953\u4e4b\u8c37 "],"title":"\u3010\u60c5\u5831\u3011MESA v199 AWAKE (\u6230\u570bV4)","pic":"https:\/\/i.imgur.com\/dZYUhy4.png","ts":1605075750,"gp":"19","comment":38},{"bsn":"23805","snA":"647411","sn":"3813987","btitle":["\u795e\u9b54\u4e4b\u5854"],"title":"\u3010\u5fc3\u5f97\u3011\u53c8\u4f86\u4e94\u5c6c\u64ca\u76fe? \u6253\u843d\u51a5\u6df5\u3002\u4e0b \u5929\u5143\u7a81\u7834\u901a\u95dc","pic":"https:\/\/i1.ytimg.com\/vi\/sM1lk_oHJCs\/hqdefault.jpg","ts":1605064294,"gp":"50","comment":7},{"bsn":"36730","snA":"6403","sn":"37248","btitle":["\u539f\u795e"],"title":"\u3010\u554f\u984c\u3011\u65b0\u6389\u5bf6\u4e18\u4e18\u4eba","pic":"","ts":1605070880,"gp":"3","comment":54},{"bsn":"21911","snA":"6261","sn":"25741","btitle":["\u9748\u9b42\u884c\u8005"],"title":"\u3010\u5fc3\u5f97\u3011\u52f3\u7ae0\u516c\u9053\u4f2f","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/7035f5a939cd6bd8cf5a0f887f6aca3c.JPG","ts":1605144307,"gp":"15","comment":42},{"bsn":"30861","snA":"27576","sn":"303219","btitle":["\u8d85\u7570\u57df\u516c\u4e3b\u9023\u7d50\u2606Re:Dive "],"title":"\u3010\u5176\u4ed6\u3011\u51f1\u7559 \u30e1\u30ea\u30fc\u30af\u30ea\u30b9\u30de\u30b9","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/5f7140a64b888dc0c14250b2ffa103f3.JPG","ts":1605108579,"gp":"50","comment":6},{"bsn":"36730","snA":"6409","sn":"37265","btitle":["\u539f\u795e"],"title":"\u3010\u8a0e\u8ad6\u30111.1\u516c\u5b50\u6e2c\u8a66\u61f6\u4eba\u5305","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/c92782e86da5cedff25672d145ea0791.JPG","ts":1605071833,"gp":"24","comment":32},{"bsn":"45581","snA":"987","sn":"7625","btitle":["\u62f3\u9858\u963f\u4fee\u7f85"],"title":"\u3010\u60c5\u5831\u301185\u8a71\uff0c\u80fd\u6d3b\u8457\u4e0b\u53bb\u7684\u53ea\u6709\u4e00\u4eba","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/a2d6022a44236089b9056f0c86c50d0a.JPG","ts":1605108537,"gp":"18","comment":38},{"bsn":"26742","snA":"69836","sn":"569322","btitle":["Fate\/Grand Order"],"title":"\u3010\u653b\u7565\u30112020\u865b\u6578\u5927\u6d77\u6230\u7d20\u6750\u95dc(\u66f4\u65b0\u4e2d)","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/7507fd49dc4b5ce35f595fea04b733ce.JPG","ts":1605098807,"gp":"40","comment":16},{"bsn":"60036","snA":"54301","sn":"307873","btitle":["\u7d9c\u5408\u516c\u4ed4\u73a9\u5177\u8a0e\u8ad6\u5340"],"title":"\u3010\u5176\u4ed6\u3011\uff27\uff2b\u6a21\u578b\u4e0a\u8272\u3010Fate\u3011\u963f\u723e\u6258\u8389\u96c5\u30fb\u6f58\u5fb7\u62c9\u8ca2","pic":"https:\/\/i.imgur.com\/r3kn37s.jpg","ts":1605091339,"gp":"54","comment":1},{"bsn":"60599","snA":"31100","sn":"97907","btitle":["Steam \u7d9c\u5408\u8a0e\u8ad6\u677f"],"title":"\u3010\u5fc3\u5f97\u3011\u8ddf\u8457\u863f\u8389\u5deb\u5973\u4e00\u8d77\u5192\u96aa\u5427! \/ \u5df2\u4e0a\u67b6Steam","pic":"https:\/\/i1.ytimg.com\/vi\/4HQ_krla0pY\/hqdefault.jpg","ts":1605073729,"gp":"27","comment":27},{"bsn":"30861","snA":"27554","sn":"302844","btitle":["\u8d85\u7570\u57df\u516c\u4e3b\u9023\u7d50\u2606Re:Dive "],"title":"\u3010\u8a0e\u8ad6\u301111\/11\u81f311\/12\u865f\u4e0b\u53483\u9ede\u5f8c \u7af6\u6280\u5834\u4e0b\u9632\u904b\u52d5 \u7d66\u65b0\u624b\u628a\u7926\u6316\u5b8c","pic":"","ts":1604989204,"gp":"588","comment":312},{"bsn":"60561","snA":"18509","sn":"144398","btitle":["\u8077\u5834\u7518\u82e6\u8ac7 "],"title":"\u3010\u5fc3\u5f97\u3011\u597d\u5fc3\u88ab\u96f7\u89aa\uff0c\u90e8\u5206\u5df4\u53cb\u7684\u5fc3\u614b\u771f\u7684\u4e0d\u6562\u606d\u7dad","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/9529a6cc0779640d00e6900573ef5c37.JPG","ts":1605045475,"gp":"592","comment":176},{"bsn":"25052","snA":"83516","sn":"515884","btitle":["\u602a\u7269\u5f48\u73e0"],"title":"\u3010\u60c5\u5831\u301111\/11\u65e5\u7248\u751f\u653e\u9001\u4e32(\u4e03\u5927\u7f6a\u7b2c\u4e8c\u5f48\uff0c\u6fc0\u7378\uff0c\u68ee\u862d\u4e38\u7378\u795e\u5316\uff0c\u672a\u958b\u5236\u9738\u734e\u52f5)","pic":"https:\/\/i1.ytimg.com\/vi\/9iCUPjWjsuY\/hqdefault.jpg","ts":1605044712,"gp":"102","comment":197},{"bsn":"25052","snA":"83517","sn":"515885","btitle":["\u602a\u7269\u5f48\u73e0"],"title":"\u3010\u60c5\u5831\u301111\/11 \u5f48\u73e0\u7206\u65b0\u805e (\u4e03\u5927\u7f6a\u7b2c\u4e8c\u5f48\uff0c\u672a\u958b\u4e4b\u5927\u5730\u734e\u52f5\u66f4\u65b0\uff1a\u9001\u9650\u5b9a\u89d2\u8272\u7b49\u7b49\uff0c\u68ee\u862d\u4e38\u7378\u795e\u5316)","pic":"https:\/\/i1.ytimg.com\/vi\/Tde_boNJw7I\/hqdefault.jpg","ts":1605047333,"gp":"115","comment":163},{"bsn":"37030","snA":"4398","sn":"15383","btitle":["RO \u4ed9\u5883\u50b3\u8aaa\uff1a\u65b0\u4e16\u4ee3\u7684\u8a95\u751f"],"title":"\u3010\u60c5\u5831\u3011\u73fe\u5728\u904a\u6232\u6700\u5927\u7684\u554f\u984c #\u6700\u65b0\u5f8c\u7e8c\u6587\u7ae0","pic":"","ts":1605015030,"gp":"207","comment":126},{"bsn":"36730","snA":"6336","sn":"36926","btitle":["\u539f\u795e"],"title":"\u3010\u60c5\u5831\u3011\u5b98\u65b911\u670810\u65e5\uff0c\u518d\u9001\u539f\u77f3\uff01","pic":"","ts":1605010603,"gp":"209","comment":14},{"bsn":"33651","snA":"4148","sn":"40220","btitle":["\u660e\u65e5\u65b9\u821f "],"title":"\u3010\u7e6a\u5716\u3011\u9ed1","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/bcb6d3eac9349d30e4f125a9c4e1aa95.JPG","ts":1604921297,"gp":"337","comment":1},{"bsn":"23805","snA":"647351","sn":"3813640","btitle":["\u795e\u9b54\u4e4b\u5854"],"title":"\u3010\u7e6a\u5716\u3011\u6cf3\u88dd\u5207\u897f\u4e9e&amp;\u5c0f\u59b9","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/b2cfd5df7394cf9c5cb0d7e5ca3d1ee2.JPG","ts":1604923687,"gp":"161","comment":0},{"bsn":"23805","snA":"647354","sn":"3813661","btitle":["\u795e\u9b54\u4e4b\u5854"],"title":"\u3010\u5176\u4ed6\u3011\u96d9\u9031\u6e1b\u50b7\u7a0b\u5ea6\u7d2f\u52a0","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/72dfe20c1e0dd3e816f4702fe27f707c.JPG","ts":1604929872,"gp":"236","comment":0},{"bsn":"32550","snA":"10972","sn":"59535","btitle":["\u78a7\u85cd\u822a\u7dda"],"title":"\u3010\u7e6a\u5716\u3011\u4e8c\u4eba\u5408\u5531","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/bc0b83d6df0f705b19ba6228a5fafc7e.JPG","ts":1604990702,"gp":"318","comment":2},{"bsn":"47377","snA":"141","sn":"285","btitle":["\u9b54\u5973\u4e4b\u65c5"],"title":"\u3010\u7e6a\u5716\u3011\u4f0a\u857e\u5a1c","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/fe7b497feda56f1584e1c108f4668f30.JPG","ts":1604924983,"gp":"164","comment":0},{"bsn":"46988","snA":"3697","sn":"24692","btitle":["\u9b3c\u6ec5\u4e4b\u5203"],"title":"\u3010\u5fc3\u5f97\u3011\u8ac7\u8ac7\u9b3c\u6ec5\u5728\u65e5\u672c\u70ba\u4f55\u90a3\u9ebc\u7d05?","pic":"","ts":1605023624,"gp":"18","comment":72},{"bsn":"23805","snA":"647383","sn":"3813835","btitle":["\u795e\u9b54\u4e4b\u5854"],"title":"\u3010\u5176\u4ed6\u3011\u5c0d\u65bc\u5341\u4e00\u5c01\u8ddf\u7279\u54e5\u89e3\u653e\u7684\u611f\u60f3","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/8485e1e4e5e95a157f360833cb931434.JPG","ts":1604995433,"gp":"375","comment":11},{"bsn":"60599","snA":"31095","sn":"97879","btitle":["Steam \u7d9c\u5408\u8a0e\u8ad6\u677f"],"title":"\u3010\u60c5\u5831\u3011\u53f0\u7063\u6210\u4eba3D\u300a\u7378\u4eba\u6309\u6469\u5e97\u300b\u7f8e\u4eba\u9b5a\u5be6\u5728\u592a\u5927\u4e86\uff0cSteam\u6709\u671b\u5e74\u5167\u4e0a\u67b6","pic":"https:\/\/i.imgur.com\/gZsl9zv.jpg","ts":1604965412,"gp":"224","comment":2},{"bsn":"23805","snA":"647393","sn":"3813885","btitle":["\u795e\u9b54\u4e4b\u5854"],"title":"\u3010\u5275\u4f5c\u3011\u7d50\u679c\u9084\u662f\u5fcd\u4e0d\u4f4f\u756b\u4e86(\u7206\u96f7\u614e\u5165","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/217761b65a009789a01111e4adf54a58.JPG","ts":1605005230,"gp":"181","comment":3},{"bsn":"44991","snA":"3007","sn":"13191","btitle":["\u679c\u7136\u6211\u7684\u9752\u6625\u6200\u611b\u559c\u5287\u641e\u932f\u4e86"],"title":"\u3010\u60c5\u5831\u3011\u7a3b\u9cf4\u56db\u5b63\u8001\u5e2b\u756b\u7684\u9b54\u5973\u88dd\u5e73\u585a\u975c","pic":"https:\/\/pbs.twimg.com\/media\/EmWZjJiVMAAYmXp.jpg","ts":1604891618,"gp":"143","comment":0},{"bsn":"29659","snA":"40396","sn":"233017","btitle":["Pokemon GO "],"title":"\u3010\u60c5\u5831\u3011Pok\u00e9mon GO to HOME \u5df2\u7d93\u958b\u653e","pic":"","ts":1605058883,"gp":"10","comment":60},{"bsn":"33651","snA":"4159","sn":"40312","btitle":["\u660e\u65e5\u65b9\u821f "],"title":"\u3010\u6d3b\u52d5\u3011\u65e5\u3001\u570b\u969b\u670d\u5371\u6a5f\u5408\u7d04\u300c\u9ec3\u9435\u884c\u52d5\u300d\u5e38\u99d018\u934d\u5c64\u5206\u4eab\u4e32","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/5da84ca05a83568574306dc6dbb0958a.JPG","ts":1605016699,"gp":"35","comment":53},{"bsn":"1647","snA":"113260","sn":"528659","btitle":["\u795e\u5947\u5bf6\u8c9d\uff08\u7cbe\u9748\u5bf6\u53ef\u5922\uff09\u7cfb\u5217 "],"title":"\u3010\u60c5\u5831\u3011Pok\u00e9mon GO\u2192Pok\u00e9mon HOME \u5df2\u7d93\u958b\u653e","pic":"https:\/\/i.imgur.com\/iWI9jBe.jpg","ts":1605048841,"gp":"26","comment":59},{"bsn":"26742","snA":"69818","sn":"569237","btitle":["Fate\/Grand Order"],"title":"\u3010\u7e6a\u5716\u3011\u963f\u6bd4\u6bd4","pic":"https:\/\/imgur.com\/5WuA16v.jpg","ts":1604966950,"gp":"345","comment":0},{"bsn":"60001","snA":"47660","sn":"550494","btitle":["\u96fb\u8996\u904a\u6a02\u5668\u7d9c\u5408\u8a0e\u8ad6\u5340"],"title":"\u3010\u8a0e\u8ad6\u3011\u71b1\u9a30\u9a30\u7684 XBOX Serie X \u5165\u624b","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/7b81dd5c1f24817d10825a2661d42cea.JPG","ts":1605005594,"gp":"51","comment":39},{"bsn":"36833","snA":"16341","sn":"67625","btitle":["\u528d\u8207\u9060\u5f81"],"title":"\u3010\u8a0e\u8ad6\u3011\u5171\u540c\u8a0e\u8ad6\u5982\u4f55\u6539\u5584\u5718\u968a\u9060\u5f81\u7522\u751f\u7684\u5f0a\u75c5","pic":"","ts":1604996648,"gp":"128","comment":26},{"bsn":"47377","snA":"139","sn":"283","btitle":["\u9b54\u5973\u4e4b\u65c5"],"title":"\u3010\u5206\u4eab\u3011\u7fbd\u9ce5\u98af\u68a7 \u6240\u756b\u7684\u300a\u9b54\u5973\u4e4b\u65c5\u300b\u4f0a\u857e\u5a1c","pic":"https:\/\/pbs.twimg.com\/media\/EmTB8TiUYAEX2Ls.jpg","ts":1604917611,"gp":"149","comment":0},{"bsn":"29919","snA":"7888","sn":"25989","btitle":["Dead by Daylight\uff08\u9ece\u660e\u6b7b\u7dda\uff09"],"title":"\u3010\u60c5\u5831\u30114.4.0 \u65b0\u89d2\u8272\u300c\u9023\u9ad4\u5b30\u300d\u6280\u80fd\u53ca\u80fd\u529b\u8a73\u60c5","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/422660a9bea7b12cc38043360346d4dd.JPG","ts":1605024941,"gp":"96","comment":53},{"bsn":"46197","snA":"1682","sn":"6343","btitle":["\u8d64\u5742\u30a2\u30ab \u4f5c\u54c1\u96c6\uff08\u8f1d\u591c\u59ec\u60f3\u8b93\u4eba\u544a\u767d\uff09 "],"title":"\u3010\u5206\u4eab\u3011kachobi \u6240\u756b\u7684\u300a\u8f1d\u591c\u59ec\u60f3\u8b93\u4eba\u544a\u767d\u300b\u51ac\u88dd\u4f0a\u4e95\u91ce\u5fa1\u5b50","pic":"https:\/\/i.imgur.com\/xHw1474.jpg","ts":1604898707,"gp":"118","comment":0},{"bsn":"26742","snA":"69817","sn":"569229","btitle":["Fate\/Grand Order"],"title":"\u3010\u7e6a\u3011\u9072\u4f86\u7684\u842c\u8056\u7bc0 \u8272\u60c5\u8304\u5b50","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/f6b0371f16f2b4bcf4f16e2e950fc248.JPG","ts":1604927952,"gp":"156","comment":0},{"bsn":"34880","snA":"8484","sn":"47357","btitle":["\u7b2c\u4e03\u53f2\u8a69 "],"title":"\u3010\u60c5\u5831\u30112020\/11\/11\uff08\u4e09\uff09\u76f4\u64ad\u532f\u7e3d \u65b0\u82f1\u96c4\u8299\u862d \u77ed\u528d\u5e2d\u5361\u6210\u5c31 RTA\u7d50\u675f\u65e5\u671f \u795e\u79d8 \u786c\u5e63\u5546\u5e97\u8f2a\u8f49","pic":"https:\/\/i1.ytimg.com\/vi\/dja-cFiJCY4\/hqdefault.jpg","ts":1605003188,"gp":"24","comment":45},{"bsn":"60646","snA":"150","sn":"431","btitle":["Xbox  \/ Xbox Series X "],"title":"\u3010\u8a0e\u8ad6\u3011\u8cb7\u7dad\u4eac\u7d00\u5143\u767c\u751f\u7684\u5c0d\u8a71\uff0c\u5e0c\u671b\u5fae\u8edf\u52a0\u6cb9","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/d643678b64adca2201f6d35257cb4a0e.JPG","ts":1604999318,"gp":"29","comment":45},{"bsn":"35284","snA":"2344","sn":"8619","btitle":["\u5fa9\u6d3b\u90aa\u795e Re ; universe"],"title":"\u3010\u5fc3\u5f97\u3011\u6700\u65b0\u7d00\u5ff5\u9650\u5b9a\u8f49\u86cb\u62bd\u5361\u96c6\u4e2d\u4e32 SS\u6eab\u8482\u59ae2.0 SS\u5c0f\u706b\u8eca2.0 SS\u838e\u62c9 SS\u827e\u84ee \u6700\u65b0Romancing\u796d\u767b\u5834!!","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/2588d9fb6a4d719faadfc4ccb67c616b.JPG","ts":1605059171,"gp":"3","comment":47},{"bsn":"17532","snA":"668492","sn":"5709577","btitle":["\u82f1\u96c4\u806f\u76df League of Legends "],"title":"\u3010\u60c5\u5831\u301110.24 \u65b0\u9020\u578b\u9810\u89bd\uff1a\u661f\u6cb3\u7cfb\u5217\u3001\u5e7d\u51a5\u9e97\u73ca\u5353\u3001\u5c0a\u7235\u661f\u5149\u7d22\u62c9\u5361","pic":"https:\/\/i1.ytimg.com\/vi\/4lIxzH9CZmA\/hqdefault.jpg","ts":1605031256,"gp":"28","comment":40},{"bsn":"60596","snA":"51679","sn":"282127","btitle":["PS4 \/ PlayStation4 "],"title":"\u3010\u60c5\u5831\u3011Store@PSN \u96d911\u5feb\u9583\u7279\u8ce3, 11\/10 ~ 11\/12 \u53ea\u6709\u4e09\u5929","pic":"","ts":1604983996,"gp":"105","comment":31},{"bsn":"30861","snA":"27556","sn":"302857","btitle":["\u8d85\u7570\u57df\u516c\u4e3b\u9023\u7d50\u2606Re:Dive "],"title":"\u3010\u5fc3\u5f97\u3011\u9732\u5a1c\u4e4b\u5854 430\/430ex\/\u63a2\u7d22\u8ff4\u5eca-\u914d\u968a\u5206\u4eab \u96c6\u4e2d\u4e32","pic":"https:\/\/i1.ytimg.com\/vi\/by3auZ9bJIA\/hqdefault.jpg","ts":1604997157,"gp":"109","comment":6},{"bsn":"30861","snA":"27547","sn":"302731","btitle":["\u8d85\u7570\u57df\u516c\u4e3b\u9023\u7d50\u2606Re:Dive "],"title":"\u3010\u5fc3\u5f97\u3011\u9019\u628a\u69cd\u6bd4\u528d\u597d\u7528~~\u30e4\u30d0\u30a4\u3067\u3059\u306d\uff01\u2606","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/3e2a634a44700a2307f4acefbe723bbf.JPG","ts":1604933684,"gp":"182","comment":4},{"bsn":"25204","snA":"11051","sn":"94176","btitle":["\u78a7\u85cd\u5e7b\u60f3"],"title":"\u3010\u7e6a\u5716\u3011\u89d2\u65cf\u6cf3\u88dd","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/750c68d06709617da4185207d8f20938.JPG","ts":1604920564,"gp":"160","comment":1},{"bsn":"9703","snA":"31389","sn":"94586","btitle":["\u523a\u5ba2\u6559\u689d"],"title":"\u3010\u5fc3\u5f97\u3011\u9019\u4ee3\u4e5f\u592a\u591a\u4ee4\u904a\u6232\u4e0d\u80fd\u904a\u73a9\u7684bug\u4e86\u5427","pic":"","ts":1605027598,"gp":"25","comment":42},{"bsn":"36730","snA":"6333","sn":"36899","btitle":["\u539f\u795e"],"title":"\u3010\u7e6a\u5716\u3011\u83f2\u8b1d\u723e","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/dd7365bc0044b36784f6c83531bed65b.JPG","ts":1605006084,"gp":"131","comment":0},{"bsn":"33651","snA":"4150","sn":"40234","btitle":["\u660e\u65e5\u65b9\u821f "],"title":"\u3010\u653b\u7565\u3011\u65e5\u670d\u5371\u6a5f\u5408\u7d04\u300c\u9ec3\u9435\u884c\u52d5\u300d\u5c0f\u63d0\u9192 (\u65b0\u7d04\u4f4e\u4fdd\u73a9\u5bb6\u9700\u77e5","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/5b5e2e0aeb78d7cbad757de7cc05d262.JPG","ts":1604933138,"gp":"93","comment":1},{"bsn":"23805","snA":"647396","sn":"3813908","btitle":["\u795e\u9b54\u4e4b\u5854"],"title":"\u3010\u5fc3\u5f97\u3011\u5c01\u738b\u5fa9\u4ec7\u8a18 1\uff5e8\u5c01\u738b\u6311\u62301\uff5e8\u5c01\u5922\u9b58","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/0deac3fec78e960ca714eb46df4772ad.JPG","ts":1605012816,"gp":"74","comment":7},{"bsn":"60596","snA":"51670","sn":"282093","btitle":["PS4 \/ PlayStation4 "],"title":"\u3010\u60c5\u5831\u3011\u7dad\u4eac\u7d00\u5143 \u6cb3\u87f9\u7248\u8207\u6b63\u5e38\u6bd4\u8f03","pic":"https:\/\/i1.ytimg.com\/vi\/bpU79mhrElE\/hqdefault.jpg","ts":1604944846,"gp":"44","comment":40},{"bsn":"60596","snA":"51672","sn":"282101","btitle":["PS4 \/ PlayStation4 "],"title":"\u3010\u5fc3\u5f97\u3011\u8acb\u4ee5\u5be6\u969b\u884c\u52d5\u7f77\u8cb7\u7dad\u4eac\u7d00\u5143","pic":"","ts":1604964506,"gp":"142","comment":20},{"bsn":"60030","snA":"563495","sn":"2285217","btitle":["\u96fb\u8166\u61c9\u7528\u7d9c\u5408\u8a0e\u8ad6 "],"title":"\u3010\u60c5\u5831\u301111\/11 11\u9ede PCHOME\u53ef\u6436\u516c\u72483070 3090","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/3034e1b7a1237ecfaa416997b854b6e6.JPG","ts":1605005578,"gp":"5","comment":38},{"bsn":"21911","snA":"6212","sn":"25522","btitle":["\u9748\u9b42\u884c\u8005"],"title":"\u3010\u5bc6\u6280\u3011\u54c8\u9732\u6211\u59b9\u59b9","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/2dcc1200136bddbb55194f81d0dee056.JPG","ts":1604929803,"gp":"87","comment":1},{"bsn":"60645","snA":"483","sn":"2341","btitle":["PS5 \/ PlayStation5 "],"title":"\u3010\u9592\u804a\u3011\u7dad\u4eac\u7d00\u5143\u9084\u6c92\u9000\u7684\u5feb\u9000\u5594\u3002","pic":"https:\/\/i1.ytimg.com\/vi\/ex9Wc5jBHGY\/hqdefault.jpg","ts":1604987732,"gp":"32","comment":29},{"bsn":"1054","snA":"15342","sn":"53528","btitle":["\u8679\u5f69\u516d\u865f \u7cfb\u5217"],"title":"\u3010\u5fc3\u5f97\u3011\u65b0\u89d2ARUNI\u6c92\u6709\u5f88\u5f37 TACHANKA\u79fb\u9664\u596e\u8d77\u662f\u591a\u9918","pic":"","ts":1605037608,"gp":"9","comment":38},{"bsn":"25052","snA":"83513","sn":"515848","btitle":["\u602a\u7269\u5f48\u73e0"],"title":"\u3010\u60c5\u5831\u3011\u521d\u6b2110\u9023\u8f49\u86cb\u30fb\u26055\u6216\u4ee5\u4e0a\u89d2\u8272\u51fa\u73fe\u6a5f\u738724%\uff01\u3000\u300c\u6fc0\u7378\u795e\u796d\u300d11\u670811\u65e5\u9023\u74b0\u51fa\u64ca","pic":"https:\/\/www.monster-strike.com.tw\/entryimages\/jujhb33_TVb_yyy_Ty77855_tj33_C%243v.png","ts":1605002687,"gp":"14","comment":39},{"bsn":"60053","snA":"57044","sn":"244356","btitle":["\u6a21\u578b\u6280\u8853\u8207\u8cc7\u8a0a"],"title":"\u3010\u4f5c\u54c1\u5206\u4eab\u3011  RG SAZABI MADWORKS\u6539\u5957","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/cd51261ed9e54868d2ee27da26043f7f.JPG","ts":1604994913,"gp":"172","comment":0},{"bsn":"7650","snA":"1011589","sn":"6330896","btitle":["\u65b0\u6953\u4e4b\u8c37 "],"title":"\u3010\u9592\u804a\u3011\u84bc\u9f8d\u4fe0\u5ba2\u5373\u5c07\u8d70\u5165\u6b77\u53f2\uff1f\u71215\u8f494\u6280\uff01\u66ab\u505c\u958b\u767c","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/0a5519344b405686ed5bb0fda435ea9a.JPG","ts":1605049516,"gp":"49","comment":34},{"bsn":"38113","snA":"7019","sn":"31860","btitle":["\u5b88\u671b\u50b3\u8aaa Guardian Tales "],"title":"\u3010\u9592\u804a\u3011\u91cd\u63a8\u4e3b\u7dda\u7684\u5c0f\u611f","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/5e208260872d774df625ab58610a31bd.JPG","ts":1605050436,"gp":"16","comment":35},{"bsn":"21911","snA":"6209","sn":"25502","btitle":["\u9748\u9b42\u884c\u8005"],"title":"\u3010\u7e6a\u5716\u3011\u53f2\u9edb\u62c9 \u5154\u5973\u90ce","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/a7a686fb19e95fb41731ee9c9eb2fa20.JPG","ts":1604906433,"gp":"173","comment":0},{"bsn":"18966","snA":"126261","sn":"540447","btitle":["\u6d41\u4ea1\u9eef\u9053 Path of Exile"],"title":"\u3010\u8a0e\u8ad6\u3011\u85cf\u8eab\u8655 - \u9b3c\u6ec5\u4e4b\u5203 - \u7317\u7aa9\u5ea7  (\u9b3c\u6ec5\u7cfb\u5217\u8f09\u9ede)","pic":"https:\/\/truth.bahamut.com.tw\/s01\/202011\/9a3c42970850f25049b75b9051e37ab2.JPG","ts":1605023633,"gp":"102","comment":18}];
$.each( forum, function( fk, fv) {
var _tmp = {
'pic': fv.pic,
'sn': '//forum.gamer.com.tw/C.php?bsn='+fv.bsn+'&snA='+fv.snA,
'title': fv.title,
'btitle': fv.btitle,
'ts': fv.ts,
'area': '哈啦區',
'gp': fv.gp,
'comment': fv.comment
};

_setData(_tmp);
});
forum = '';

_fresh.sort(_sortData);
_old.sort(_sortData);
_oldest.sort(_sortData);

var _row = 30,
_page = 1;

var _tp = Math.ceil((_fresh.length + _old.length) / _row);
$.each(_old, function(ko, vo) {
_old[ko].page = vo.ts15 % _tp;
});

_old.sort(function(a, b){
if( a.page == b.page ) {
return parseFloat(b.ts) - parseFloat(a.ts);
}

return a.page - b.page;
});

var _data = _fresh.concat(_old, _oldest),
fAry = ['A','B','C'];

var river = {
show: function() {
var s = (_page-1)*_row;
var d = _data.slice(s, (_row*_page));

var html = '';

$.each(d, function(dk, dv) {
html = '';
html += '<figure>';

if( dv.pic ) {
if (/https?:\/\/(?:truth|p2)\.bahamut\.com\.tw\//.test(dv.pic)) {
dv.pic = dv.pic + '?w=300';
}
html += '  <div class="img_box"><a href="'+dv.sn+'" data-gtmgamer="'+dv.area+'" target="_blank"><img src="https://i2.bahamut.com.tw/acg/noimg120.jpg" data-src="'+dv.pic+'" class="lazyload"></a></div>';
}

html += '  <figcaption>';
html += '    <a href="'+dv.sn+'" data-gtmgamer="'+dv.area+'" target="_blank">'+dv.title+'</a>';

html += '    <p>';

if (dv.btitle) {
html += '<span>'+dv.btitle+'</span>';
}

html += '      <i class="fa fa-thumbs-o-up"></i> '+dv.gp+'　<i class="fa fa-comments"></i> '+dv.comment+'</p>';
html += '  </figcaption>';
html += '</figure>';

$('#r'+fAry[_getColNo()]).append(html);
});

_page = _page + 1;

if (0 >= d.length) {
$(window).off('scroll', river.scrollHandler);
}
},
scrollHandler: function() {
var _bbottom = $("body").height() - $(window).height() - 670;
var _rAh = $("#rA").height(),
_rBh = $("#rB").height(),
_rCh = $("#rC").height();

if ($(window).scrollTop() >= _bbottom) {
river.show();
} else {
if (_rAh && _rBh && _rCh) {
var _rAB = Math.abs(_rAh - _rBh),
_rAC = Math.abs(_rAh - _rCh),
_rBC = Math.abs(_rBh - _rCh),
_least = 750;

if (_rAB > _least || _rAC > _least || _rBC > _least) {
river.show();
}
}
}
}
};

window.WATERFALL = river;
})(window, jQuery);
</script>

<div id="rivers">
<div id="rA" style="float:left;width:240px;margin-left:1px;"></div>
<div id="rC" style="float:right;width:240px;margin-right:1px;"></div>
<div id="rB" style="float:left;width:240px;margin:0 14px 0 14px;"></div>
</div>

<script>
jQuery(function() {
jQuery(window).scroll(WATERFALL.scrollHandler);
});
</script>
</div><!--中間內容結束-->

<div class="BA-right" id="flyRightBox"><!--右側內容(廣告區)-->
<script>
(function(window, $, undefined) {
'use strict';

var _ad = {"unfixed":{"a0":["<a href=\"https:\/\/lihi1.cc\/mJtH1\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209880\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/23\/1ef8c135a60d4dacd271c19b5d1aibn5.PNG\"><\/a>","<a href=\"https:\/\/lihi1.cc\/Ugfqt\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"210113\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/62\/8285aea89c143337ca3c5829cd1aiia5.JPG\"><\/a>","<a href=\"https:\/\/lihi1.cc\/76sm8\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"210114\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/64\/789502650ea993c38645b54b681aiic5.JPG\"><\/a>"],"a1":["<a href=\"https:\/\/ad.doubleclick.net\/ddm\/trackclk\/N1313774.1921973GAMERTW\/B24943959.286871165;dc_trk_aid=480609151;dc_trk_cid=140423148;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;tfua=;gdpr=${GDPR};gdpr_consent=${GDPR_CONSENT_755}\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"210023\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/51\/6ae99d82fa5c50068482753f011aghr5.JPG\"><\/a>"],"a2":["<a href=\"https:\/\/prj.gamer.com.tw\/2020\/xbox\/\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"208844\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/29\/935102d9316c0146bb231f53d21aik55.PNG\"><\/a>"],"a3":["<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=15281\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209658\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/54\/77b8510ebd0dd4f594eea09a8d18an25.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18420\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209659\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/95\/7fbdb815bc3a798388531269e91a4kf5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18421\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209660\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/96\/91e76bfd35978a81fb20b9012a1a4kg5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18422\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209661\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/98\/87e3e80cf292748bfd014158351a4ki5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18430\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209662\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/99\/556e3926d149a3847336dffb331a4kj5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18428\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209663\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/00\/b7e6046d587bba19292562232d1a4kk5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18370\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209664\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/01\/2d38b79291234e4d0ef6cdd3ce1a4kl5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18427\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209665\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/02\/1a267ebe8e94695ebdab402bad1a4km5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18484\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209667\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/04\/e97b0536b4d3de495e7b45e4c91a4ko5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18432\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209668\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/05\/a333f63caf910edebe32be69011a4kp5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18433\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209669\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/06\/0ef52cd2c0db235d094efb76e41a4kq5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18434\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209670\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/07\/04c2c9fb6a942b064e06d1de5d1a4kr5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18435\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209671\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/08\/797e4d7f2fd2d7b6e3fc3953831a4ks5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18436\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209672\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/09\/ed741d4e90067a77f292499f371a4kt5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18438\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209673\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/10\/e9eaefae8a35eacadf64efd44d1a4ku5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18439\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209674\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/11\/085c8598a0bd9286075111ef901a4kv5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18487\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209675\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/12\/45518809a8b4c930fe780a14591a4kw5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18525\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209676\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/13\/c78dc1e24bd3cdba26cffcdc181a4kx5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18215\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209677\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/90\/63b609531dfc1e89b8f5d5233f1a5c25.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18431\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209678\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/08\/df11cfe787291d727813773fe91a5ck5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18523\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209679\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/10\/efa25645dd5a647017d1ba596c1a5cm5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18535\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209680\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/12\/53c5402c386cd7e076a1e760091a5co5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18539\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209681\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/15\/cecaad8a1b8ace49cb3746974d1a5cr5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18437\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209682\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/95\/89ee741b2be98834691e633a0a1a6yj5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18552\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209683\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/96\/5f802e008e7ccbd4b4189489ad1a6yk5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18550\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209684\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/97\/22cf0f48ae760f890a1770fac21a6yl5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18541\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209685\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/98\/bcb7c198147bc792bfbe7fdade1a6ym5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18369\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209686\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/01\/5cd85f8908e552d1d082a8dd271a81l5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18491\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209687\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/03\/2eb2e3e99ce271f52ab0cd344c1a81n5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18559\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209688\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/05\/097800f82a73ecf5b9a96a1fa91a81p5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18612\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209689\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/07\/a3a6186cfbc383efbbd52cf7301a81r5.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18622\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209690\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/17\/d4fb7a7877b242f3c33301edcf1a8215.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18557\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209691\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/21\/1eff37f33742a2e51e6f0c2edb1a8255.JPG\"><\/a>","<a href=\"https:\/\/ani.gamer.com.tw\/animeVideo.php?sn=18429\" target=\"_blank\" class=\"a-mercy-d\" data-mercy=\"\" data-si=\"\" data-ssn=\"209666\"><img border=0 src=\"https:\/\/p2.bahamut.com.tw\/S\/2KU\/49\/eab11d2fd777feedc077c370311afy95.JPG\"><\/a>"]}};
if ($(_ad.fixed).length) {
$.each(_ad.fixed, function(k, val) {
if (val.length == 1) {
document.write('<div>' + val + '<br><img src="https://i2.bahamut.com.tw/spacer.gif" width="1" height="6"></div>');
} else {
document.write('<div>' + val[AdBaha.ck('FA', k, val.length)] + '<br><img src="https://i2.bahamut.com.tw/spacer.gif" width="1" height="6"></div>');
}
});
}

if ($(_ad.unfixed).length) {
_ad.unfixed = AdBaha.Shuffle(_ad.unfixed);
$.each(_ad.unfixed, function(k, val) {
if (val.length == 1) {
document.write('<div>' + val + '<br><img src="https://i2.bahamut.com.tw/spacer.gif" width="1" height="6"></div>');
} else {
document.write('<div>' + val[AdBaha.ck('FA', k, val.length)] + '<br><img src="https://i2.bahamut.com.tw/spacer.gif" width="1" height="6"></div>');
}
});
}

_ad = [];
if (_ad.length) {
document.write('<div>' + AdBaha.output(_ad, 13) + '<br><img src="https://i2.bahamut.com.tw/spacer.gif" width="1" height="6"></div>');
}
})(window, jQuery);
</script>
<div id="fuli-ad">
<h1 class="BA-ltitle">勇者福利社</h1>
<div class="BA-rbox1">
<a class="fuli-item-a" href="https://fuli.gamer.com.tw/cnt.php?sn=5287" target="_blank" data-gtm="福利社宣傳" style="">
<img src="https://p2.bahamut.com.tw/B/2KU/63/d81dcee922fdd8229d61e926461ainv5.PNG">
<p class="fuli-title">
<span>立即參與問答大賽贏得次世代主機</span>
</p>
</a>
<a class="fuli-item-b fuli-item-priority-1" href="https://fuli.gamer.com.tw/cnt.php?sn=5285" target="_blank" data-gtm="福利社一般宣傳" style="display: none;">
<img src="https://p2.bahamut.com.tw/USER/ACTIVE/active_pic5285.JPG">
<p class="fuli-title">
<span class="tag-type">勇者任務</span><span>決定《最強蝸牛》FB小編命運 iPhone12 等豪華禮品等你拿</span>
</p>
</a>
<a class="fuli-item-b fuli-item-priority-2" href="https://fuli.gamer.com.tw/shop_detail.php?sn=1557" target="_blank" data-gtm="福利社一般宣傳" style="display: none;">
<img src="https://p2.bahamut.com.tw/B/2KU/19/4b323c6f3c9b84fbc4db75278e1agjn5.JPG">
<p class="fuli-title">
<span class="tag-type">抽抽樂</span><span>設備再升級 - DIGIFAST 雙 11 必殺加倍送！</span>
</p>
</a>
<a class="fuli-item-b fuli-item-priority-2" href="https://fuli.gamer.com.tw/shop_detail.php?sn=1552" target="_blank" data-gtm="福利社一般宣傳" style="display: none;">
<img src="https://p2.bahamut.com.tw/B/2KU/72/cd5e64b23867a32c4250baeaff1agcs5.JPG">
<p class="fuli-title">
<span class="tag-type">抽抽樂</span><span>《巴哈姆特 24 週年》天天巴幣 100 倍抽</span>
</p>
</a>
<a class="fuli-item-b fuli-item-priority-2" href="https://fuli.gamer.com.tw/shop_detail.php?sn=1551" target="_blank" data-gtm="福利社一般宣傳" style="display: none;">
<img src="https://p2.bahamut.com.tw/B/2KU/43/f58ce2f73088e2bd8d5f2036391ager5.JPG">
<p class="fuli-title">
<span class="tag-type">抽抽樂</span><span>《巴哈姆特 24 週年》週週巴幣抽主機（抽獎品項：動森Switch特仕機）</span>
</p>
</a>
<a class="fuli-item-b fuli-item-priority-2" href="https://fuli.gamer.com.tw/shop_detail.php?sn=1550" target="_blank" data-gtm="福利社一般宣傳" style="display: none;">
<img src="https://p2.bahamut.com.tw/B/2KU/41/b9411c5fc33223b2b6046bfb081agep5.JPG">
<p class="fuli-title">
<span class="tag-type">抽抽樂</span><span>《巴哈姆特 24 週年》站長摸彩大放送</span>
</p>
</a>
<a class="fuli-item-b fuli-item-priority-2" href="https://fuli.gamer.com.tw/shop_detail.php?sn=1549" target="_blank" data-gtm="福利社一般宣傳" style="display: none;">
<img src="https://p2.bahamut.com.tw/B/2KU/18/5aa2282f9a823490dccb2c7a631age25.JPG">
<p class="fuli-title">
<span class="tag-type">抽抽樂</span><span>《巴哈姆特 24 週年》勇者週邊抽抽樂</span>
</p>
</a>
<a class="fuli-item-b fuli-item-priority-2" href="https://fuli.gamer.com.tw/shop_detail.php?sn=1548" target="_blank" data-gtm="福利社一般宣傳" style="display: none;">
<img src="https://p2.bahamut.com.tw/B/2KU/94/3f96789113a67d7ec5377bfdac1agde5.JPG">
<p class="fuli-title">
<span class="tag-type">抽抽樂</span><span>《巴哈姆特歷屆週邊》福袋抽抽樂</span>
</p>
</a>
<a class="fuli-item-b fuli-item-priority-2" href="https://fuli.gamer.com.tw/shop_detail.php?sn=1558" target="_blank" data-gtm="福利社一般宣傳" style="display: none;">
<img src="https://p2.bahamut.com.tw/B/2KU/66/7a8d499269db48fdd3cffe371d1agqi5.JPG">
<p class="fuli-title">
<span class="tag-type">抽抽樂</span><span>Assassin’s Creed Valhalla by Reebok 傳奇降臨 維京幻想啟發街頭新潮流</span>
</p>
</a>
<a class="fuli-item-b fuli-item-priority-2" href="https://fuli.gamer.com.tw/shop_detail.php?sn=1539" target="_blank" data-gtm="福利社一般宣傳" style="display: none;">
<img src="https://p2.bahamut.com.tw/B/2KU/53/bdd2415b80df9b0d9da275a68f1adhh5.JPG">
<p class="fuli-title">
<span class="tag-type">抽抽樂</span><span>《Anker Soundcore》「自由音頻，解放聲線」真無線藍牙耳機｜WitsPer智選家</span>
</p>
</a>
</div>
</div>
<script>
(function (window, $, undefined) {
'use strict';
var box = $('#fuli-ad .BA-rbox1');
var obj1 = $('.fuli-item-priority-1');
var obj2 = $('.fuli-item-priority-2');
box.find('.fuli-item-b').remove();

shuffle(obj1);
shuffle(obj2);

obj1.each(function () {
if (box.find('a').length >= 5) {
return false;
}

box.append($(this));
});

obj2.each(function () {
if (box.find('a').length >= 5) {
return false;
}

box.append($(this));
});

$('.fuli-item-b').show();

function shuffle(array) {
for (var i = array.length - 1; i > 0 ; i--) {
var j = Math.floor(Math.random() * (i+1));
[array[i], array[j]] = [array[j], array[i]];
}
}
})(window, jQuery);
</script>
</div><!--右側內容(廣告區) 結束-->
</div><!--中間主內容外包裝結束-->
</div>
<div style="position: fixed; left: 20px; right: 20px; bottom: 20px; z-index: 99;">
<div style="position: relative; padding: 15px 30px 15px 15px; border: 1px solid transparent; border-radius: 3px; line-height: 1.5; background: #FFFFDB; border-color: #E1D3B8; color: #705930; max-width: 1250px; margin-auto; display: none;">
<a href="javascript:;" class="alert-close oldbrowser-close" style="position: absolute; top: 5px; right: 5px; padding: 3px; border: 1px solid transparent; border-radius: 2px; color: rgba(102, 102, 102, 0.6);"><i class="material-icons" style="font-size: 20px;">close</i></a>
<p style="color: #705930;"><i class="material-icons" style="vertical-align: bottom; font-size: 22px; margin-right: 5px;">face</i>基於日前微軟官方表示 Internet Explorer 不再支援新的網路標準，可能無法使用新的應用程式來呈現網站內容，在瀏覽器支援度及網站安全性的雙重考量下，為了讓巴友們有更好的使用體驗，巴哈姆特即將於 2019年9月2日 停止支援 Internet Explorer 瀏覽器的頁面呈現和功能。<br>屆時建議您使用下述瀏覽器來瀏覽巴哈姆特：<br><a href="https://www.google.com/intl/zh-TW_ALL/chrome/" style="color: #0055aa; text-decoration: none;" target="_blank">。Google Chrome（推薦）</a><br><a href="https://www.mozilla.org/zh-TW/firefox/new/" style="color: #0055aa; text-decoration: none;" target="_blank">。Mozilla Firefox</a><br><a href="https://www.microsoft.com/zh-tw/windows/microsoft-edge" style="color: #0055aa; text-decoration: none;" target="_blank">。Microsoft Edge（Windows10以上的作業系統版本才可使用）</a></p>
</div>
</div>
<script>
var mercyoldbrowser = (function () {
var oldbrowser = {};

oldbrowser.show = function () {
jQuery('.oldbrowser-close').parent().show();
};

oldbrowser.hide = function () {
jQuery('.oldbrowser-close').parent().hide();
};

oldbrowser.check = function () {
if (localStorage.getItem('oldbrowsermercy') === null) {
jQuery('.oldbrowser-close').parent().show();
} else {
var hourts = Math.floor(Date.now() / 1000)-86400;
if (localStorage.getItem('oldbrowsermercy') < hourts) {
jQuery('.oldbrowser-close').parent().show();
localStorage.removeItem('oldbrowsermercy');
}
}
};

return oldbrowser;
} ());

jQuery(".oldbrowser-close").click(function () {
mercyoldbrowser.hide();

if (typeof(Storage) !== "undefined") {
localStorage.setItem('oldbrowsermercy', Math.floor(Date.now() / 1000));
}
});

var isIEFlag=window.ActiveXObject || "ActiveXObject" in window;
jQuery(function () {
if (isIEFlag) {
if (typeof(Storage) === "undefined") {
mercyoldbrowser.show();
} else {
mercyoldbrowser.check();
}
}
});
</script>
<script>
var allobjs = document.querySelectorAll("object");
for (var i = 0, l = allobjs.length; i < l; i++) {
allobjs[i].style.visibility="hidden";
allobjs[i].style.visibility="visible";
}

buttonToMobile();

/****右邊固定****/
(function (window, $, undefined) {
var fBoxPosX = 0;
var fBoxPosY = 0;
var rightBox = $('#flyRightBox');
var fuliAdBox = $('#fuli-ad');

var ad = {
fixedFuliBoxTop: function () {
var scrollbarTop = $(window).scrollTop();
var scrollbarLeft = $(window).scrollLeft();
var contxClientWidth = $(window).width();
var contxClientHeight = $(window).height();
var fBoxHeight = rightBox.height() + fuliAdBox.height();

var fBoxRemnants = fuliAdBox.height() - rightBox.height();

if (scrollbarTop > ($('.BA-center').offset().top - fBoxRemnants) && fBoxRemnants <= 0) {
rightBox.css('position', 'fixed');
rightBox.css('top', fBoxRemnants + 15 + 'px');

if (scrollbarLeft !== 0) {
rightBox.css('left', fBoxPosX - scrollbarLeft + 'px');
} else {
rightBox.css('left', fBoxPosX + 'px');
}
} else if (scrollbarTop > fBoxPosY && fBoxRemnants > 0) {
rightBox.css('position', 'fixed');
rightBox.css('top', 0);

if (scrollbarLeft !== 0) {
rightBox.css('left', fBoxPosX - scrollbarLeft + 'px');
} else {
rightBox.css('left', fBoxPosX + 'px');
}
} else {
rightBox.css('position', '');
}
},

boxResize: function () {
rightBox.css('position', '');
fBoxPosX = rightBox.offset().left;
ad.fixedFuliBoxTop();
}
};

if ($('#BA-center-id').height() > rightBox.height()) {
fBoxPosX = rightBox.offset().left;
fBoxPosY = rightBox.offset().top;

$(window).on('scroll', ad.fixedFuliBoxTop);
$(window).on('resize', ad.boxResize);
}

ad.fixedFuliBoxTop();
}) (window, jQuery);
/****右邊固定****/

jQuery(".a-mercy-d").click(function() {
var url = this.href;
var goRedirect = false;
var openNew = (this.target) ? true : false;

if( url ) {
jQuery.get( "/ajax/baha_lkasoo.php", { id: jQuery(this).data('ssn') } )
.done(function() {
goRedirect = true;
if (!openNew) {
document.location = url;
}
});

setTimeout(function() {
if (!goRedirect) {
if (!openNew) {
document.location = url;
}
}
}, 1500);
}

if( !openNew ){
return false;
}
});

var mercyadblock = (function () {
var adblock = {};

adblock.show = function () {
jQuery('.adb-close').parent().show();
};

adblock.hide = function () {
jQuery('.adb-close').parent().hide();
};

adblock.check = function () {
if (localStorage.getItem('admercyblocks') === null) {
jQuery('.adb-close').parent().show();
} else {
var hourts = Math.floor(Date.now() / 1000)-86400;
if (localStorage.getItem('admercyblocks') < hourts) {
jQuery('.adb-close').parent().show();
localStorage.removeItem('admercyblocks');
}
}
};

return adblock;
} ());
</script>
</body>
</html>
