<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Product::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function game()
    {
        $time = '今年';
        return view('gamer2', compact('time'));
        // return view('gamer', ['time' => '今年絕對']);
    }
    function foreach () {
        $items = ['google', 'yahoo', 'facebook'];
        $foods = [];
        return view('foreach', compact('items', 'foods'));
    }
    public function test1()
    {
        return view('test.test1');
        //test;
    }
    public function addinfo()
    {
        //用物件屬性
        // $Product = new Product;
        // $Product->title = 'title1';
        // $Product->price = '1000';
        // $Product->desc = '123456';
        // $Product->enabled = '1';
        // $Product->sell_at = '2020-11-20';
        // $Product->cgy_id = '3';
        // $Product->save();

        //用建構子
        // $Product = new Product([
        //     'title' => 'title_2',
        //     'price' => '3500',
        //     'desc' => '6547989',
        //     'enabled' => '0',
        //     'sell_at' => '20201121',
        //     'cgy_id' => '1',
        // ]);
        // $Product->save();

        //把資料存成陣列 然後CREATE
        $data = [
            'title' => 'title_3',
            'price' => '4500',
            'desc' => '6547989',
            'enabled' => '0',
            'sell_at' => '20201121',
            'cgy_id' => '1',
        ];
        $Product = Product::create($data);

    }
    protected function find()
    {
        // $item = Product::find(2);
        // $item = Product::findOrFail(A);
        $item = Product::where('price', '>', '2000')->first();
        $item = Product::get();
        $item = Product::all();
        $item = Product::where('enabled', true)->orderBy('created_at', 'desc')->take(3)->get();
        $item = Product::where('id', '>', '499')->whereBetween('price', [1900, 3600])->get();
        print_r($item);
    }
    public function findOne(Product $product)
    {
        // $product = Product::where('price', '>', '2000')->first();
        //使用Route Model Binding不須再查詢
        dd($product);
    }
    public function findcgy(Product $product)
    {
        //更改產品的cgy_id

        //寫法一
        // $product->cgy_id = 1;
        // $product->save();
        // dd($product);

        //寫法2
        // $cgy = \App\Models\Cgy::find(2);
        // // dd($cgy);
        // $product->cgy()->associate($cgy);
        // $product->save();
        // dd($product);

        $cgy = $product->cgy;
        dd($cgy);

    }

    public function findproduct(\App\Models\Cgy $cgy)
    {
        // $cgy = \App\Models\Cgy::find($id);
        $product = $cgy->products;
        //增加查詢使用products()
        $product = $cgy->products()->where('price', '>', '5000')->take(2)->get();
        dd($product);
    }

    public function addproduct(\App\Models\Product $product)
    {
        $order = \App\Models\Order::find(1);
        //透過save跟attach指派產品給order
        // $order->products()->save($product);
        $order->products()->attach($product, ['qty' => 5, 'comment' => '包裝禮物']);
        dd($order);
    }

    public function order(\App\Models\Order $order)
    {
        $products = $order->products;
        // dd($products);
        return view('list', compact('products'));
    }
    public function remote(\App\Models\Product $product)
    {
        $order = \App\Models\Order::find(1);
        // dd($product->id);
        $order->products()->detach($product->id);
    }
    public function sync(\App\Models\Order $order)
    {
        $order->products()->sync([1, 3, 5]);
    }

    public function withproduct()
    {
        $cgys = App\Models\Cgy::with('product')->get();
    }

    public function hasorder()
    {
        $products = \App\Models\Product::has('orders')->get();
        $products = \App\Models\Product::has('orders', '>', 5)->get();

        dd($products);
    }
}
