<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'price', 'desc', 'enabled', 'sell_at', 'cgy_id'];

    public function cgy()
    {
        return $this->belongsTo(\App\Models\Cgy::class);
    }
    //設定touches屬性可在⼦紀錄變更時⼀併更新⽗紀錄的updated_at
    protected $touches = ['cgys'];

    public function orders()
    {
        return $this->belongsToMany('App\Models\Order');
        //兩種寫法都可以
        // return $this->belongsToMany(App\Models\Order::class);

    }
}
