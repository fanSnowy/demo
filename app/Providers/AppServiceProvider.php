<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->share('phone', 'phone is 123456789');

        view()->composer(['gamer', 'foreach'], function ($view) {
            $view->with('age', '18');
        });

        view()->composer('test.*', function ($view) {
            $view->with('height', '180');
        });

    }
}
